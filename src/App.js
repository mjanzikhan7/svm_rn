import React, { useEffect, useRef } from 'react';
import { StatusBar, SafeAreaView } from 'react-native'
import Routing from './Routing';
/*import firebase from 'react-native-firebase';
import NotificationPopup from 'react-native-push-notification-popup';*/
import { NavigationActions } from 'react-navigation';
import Assets from './assets';

console.disableYellowBox = true;
StatusBar.setHidden(false);
StatusBar.setBackgroundColor(Assets.colors.grayBg);


const App = () => {
    const navigatorRef = useRef(null);
    const popupRef = useRef(null);
    useEffect(() => {
        console.log("componentDidMount is called")
        // Build a channel
        /*const channel = new firebase.notifications.Android.Channel('Appointment-channel', 'Appointment Channel', firebase.notifications.Android.Importance.Max)
            .setDescription('Appointment Channel');
        createNotificationListeners();*/

    }
    )

    /*const createNotificationListeners = async () => {

        /!*
   Triggered when a particular notification has been received in foreground
   *!/
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const { title, body, data } = notification;
            console.log("notificationOpenedListener", "Title: " + JSON.stringify(title), "Body: " + body, "data: " + JSON.stringify(data))
            onNotificationRecieve(data)
            showNotification(title, body);
        });
        /!*
       If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
       *!/
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body, data } = notificationOpen.notification;
            console.log("notificationOpenedListener", "Title: " + JSON.stringify(title), "Body: " + body, "data: " + JSON.stringify(data))
            onNotificationRecieve(data)
            firebase.notifications().removeDeliveredNotification('Appointment-channel')
        });

        /!*
        If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
       *!/
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const { title, body, data } = notificationOpen.notification;
            console.log("notificationOpen", "Title: " + JSON.stringify(title), "Body: " + body, "data: " + JSON.stringify(data))
            onNotificationRecieve(data)
            firebase.notifications().removeDeliveredNotification('Appointment-channel')
        }
    }

    const onNotificationRecieve = (data) => {
        console.log("onNotificationRecieve", data)
        /!*  if (data?.type === "reset" && data?.resetToken) {
             if (this.navigator) {
                 this.navigator.dispatch(
                     NavigationActions.navigate({
                         routeName: 'CheckEmailScreen',
                         params: {
                             from: "fp",
                             resetToken: data.resetToken,
                         }
                     })
                 );
             }

         } *!/
    }

    const showNotification = (title, body) => {
        if (popupRef) {
            popupRef.current.show({
                onPress: () => {
                    // TODO
                },
                // appIconSource: images.logo,
                appTitle: 'Appointment',
                timeText: 'Now',
                title: title,
                body: body,
                slideOutTime: 5000
            });
        }
    }*/


    return (
        <SafeAreaView
            style={{ flex: 1, backgroundColor: '#242424', height:500 }}>
            <Routing ref={navigatorRef} />
            {/*<NotificationPopup
                ref={popupRef}
                style={{ zIndex: 99 }}
            />*/}
        </SafeAreaView>
    )

}

export default App;
