import SimpleToast from 'react-native-simple-toast';

// export const BaseUrlAPI ="https://signature-designs.co.uk/staging-api/public/api/v1"
// export const BaseUrlAPI ="https://shinyviewmotors.com/staging-api/public/api/v1"
//Live URL
export const BaseUrlAPI ="https://shinyviewmotors.com/api/public/api/v1"
export const Vehicle ="/vehicle"
export const HomePage ="/home-page"
export const Auth ="/auth"

export const API = {

   //Vehicle
    VehicleDetails: BaseUrlAPI+Vehicle+"/details?",
    GetBrandsWithVehicleType: BaseUrlAPI+Vehicle+"/vehicle-with-brand?",
    GetBrandModels: BaseUrlAPI+Vehicle+"/maker-with-model?",
    Filter: BaseUrlAPI+Vehicle+"/filter",
    ContactSeller: BaseUrlAPI+Vehicle+"/send-email-to-seller",
    FeatureList: BaseUrlAPI+"/vehicle-feature/list",
    VehicleType: BaseUrlAPI+"/sell/vehicle-type",
    SubscriptionPlans: BaseUrlAPI+"/sell/vehicle-price-plan?vehicle_price_id=",
    VerifyCoupon: BaseUrlAPI+"/sell/coupon-verify",
    SaveVehicle: BaseUrlAPI+"/sell/mobile-vehicle-save",

    //HomePage
    FeaturedVehicles: BaseUrlAPI+HomePage+"/featured-list?",
    FilterLists: BaseUrlAPI+HomePage+"/list",
    TotalVehicles: BaseUrlAPI+HomePage+"/total-vehicle-with-filter",

    //Auth
    Login: BaseUrlAPI+Auth+"/login",
    Forgot: BaseUrlAPI+Auth+"/forgot-code",
    Register: BaseUrlAPI+Auth+"/register",
    UpdateProfile: BaseUrlAPI+Auth+"/update-user",
    ForgotCodeVerify: BaseUrlAPI+Auth+"/check-forgot-code",
    ForgotPassUpdate: BaseUrlAPI+Auth+"/update-forgot-password",





}
