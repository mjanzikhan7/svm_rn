import React from 'react';
// import {createAppContainer} from 'react-navigation';
// import {createStackNavigator, Assets} from 'react-navigation-stack';
// import {createBottomTabNavigator} from 'react-navigation-tabs';
import assets from './assets';
import {Image, StatusBar} from 'react-native';
import Preference from 'react-native-preference';
// import _ from 'lodash';
// import Preference from 'react-native-preference';


/*
//Business Screen imports
import Landing from './containers/Business/Landing';
import BusinessSignIn from './containers/Business/Business Sign-in';
import Registration from './containers/Business/Register';
import BusinessSetup from './containers/Business/Business Setup';
import UpdateBusinessProfile from './containers/Business/UpdateBusinessProfile';
import BusinessHours from './containers/Business/BusinessHours';
import Popup from './containers/Business/Profile-Complete-Pop-Up';
import HomeScreenMonth from './containers/Business/HomeScreen_Month';
import Filter from './containers/Business/Filter';
import AddAppointmentTime from './containers/Business/AddNewAppointmentTime';
import AddAppointmentForm from './containers/Business/AddNewAppointment_Form';
import AppointmentAddedPopup from './containers/Business/AppointmentAdded_Popup';
import ActivityScreen from './containers/Business/Activity';
import AllCustomer from './containers/Business/AllCustomer';
import AddCustomer from './containers/Business/AddNewCustomer';
import ServiceCategory from './containers/Business/ServiceCategory';
import AllServices from './containers/Business/AllServices';
import AddServices from './containers/Business/AddNewServices';
import MyEarningBusiness from './containers/Business/MyEarning';
import AppointmentProgress from './containers/Business/AppointmentProgress';
import Setting from './containers/Business/Setting';
import SettingWaitlist from './containers/Business/SettingWaitlist';
import FirstPersonWaitlist from './containers/Business/SettingFirstPersonWaitlist';
import Pricing from './containers/Business/Pricing';
import MapExample from './containers/GoogleMap';
import EditCustomer from './containers/Business/EditCustomer';
import Account from './containers/Business/Account';
import Manual from './containers/Business/Manual';
import EditService from './containers/Business/EditServices';
import ForgotPassword from './containers/Forgot Password';
import Quote from './containers/Business/Quote/Quote';
import Terms from './containers/Business/Terms/Terms';
import Privacy from './containers/Business/Privacy/Privacy';
import Support from './containers/Business/Support/Support';
import Feedback from './containers/Business/Feedback/Feedback';
import Billing from './containers/Business/Billing';
import ReviewOrder from './containers/Business/Review Order';
import PlaceOrder from './containers/Business/Place Order';
import AllStaffMember from './containers/Business/AllStaffMember';
import EditStaffMember from './containers/Business/EditStaffMember';
import AddStaffMember from './containers/Business/AddStaffMember';
import SelectCustomer from './containers/Business/SelectCustomer';
import EditAppointment from './containers/Business/Edit Appointment';
import SelectStaffMember from './containers/Business/SelectStaffMember';
import Map from './containers/Business/Map';
import businessUpdatePassword from './containers/Business/Change Password';
*/


/*//Admin Screen imports
import AddStaff from './containers/AdminScreens/AddStaff';
import MyAccount from './containers/AdminScreens/MyAccount';
import MyEarning from './containers/AdminScreens/MyEarning';
import StaffMember from './containers/AdminScreens/StaffMember';
import AdminLogin from './containers/AdminScreens/AdminLogin';*/


/*//user Screen Import
import Browse from './containers/UserScreens/Browse';
import Appointments from './containers/UserScreens/Appointments';
import Profile from './containers/UserScreens/Profile';
import RegisterUser from './containers/UserScreens/RegisterUser';
import CategorySelection from './containers/UserScreens/CategorySelection';
import BookReservation_1 from './containers/UserScreens/BookReservationStepOne';
import AccountDetail from './containers/UserScreens/AccountDetail';
import Settings from './containers/UserScreens/Settings';
import Payment from './containers/UserScreens/Payment';
import AddPaymentCard from './containers/UserScreens/AddPaymentCard';
import BookReservation_2 from './containers/UserScreens/BookReservStepTwo';
import BookReservation_3 from './containers/UserScreens/BookReservStepThree';
import DetailScreen from './containers/UserScreens/DetailScreen';
import Favorite from './containers/UserScreens/Favorite';
import RateReview from './containers/UserScreens/RateReview';
import BookReservation_three from './containers/UserScreens/BookReservationStepthree_';
import BookAppointmentServices from './containers/UserScreens/BookAppointmentServices';*/


//Business Bottom Nav
/*const BusinessBottomTab = createBottomTabNavigator(
    {
        HomeScreenMonth: {
            screen: HomeScreenMonth,
            navigationOptions: ({navigation}) => ({
                title: 'Appointments',
            }),
        },
        Activity: {
            screen: ActivityScreen,
            navigationOptions: ({navigation}) => ({
                title: 'Activity',
            }),
        },
        Customer: {
            screen: AllCustomer,
            navigationOptions: ({navigation}) => ({
                title: 'Customer',
            }),
        },
        Account: {
            screen: Account,
            navigationOptions: ({navigation}) => ({
                title: 'Account',
            }),
        },
        Setting: {
            screen: Setting,
            navigationOptions: ({navigation}) => ({
                title: 'Settings',
            }),
        },
    },
    {
        tabBarOptions: {
            showLabel: true,
            activeTintColor: '#2D535A',
            inactiveTintColor: 'white',
            keyboardHidesTabBar: true,
            pressColor: 'gray',
            style: {
                padding: 5,
                height: 60,
                backgroundColor: '#6BA706',
                borderTopWidth: 0,
            },
        },
        initialRouteName: 'HomeScreenMonth',
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) => {
                const {routeName} = navigation.state;
                if (routeName === 'HomeScreenMonth') {
                    return <Image source={focused ? assets.icons.appointmentsUser : assets.icons.appointmentsUser}
                                  style={{
                                      resizeMode: 'contain',
                                      height: 25,
                                      width: 25,
                                      marginTop: 5,
                                      tintColor: !focused && 'white',
                                  }}/>;
                } else if (routeName === 'Activity') {
                    return <Image source={focused ? assets.icons.focusActivity : assets.icons.Activity}
                                  style={{resizeMode: 'contain', height: 25, width: 25, marginTop: 5}}/>;
                } else if (routeName === 'Customer') {
                    return <Image source={focused ? assets.icons.focusCustomer : assets.icons.Customer}
                                  style={{resizeMode: 'contain', height: 25, width: 25, marginTop: 5}}/>;
                } else if (routeName === 'Account') {
                    return <Image source={focused ? assets.icons.Account : assets.icons.Account} style={{
                        resizeMode: 'contain',
                        height: 25,
                        width: 25,
                        marginTop: 5,
                        tintColor: focused && '#2C5359',
                    }}/>;
                } else if (routeName === 'Setting') {
                    return <Image source={focused ? assets.icons.focusSetting : assets.icons.Setting}
                                  style={{resizeMode: 'contain', height: 25, width: 25, marginTop: 5}}/>;
                }
            },
            // tabBarTextStyle:{

            // }
        }),
    },
);*/

/*//StaffMember Bottom Nav
const StaffBottomTab = createBottomTabNavigator(
    {
        HomeScreenMonth: {
            screen: HomeScreenMonth,
            navigationOptions: ({navigation}) => ({
                title: 'Appointments',
            }),
        },
        Activity: {
            screen: ActivityScreen,
            navigationOptions: ({navigation}) => ({
                title: 'Activity',
            }),
        },
        Customer: {
            screen: AllCustomer,
            navigationOptions: ({navigation}) => ({
                title: 'Customer',
            }),
        },
        Account: {
            screen: Account,
            navigationOptions: ({navigation}) => ({
                title: 'Account',
            }),
        },
        Setting: {
            screen: Setting,
            navigationOptions: ({navigation}) => ({
                title: 'Settings',
            }),
        },
    },
    {
        tabBarOptions: {
            showLabel: true,
            activeTintColor: '#2D535A',
            inactiveTintColor: 'white',
            keyboardHidesTabBar: true,
            pressColor: 'gray',
            style: {
                padding: 5,
                height: 60,
                backgroundColor: '#6BA706',
                borderTopWidth: 0,
            },
        },
        initialRouteName: 'HomeScreenMonth',
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) => {
                const {routeName} = navigation.state;
                if (routeName === 'HomeScreenMonth') {
                    return <Image source={focused ? assets.icons.appointmentsUser : assets.icons.appointmentsUser}
                                  style={{
                                      resizeMode: 'contain',
                                      height: 25,
                                      width: 25,
                                      marginTop: 5,
                                      tintColor: !focused && 'white',
                                  }}/>;
                } else if (routeName === 'Activity') {
                    return <Image source={focused ? assets.icons.focusActivity : assets.icons.Activity}
                                  style={{resizeMode: 'contain', height: 25, width: 25, marginTop: 5}}/>;
                } else if (routeName === 'Customer') {
                    return <Image source={focused ? assets.icons.focusCustomer : assets.icons.Customer}
                                  style={{resizeMode: 'contain', height: 25, width: 25, marginTop: 5}}/>;
                } else if (routeName === 'Account') {
                    return <Image source={focused ? assets.icons.Account : assets.icons.Account} style={{
                        resizeMode: 'contain',
                        height: 25,
                        width: 25,
                        marginTop: 5,
                        tintColor: focused && '#2C5359',
                    }}/>;
                } else if (routeName === 'Setting') {
                    return <Image source={focused ? assets.icons.focusSetting : assets.icons.Setting}
                                  style={{resizeMode: 'contain', height: 25, width: 25, marginTop: 5}}/>;
                }
            },
            // tabBarTextStyle:{

            // }
        }),
    },
);*/


//User Bottom Nav

import FilterDrawer from './components/FilterDrawer';
import SplashScreen from './containers/SplashScreen';
import HomeScreen from './containers/HomeScreen';
import AccountScreen from './containers/AccountScreen';
import SearchScreen from './containers/SearchScreen';
import SellScreen from './containers/SellScreen';
import ProductDetailScreen from './containers/ProductDetailScreen';
import LoginScreen from './containers/LoginScreen';
import Packages from './containers/Packages';
import BuildUp from './containers/BuildUp';
import ApplyCoupon from './containers/ApplyCoupon';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';

const UserBottomTab = createBottomTabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: ({navigation}) => ({
                title: 'Home',
                tabBarOnPress: () => {

                     navigation.navigate('Home')

                },
            }),
        },
        Search: {
            screen: SearchScreen,
            navigationOptions: ({navigation}) => ({
                title: 'Search',
                tabBarOnPress: () => {

                    navigation.navigate('Search',{
                        nav:"search"

                    })

                },
            }),
        },
        Sell: {
            // screen: ApplyCoupon,
            // screen: BuildUp,
            screen: Packages,
            navigationOptions: ({navigation}) => ({
                title: 'Sell',
                tabBarOnPress: () => {
                    if (Preference.get('user')!= null) {
                        navigation.navigate('Sell')
                    }else{
                        navigation.navigate('Account')
                    }
                },
            }),
        }, Account: {
            screen: AccountScreen,
            navigationOptions: ({navigation}) => ({
                title: 'Account',
                tabBarOnPress: () => {

                    navigation.navigate('Account')

                },
            }),
        },

    },
    {
        tabBarOptions: {
            showLabel: true,
            activeTintColor: assets.colors.activeTintColor,
            inactiveTintColor: assets.colors.inactiveTintColor,
            keyboardHidesTabBar: true,
            pressColor: 'gray',
            style: {
                padding: 5,
                // height: 60,
                backgroundColor: assets.colors.grayBg,
                borderTopWidth: 0,
            },
        },
        initialRouteName: 'Home',
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, horizontal, tintColor}) => {
                const {routeName} = navigation.state;
                if (routeName === 'Home'  ) {
                    return <Image source={focused ? assets.icons.HomeSelected : assets.icons.Home}
                                  style={{
                                      resizeMode: 'contain',
                                      height: focused ? 20 : 20,
                                      width: focused ? 20 : 20,
                                      // marginEnd: focused ? 5 : 0,
                                      marginTop: 5,
                                  }}/>;

                } else if (routeName === 'Search') {
                    return <Image source={focused ? assets.icons.SearchSelected : assets.icons.Search} style={{
                        resizeMode: 'cover',
                        height: focused ? 20 : 200,
                        width: focused ? 20 : 200,
                        // marginEnd: focused ? 5 : 0,
                        marginTop: 5,
                    }}/>;
                } else if (routeName === 'Sell') {
                    return <Image source={focused ? assets.icons.SellSelected : assets.icons.Sell}
                                  style={{
                                      resizeMode: 'contain',
                                      height: focused ? 20 : 20,
                                      width: focused ? 20 : 20,
                                      // marginEnd: focused ? 5 : 0,
                                      marginTop: 5,
                                  }}/>;
                }else if (routeName === 'Account') {
                    return <Image source={focused ? assets.icons.AccountSelected : assets.icons.Account}
                                  style={{
                                      resizeMode: 'contain',
                                      height: focused ? 20 : 20,
                                      width: focused ? 20 : 20,
                                      // marginEnd: focused ? 5 : 0,
                                      marginTop: 5,
                                  }}/>;
                }
            },
        }),
    },
);

const AppNavigatorDrawerStackUserHomeScreen = createStackNavigator(
    {
        Home:UserBottomTab ,
        // Search: SearchScreen,
    },
    {
        initialRouteName: 'Home',
        headerMode: 'none',
    },
);

const AppDrawerNavigator = createDrawerNavigator(
    {
        mainApp: {
            // screen: AppNavigatorDrawerStackUserHomeScreen,
            screen: AppNavigatorDrawerStackUserHomeScreen,
        },
    },
    {


        drawerType: 'front',
        drawerPosition:/* (Preference.get('language') == 'ar')? "right": */'left',
        overlayColor: 0,
        drawerBackgroundColor: '#fff',
        initialRouteName: 'mainApp',
        contentComponent: FilterDrawer,
    },
);

const AppNavigator = createStackNavigator({
        //business screen import
        SplashScreen: SplashScreen,
        SellScreen: SellScreen,
        SearchScreen: SearchScreen,
        AccountScreen: AccountScreen,
        LoginScreen: LoginScreen,
        ProductDetailScreen: ProductDetailScreen,
        HomeScreen: HomeScreen,
        BuildUp: BuildUp,
        ApplyCoupon: ApplyCoupon,
        UserBottomTab: UserBottomTab,
        AppDrawerNavigator: AppDrawerNavigator,

        /*Landing: Landing,
        ForgotPassword: ForgotPassword,
        BusinessSignIn: BusinessSignIn,
        Registration: Registration,
        BusinessSetup: BusinessSetup,
        UpdateBusinessProfile: UpdateBusinessProfile,
        BusinessHours: BusinessHours,
        Popup: Popup,
        // BusinessBottomTab: BusinessBottomTab,
        FilterScreen: Filter,
        AddAppointmentTime: AddAppointmentTime,
        AddAppointmentForm: AddAppointmentForm,
        AppointmentAddedPopup: AppointmentAddedPopup,
        AddCustomer: AddCustomer,
        ServiceCategory: ServiceCategory,
        AllServices: AllServices,
        AddServices: AddServices,
        AppointmentProgress: AppointmentProgress,
        SettingWaitlist: SettingWaitlist,
        FirstPersonWaitlist: FirstPersonWaitlist,
        Pricing: Pricing,
        MapExample: MapExample,
        EditCustomer: EditCustomer,
        MyEarningBusiness: MyEarningBusiness,
        Manual: Manual,
        EditService: EditService,
        Privacy: Privacy,
        Terms: Terms,
        Quote: Quote,
        Support: Support,
        Feedback: Feedback,
        Billing: Billing,
        ReviewOrder: ReviewOrder,
        PlaceOrder: PlaceOrder,
        AllStaffMember: AllStaffMember,
        EditStaffMember: EditStaffMember,
        AddStaffMember: AddStaffMember,
        SelectCustomer: SelectCustomer,
        EditAppointment: EditAppointment,
        SelectStaffMember: SelectStaffMember,
        Map: Map,
        businessUpdatePassword: businessUpdatePassword,


        //Admin Screen imports
        AddStaff: AddStaff,
        MyAccount: MyAccount,
        MyEarning: MyEarning,
        StaffMember: StaffMember,
        AdminLogin: AdminLogin,


        //user Screen Import
        // UserBottomTab: UserBottomTab,
        RegisterUser: RegisterUser,
        CategorySelection: CategorySelection,
        BookReservation_1: BookReservation_1,
        AccountDetail: AccountDetail,
        Settings: Settings,
        Payment: Payment,
        AddPaymentCard: AddPaymentCard,
        BookReservation_2: BookReservation_2,
        BookReservation_3: BookReservation_3,
        DetailScreen: DetailScreen,
        Favorite: Favorite,
        RateReview: RateReview,
        BookReservation_three: BookReservation_three,
        BookAppointmentServices: BookAppointmentServices,*/

    },
    {

        // initialRouteName: 'BusinessSetup',
        initialRouteName: 'SplashScreen',
        // initialRouteName: 'ReviewOrder',
        headerMode: 'none',

    },
);




export default createAppContainer(AppNavigator);
