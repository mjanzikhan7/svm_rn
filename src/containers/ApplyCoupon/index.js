import React, {Component, Fragment} from 'react';
import {
    View, Image, StyleSheet, Text, StatusBar, TextInput,
    TouchableOpacity, FlatList, Dimensions, Platform,
} from 'react-native';
import Assets from '../../assets';
import Logo from '../../components/LogoHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Button1 from '../../components/Button1';
import {API} from '../../constants';
import SimpleToast from 'react-native-simple-toast';
import Loader from '../../components/loader';
import EditText from '../../components/EditText';
import PackageFeatureItem from '../../components/PackageFeatureItem';
import Preference from 'react-native-preference';
import DropDown from '../../components/DropDown1';
import CarSpecItemInput from '../../components/CarSpecItemInput';
import CarSpecItemPicker from '../../components/CarSpecItemPicker';
import {Picker} from 'native-base';
import CropImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import moment from 'moment';
import {NavigationActions, StackActions} from 'react-navigation';


const {width, height} = Dimensions.get('window');
const resetToHome = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: 'AppDrawerNavigator'})],
});
export default class ApplyCoupon extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            cameraModal: false,
            userData: Preference.get('user'),
            listVehicleType: [

                {
                    id: 0,
                    name: 'Select vehicle',
                    value: '20',
                },


            ],
            listVehicleMake: [

                {
                    id: 0,
                    name: 'Select Make',
                    value: '20',
                },


            ],
            listVehicleModel: [

                {
                    id: 1,
                    name: 'Select Model',
                    value: '20',
                },

            ],
            listVehicleYear: [

                {
                    id: 1,
                    reg_year: 'Select Year',
                    value: '20',
                },

            ],
            listVehicleTransmission: [

                {
                    id: 0,
                    name: 'Select Transmission',
                    value: '20',
                },

            ],
            listVehicleCondition: [

                {
                    id: 0,
                    name: 'Select Condition',
                    value: '20',
                },


            ],

            listExteriorColor: [

                {
                    id: 0,
                    color_name: 'Select Exterior',
                },

            ],
            listInteriorColor: [

                {
                    id: 0,
                    color_name: 'Select Interior',
                },

            ],
            listDriveMode: [
                {
                    id: 0,
                    drive_mode_name: 'Select Drive Mode',
                },

            ],
            listCategory: [
                {
                    id: 0,
                    name: 'Select Category',
                    slug: 'category-a-E4Scw-42',
                    status: 1,
                    count: 0,
                },

            ],
            listUlez: [
                {
                    id: 0,
                    name: 'Select ULEZ',
                },

            ],
            listMileage: [
                {
                    id: 0,
                    text: 'Select Mileage',
                },

            ],
            listFuelType: [
                {
                    id: 0,
                    name: 'Select Fuel Type',
                },

            ],
            listEngineSize: [
                {
                    id: 0,
                    engine_size: 'Select Engine Size',
                },

            ],
            listBodyType: [
                {
                    id: 0,
                    name: 'Select Body Type',
                },

            ],
            listUserType: [
                {
                    id: 0,
                    name: 'Select User Type',
                },

            ],
            listFeature: [
                {
                    id: 0,
                    name: 'Search Features',
                },
            ],
            selectedFeatures: [],
            selectedImages: [],


            selectedVType: '',
            selectedVMake: '',
            selectedVModel: '',
            selectedVY: '',
            selectedVTrm: '',
            selectedVC: '',
            selectedFeature: '',
            minPrice: '',
            maxPrice: '',
            minMilage: '',
            maxMilage: '',
            additionalFeatures: '',
            sellerNote: '',
            price: '',


            selectedExterior: '',
            selectedInterior: '',
            selectedDriveMode: '',
            selectedCategory: '',
            selectedUlez: '',
            selectedMileage: '',
            selectedFuelType: '',
            selectedEngineSize: '',
            selectedBodyType: '',
            selectedUserType: '',

            couponData: {},
            postal: '',
            city: '',
            country: '',
            fName: '',
            lName: '',
            house: '',
            street: '',
            buildUpData: this.props.navigation.getParam('data'),
        };
    }

    componentDidMount() {
        // this.getFilterList();
        // this.getFeatureList();

    }

    handleFeatures(data) {
        let temp = [];
        temp = this.state.listFeature;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listFeature: temp,
        });

    }

    handleVehicleType(data) {
        let temp = [];
        temp = this.state.listVehicleType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleType: temp,
        });

    }

    handleVehicleMaker(data) {
        let temp = [];
        temp = this.state.listVehicleMake;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleMake: temp,
        });

    }

    handleVehicleModel(data) {
        let temp = [];
        temp = this.state.listVehicleModel;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleModel: temp,
        });

    }

    handleVehicleyear(data) {
        let temp = [];
        temp = this.state.listVehicleYear;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleYear: temp,
        });

    }

    handleVehicleTransmission(data) {
        let temp = [];
        temp = this.state.listVehicleTransmission;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleTransmission: temp,
        });

    }

    handleVehicleCondition(data) {
        let temp = [];
        temp = this.state.listVehicleCondition;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleCondition: temp,
        });

    }

    handleVehicleExteriorColor(data) {
        let temp = [];
        temp = this.state.listExteriorColor;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }
        this.setState({
            listExteriorColor: temp,
        });

    }

    handleVehicleInteriorColor(data) {
        let temp = [];
        temp = this.state.listInteriorColor;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listInteriorColor: temp,
        });

    }

    handleVehicleDriveMode(data) {
        let temp = [];
        temp = this.state.listDriveMode;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listDriveMode: temp,
        });

    }

    handleVehicleCategory(data) {
        let temp = [];
        temp = this.state.listCategory;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listCategory: temp,
        });

    }

    handleVehicleUlez(data) {
        let temp = [];
        temp = this.state.listUlez;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listUlez: temp,
        });

    }

    handleVehicleMilage(data) {
        let temp = [];
        temp = this.state.listMileage;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listMileage: temp,
        });

    }

    handleVehicleFuelType(data) {
        let temp = [];
        temp = this.state.listFuelType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listFuelType: temp,
        });

    }

    handleVehicleEngineSize(data) {
        let temp = [];
        temp = this.state.listEngineSize;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listEngineSize: temp,
        });

    }

    handleVehicleBodyType(data) {
        let temp = [];
        temp = this.state.listBodyType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listBodyType: temp,
        });

    }

    handleVehicleUserType(data) {
        let temp = [];
        temp = this.state.listUserType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listUserType: temp,
        });

    }

    getFilterList() {
        this.setState({loading: true});

        fetch(API.FilterLists, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let years = [];
                    for (let x = 0; x < response.data.year.length; x++) {
                        years.push({
                            reg_year: response.data.year[x].reg_year + '',
                        });
                    }
                    /*  this.setState({
                          listVehicleType: response.data.vehicles,
                          listVehicleMake: response.data.brands,
                          listVehicleModel: response.data.models,
                          listVehicleYear: years,
                          listVehicleTransmission: response.data.transmition_type,
                          listVehicleCondition: response.data.condition,
                      });*/

                    this.handleVehicleType(response.data.vehicles);
                    this.handleVehicleMaker(response.data.brands);
                    this.handleVehicleModel(response.data.models);
                    this.handleVehicleyear(years);
                    this.handleVehicleTransmission(response.data.transmition_type);
                    this.handleVehicleCondition(response.data.condition);
                    this.handleVehicleExteriorColor(response.data.exterior_color);
                    this.handleVehicleInteriorColor(response.data.interior_color);
                    this.handleVehicleDriveMode(response.data.drive_mode);
                    this.handleVehicleCategory(response.data.category);
                    this.handleVehicleUlez(response.data.ulez);
                    this.handleVehicleMilage(response.data.mileage);
                    this.handleVehicleFuelType(response.data.fuel_type);
                    this.handleVehicleEngineSize(response.data.engine_size);
                    this.handleVehicleBodyType(response.data.body_type);
                    this.handleVehicleUserType(response.data.user_type);

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    getFeatureList() {
        this.setState({loading: true});

        fetch(API.FeatureList, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    this.handleFeatures(response.data);

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    dropVehicleFeatures() {
        let countries = this.state.listFeature;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.name}
            />);
        }));
    }

    dropVehicleType() {
        let countries = this.state.listVehicleType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleMake() {
        let countries = this.state.listVehicleMake;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleModel() {
        let countries = this.state.listVehicleModel;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item}
            />);
        }));
    }

    dropVehicleYear() {
        let countries = this.state.listVehicleYear;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.reg_year} value={item.reg_year}
            />);
        }));
    }

    dropVehicleTransmission() {
        let countries = this.state.listVehicleTransmission;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleCondition() {
        let countries = this.state.listVehicleCondition;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleUserType() {
        let countries = this.state.listUserType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleBodyType() {
        let countries = this.state.listBodyType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleEngineSize() {
        let countries = this.state.listEngineSize;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.engine_size} value={item.id}
            />);
        }));
    }

    dropVehicleFuelType() {
        let countries = this.state.listFuelType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleMileage() {
        let countries = this.state.listMileage;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.text} value={item.id}
            />);
        }));
    }

    dropVehicleUlez() {
        let countries = this.state.listUlez;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleCategory() {
        let countries = this.state.listCategory;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleDriveMode() {
        let countries = this.state.listDriveMode;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.drive_mode_name} value={item.id}
            />);
        }));
    }

    dropVehicleInterior() {
        let countries = this.state.listInteriorColor;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.color_name} value={item.id}
            />);
        }));
    }

    dropVehicleExterior() {
        let countries = this.state.listExteriorColor;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.color_name} value={item.id}
            />);
        }));
    }

    onValueChangeFeat(value: string) {
        let newFeat = this.state.selectedFeatures;
        let newItem = {name: value};
        newFeat.push(newItem);
        this.setState({
            selectedFeatures: newFeat,
            selectedFeature: value,
        });
        // SimpleToast.show(JSON.stringify(value))
    }

    onValueChangeVT(value: string) {
        this.setState({
            selectedVType: value,

        });
        // SimpleToast.show(JSON.stringify(value))
    }

    onValueChangeVMake(value: string) {
        this.setState({
            selectedVMake: value,
        });
    }

    onValueChangeVModel(value: string) {
        this.setState({
            selectedVModel: value,
        });
    }

    onValueChangeVY(value: string) {
        this.setState({
            selectedVY: value,
        });
    }

    onValueChangeVTrm(value: string) {
        this.setState({
            selectedVTrm: value,
        });
    }

    onValueChangeVC(value: string) {
        this.setState({
            selectedVC: value,
        });
    }

    onValueChangeUT(value: string) {
        this.setState({
            selectedUserType: value,
        });
    }

    onValueChangeBT(value: string) {
        this.setState({
            selectedBodyType: value,
        });
    }

    onValueChangeES(value: string) {
        this.setState({
            selectedEngineSize: value,
        });
    }

    onValueChangeFT(value: string) {
        this.setState({
            selectedFuelType: value,
        });
    }

    onValueChangeMl(value: string) {
        this.setState({
            selectedMileage: value,
        });
    }

    onValueChangeUlz(value: string) {
        this.setState({
            selectedUlez: value,
        });
    }

    onValueChangeCat(value: string) {
        this.setState({
            selectedCategory: value,
        });
    }

    onValueChangeDM(value: string) {
        this.setState({
            selectedDriveMode: value,
        });
    }

    onValueChangeInt(value: string) {
        this.setState({
            selectedInterior: value,
        });
    }

    onValueChangeExt(value: string) {
        this.setState({
            selectedExterior: value,
        });
    }


    renderPackageDetailSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Package Details'}
                    </Text>
                    <View
                        style={{
                            alignItems: 'center', margin: 10,
                            borderRadius: 15,
                            // borderWidth: 0.1,
                            backgroundColor: Assets.colors.featureBg1,
                        }}>


                        {/*Heading*/}
                        <View
                            style={styles.itemRowStyle}
                        >
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemHeadStyle}>{'PRODUCT'}</Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemHeadStyle}>{'TOTAL'}</Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>

                        {/*Package*/}
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{
                                    // 'Silver'
                                    this.state.buildUpData.planName
                                }</Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {'£' + this.state.buildUpData.planPrice}
                                    {/*{'£0.00'}*/}
                                </Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>

                        {/*Subtotal*/}
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Subtotal'}</Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {/*{'£0.00'}*/}
                                    {'£' + this.state.buildUpData.planPrice}

                                </Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>

                        {/*Total*/}
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Total'}</Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {'£' + (this.state.buildUpData.planPrice
                                        // - (this.state.couponData.discount)
                                    )}
                                    {/*{'£0.00'}*/}
                                </Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>


                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderCustomerDetailSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Customer Details'}
                    </Text>
                    <View
                        style={{
                            alignItems: 'center', margin: 10,
                            borderRadius: 15,
                            // borderWidth: 0.1,
                            backgroundColor: Assets.colors.featureBg1,
                        }}>

                        {/*Note*/}
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Note'}</Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {'£' + this.state.buildUpData.description}
                                    {/*{'Ebad Khan'}*/}
                                </Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>

                        {/*Email*/}
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Email'}</Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {''}
                                    {/*{'ebad.connect@gmail.com'}*/}
                                </Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>


                        {/*Telephone*/}
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {'Telephone'}
                                </Text>

                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>
                                    {''}
                                    {/*{'12358648'}*/}
                                </Text>

                            </View>

                        </View>
                        <View
                            style={{
                                flex: 1, flexDirection: 'row',
                                width: '90%', height: 0.5, backgroundColor: 'gray',
                                margin: 10, marginTop: 3,
                            }}/>


                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }


    renderBillingAddressSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Billing Address'}
                    </Text>
                    <View
                        style={{
                            alignItems: 'center', margin: 10,
                            borderRadius: 15,
                            // borderWidth: 0.1,
                            backgroundColor: Assets.colors.featureBg1,
                        }}>

                        <View
                            style={styles.itemRowStyle}>
                            <EditText
                                label={'First Name'}
                                keyboardType={'default'}
                                // placeholder={'First Name'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        fName: text,
                                    });
                                }}
                                value={this.state.fName}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}
                            />
                            <EditText
                                label={'Last Name'}
                                keyboardType={'default'}
                                // placeholder={'Last Name'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        lName: text,
                                    });
                                }}
                                value={this.state.lName}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}
                            />


                        </View>


                        <View
                            style={styles.itemRowStyle}>
                            <EditText
                                label={'Flat/House No *'}
                                keyboardType={'default'}
                                // placeholder={'Flat/House No *'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        house: text,
                                    });
                                }}
                                value={this.state.house}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}

                            />
                            <EditText
                                label={'Street Name *'}
                                keyboardType={'default'}
                                // placeholder={'Street Name *'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        street: text,
                                    });
                                }}
                                value={this.state.street}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}
                            />

                        </View>


                        <View
                            style={styles.itemRowStyle}>

                            <EditText
                                label={'County'}
                                // label={'Country'}
                                keyboardType={'default'}
                                // placeholder={'Country'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        country: text,
                                    });
                                }}
                                value={this.state.country}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}
                            />

                            <EditText
                                label={'Town/City *'}
                                keyboardType={'default'}
                                // placeholder={'Town/City *'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        city: text,
                                    });
                                }}
                                value={this.state.city}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}
                            />


                        </View>

                        <View
                            style={styles.itemRowStyle}>

                            <EditText
                                label={'Postal Code'}
                                keyboardType={'default'}
                                // placeholder={'Postal Code'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        postal: text,
                                    });
                                }}
                                value={this.state.postal}
                                // maxLength={this.props.maxLength}
                                // style={styles.featureTextValueStyle}
                            />

                            <View
                                style={{width: '48%'}}/>


                        </View>
                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }


    renderImageSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Upload Photo'}
                    </Text>
                    <View
                        style={{
                            // alignItems: 'center',
                            margin: 10,
                            // borderRadius: 10,
                            //  borderWidth: 0.1,
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.toggleCameraModal();
                            }}
                            style={{
                                margin: 5, padding: 10, borderRadius: 10, width: '30%',
                                borderWidth: 1, borderColor: Assets.colors.vehicleTypeBg,
                            }}>
                            <Text
                                style={{color: Assets.colors.vehicleTypeBg}}>
                                Add Image
                            </Text>

                        </TouchableOpacity>

                        <View
                            style={{
                                flexDirection: 'column',
                                flex: 0.90,
                                marginStart: 5,
                                marginEnd: 5,
                            }}>


                            <View
                                //style={style.flatlistView}
                            >
                                <FlatList
                                    // listKey={moment().format('x').toString()}
                                    style={{marginTop: 10}}
                                    data={this.state.selectedImages}
                                    keyExtractor={item => item.id}
                                    horizontal={true}
                                    extraData={this.props}
                                    showsHorizontalScrollIndicator={false}
                                    contentContainerStyle={{alignItems: 'center'}}
                                    removeClippedSubviews={false}
                                    renderItem={({item, index}) => {
                                        return this.renderSelectedImages(item, index);
                                    }}
                                />
                            </View>

                        </View>

                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderSelectedFeatures(item, index) {

        return (
            <View
                style={{
                    flexDirection: 'column',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    flex: 0.5,
                }}>

                <View
                    style={{
                        flexDirection: 'row',
                        padding: 1, flex: 1,
                        alignItems: 'center',
                    }}>
                    <Text
                        numberOfLines={1}
                        style={styles.recentTextStyle1}>
                        {item.name}

                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            let tempList = this.state.selectedFeatures;
                            tempList.splice(index, 1);
                            this.setState({
                                selectedFeatures: tempList,
                            });
                        }
                        }>
                        <Image
                            source={Assets.icons.Cross}
                            style={{resizeMode: 'cover', width: 15, height: 15, borderRadius: 5}}/>
                    </TouchableOpacity>

                </View>

            </View>

        );
    }

    captureImage = () => {

        CropImagePicker.openCamera({

            cropping: true,
            compressImageQuality: 0.2,
            multiple: false,
            freeStyleCropEnabled: true,
            hideBottomControls: true,

        }).then(image => {
            // console.log(image);
            let source1 = {uri: image.path, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.state.selectedImages.push(source1);
            this.forceUpdate();
            // this.setState({
            //     filePath: source1,
            //     // isSelectImage: true,
            //     // profileImageSelected: true,
            //
            // });
            this.toggleCameraModal();
        });
        /*
                ImagePicker.showImagePicker(options, response => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                        alert(response.customButton);
                    } else {
                        let source1 = {uri: response.uri, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
                        // You can also display the image using data:
                        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                            this.setState({
                                filePath:  source1,
                                isSelectImage: true,

                            });


                    }
                });
        */
    };
    pickFromGallery = () => {


        CropImagePicker.openPicker({

            cropping: true,
            compressImageQuality: 0.2,
            multiple: false,
            freeStyleCropEnabled: true,
            hideBottomControls: true,

        }).then(image => {
            console.log(image);
            let source1 = {uri: image.path, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.state.selectedImages.push(source1);
            this.forceUpdate();
            /* this.setState({
                 filePath: source1,
                 isSelectImage: true,
                 profileImageSelected: true,

             });*/
            this.toggleCameraModal();

        });
        /*
                ImagePicker.showImagePicker(options, response => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                        alert(response.customButton);
                    } else {
                        let source1 = {uri: response.uri, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
                        // You can also display the image using data:
                        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                            this.setState({
                                filePath:  source1,
                                isSelectImage: true,

                            });


                    }
                });
        */
    };

    toggleCameraModal() {
        this.setState({cameraModal: !this.state.cameraModal});
    }

    renderSelectedImages(item, index) {
        console.log(JSON.stringify(item,
        ));
        return (
            <View

                style={{margin: 5, borderRadius: 5}}>
                <Image source={{uri: item.uri}} style={{
                    resizeMode: 'contain',
                    width: 150, height: 120, borderRadius: 0,
                }}/>
                <TouchableOpacity
                    style={{
                        position: 'absolute', end: 15,
                        top: -5,
                    }}

                    onPress={() => {
                        let temp = [];
                        for (let i = 0; i < this.state.selectedImages.length; i++) {

                            if (index !== i) {
                                temp.push(this.state.selectedImages[i]);
                            }
                        }
                        this.setState({
                            selectedImages: temp,
                        });
                    }}>
                    <Image source={Assets.icons.Cross} style={{
                        resizeMode: 'contain', width: 15, height: 15,
                    }}/>
                </TouchableOpacity>
            </View>
        );
    }

    verifyCoupon() {
        const {
            coupon,
        } = this.state;
        this.setState({loading: true});
        console.log('verifyCoupon');
        console.log('API URL:  ' + API.VerifyCoupon);
        let requestBody = new FormData();
        requestBody.append('code', coupon);

        fetch(API.VerifyCoupon, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            // console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('\n\n\nverifyCoupon Response: ' + JSON.stringify(response.data));

            if (response.status) {
                let percent = response.data.discount/100;
                let subtractValue = this.state.buildUpData.planPrice * percent;
                let newSubtotal = this.state.buildUpData.planPrice - subtractValue;
                let newBuildUpData = this.state.buildUpData;
                newBuildUpData.planPrice = newSubtotal
                this.setState({loading: false});
                this.setState({
                    couponData: response.data,
                    buildUpData: newBuildUpData,
                });
                // SimpleToast.show("Coupon Applied")

            } else {

                this.setState({
                    loading: false,
                    couponData: {},
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
            SimpleToast.show(response.message);

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    saveVehicle() {
        const {
            coupon,
            postal,
            city,
            country,
            fName,
            lName,
            house,
            street,
            couponData,
            buildUpData,

        } = this.state;
        this.setState({loading: true});
        console.log('verifyCoupon');
        console.log('API URL:  ' + API.SaveVehicle);
        let requestBody = new FormData();


        requestBody.append('user_id', this.state.userData.user_info.id);
        /*requestBody.append('billing_first_name', 'fName');
        requestBody.append('billing_last_name', 'lName');
        requestBody.append('billing_flat_no', 'house');
        requestBody.append('billing_street_name', 'street');
        requestBody.append('billing_county', 'country');
        requestBody.append('billing_town', 'city');
        requestBody.append('billing_post_code', 'postal');*/

        for (let x = 0; x < buildUpData.featured.length; x++) {
            requestBody.append('featured[' + x + ']', buildUpData.featured[x]);
        }

        for (let x = 0; x < buildUpData.images.length; x++) {
            requestBody.append('images[' + x + ']', buildUpData.images[x]);
        }
        // requestBody.append('featured[]', buildUpData.featured);
        // requestBody.append('images[]', buildUpData.images);
        // requestBody.append('featured[]', JSON.stringify(buildUpData.featured));
        // requestBody.append('images[]', JSON.stringify(buildUpData.images));

        // requestBody.append('user_id', couponData.id);
        requestBody.append('billing_first_name', fName);
        requestBody.append('billing_last_name', lName);
        requestBody.append('billing_flat_no', house);
        requestBody.append('billing_street_name', street);
        requestBody.append('billing_county', country);
        requestBody.append('billing_town', city);
        requestBody.append('billing_post_code', postal);
        requestBody.append('price', buildUpData.price);
        requestBody.append('registered_date', buildUpData.registered_date);
        requestBody.append('ulez_id', buildUpData.ulez_id);
        requestBody.append('interior_color_id', buildUpData.interior_color_id);
        requestBody.append('fuel_type_id', buildUpData.fuel_type_id);
        requestBody.append('transmission_type_id', buildUpData.transmission_type_id);
        requestBody.append('mileage', buildUpData.mileage);
        requestBody.append('year', buildUpData.year);
        requestBody.append('condtion_id', buildUpData.condtion_id);
        requestBody.append('history', buildUpData.history);
        requestBody.append('category_id', buildUpData.category_id);
        requestBody.append('engine', buildUpData.engine);
        requestBody.append('owner', buildUpData.owner);
        requestBody.append('title', buildUpData.title);
        requestBody.append('exterior_color_id', buildUpData.exterior_color_id);
        requestBody.append('drive_mode_id', buildUpData.drive_mode_id);
        requestBody.append('body_type_id', buildUpData.body_type_id);
        requestBody.append('brand_model_id', buildUpData.brand_model_id);
        requestBody.append('brand_id', buildUpData.brand_id);
        requestBody.append('vehicle_type_id', buildUpData.vehicle_type_id);
        requestBody.append('vehicle_plan_id', buildUpData.vehicle_plan_id);
        requestBody.append('vehicle_price_id', buildUpData.vehicle_price_id);

        /*requestBody.append('mpg', 'mpg');
        requestBody.append('description', 'description');
        requestBody.append('price', '2000');
        requestBody.append('registered_date', '2021-02-11');
        requestBody.append('ulez_id', '1');
        requestBody.append('interior_color_id', '1');
        requestBody.append('fuel_type_id', '1');
        requestBody.append('transmission_type_id', '1');
        requestBody.append('mileage', '100');
        requestBody.append('year', '2000');
        requestBody.append('condtion_id', '1');
        requestBody.append('history', 'dgsga');
        requestBody.append('category_id', '1');
        requestBody.append('engine', '1.23');
        requestBody.append('owner', 'fsadag');
        requestBody.append('title', 'sdagasgs');
        requestBody.append('exterior_color_id', '1');
        requestBody.append('drive_mode_id', '1');
        requestBody.append('body_type_id', '1');
        requestBody.append('brand_model_id', '1');
        requestBody.append('brand_id', '1');
        requestBody.append('vehicle_type_id', '1');
        // requestBody.append('planPrice', buildUpData);
        requestBody.append('vehicle_plan_id', '1');
        requestBody.append('vehicle_price_id', '1');*/


        console.log('\n\n\nParams:', requestBody);

        fetch(API.SaveVehicle, {
            method: 'POST',
            headers: {
                // 'Accept': 'application/json',
                // 'Content-Type': 'multipart/form-data',
                'Accept': 'multipart/form-data',
                // 'Accept': 'application/json',
                // 'Content-Type': 'multipart/form-data',
            },
            body: requestBody,
        }).then(response => {
            // console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('\n\n\n>>>Response: ' + JSON.stringify(response));

            if (response.success) {
                this.setState({loading: false});
                this.props.navigation.dispatch(resetToHome)
                SimpleToast.show(response.message);

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
                SimpleToast.show("Something went wrong");

            }

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    render() {
        let code = "";
        if (this.state.userData.coupon !== null && this.state.userData.coupon.code !== null){
            code = this.state.userData.coupon.code
        }
        return (
            <View style={styles.mainContainer}>
                <View>
                    <Logo/>
                    <TouchableOpacity
                        style={{
                            position: 'absolute', left: 10, top: 5,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                        <Image style={{width: 30, height: 30}}
                               resizeMode={'contain'}
                               source={Assets.icons.Back}/>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            position: 'absolute', right: 10, top: 10,
                        }}
                        onPress={() => {
                            this.props.navigation.dispatch(resetToHome);
                        }}>
                        <Image style={{width: 20, height: 20, tintColor:Assets.colors.vehicleTypeBg}}
                               resizeMode={'contain'}
                               source={Assets.icons.Home}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.mainContainer}>

                    <KeyboardAwareScrollView>
                        <View
                            style={{
                                flex: 1, flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <View
                                style={{width: '100%', flexDirection: 'column'}}>
                                <Text style={styles.buttonTextStyle}>{'Checkout Steps'}</Text>
                                {/* <Text style={{
                                    fontSize: 9, color: 'black', marginStart: 20,
                                }}>{'You are currently building a Free Ad'}</Text>
                                <Text style={{
                                    fontSize: 12, color: 'black', marginStart: 20,
                                }}>{'Items marked with * are required fields'}</Text>*/}

                            </View>

                            <View
                                style={{
                                    marginTop: 20,
                                    width: '50%', borderRadius: 15,
                                    backgroundColor: Assets.colors.vehicleTypeBg,
                                    padding: 5, alignItems: 'center',
                                    justifyContent: 'center',
                                    flexDirection: 'row',
                                }}>

                                <Text
                                    style={{
                                        fontWeight: 'bold',
                                        color: 'black',
                                        fontSize: 11,
                                    }}>Coupon Code</Text>
                                <View
                                    style={{
                                        backgroundColor: 'white',
                                        width: 1,
                                        height: 20,
                                        marginStart: 5,
                                        marginEnd: 5,
                                    }}/>
                                <Text
                                    style={{
                                        fontWeight: 'bold',
                                        color: 'white',
                                        fontSize: 11,
                                    }}>
                                    {code+""}
                                </Text>


                            </View>

                            <View
                                style={{
                                    marginTop: 10,
                                    width: '90%', borderRadius: 20,
                                    borderColor: 'black',
                                    borderWidth: 0.2,
                                    alignItems: 'center',
                                    // justifyContent:"center",
                                    flexDirection: 'row',
                                    padding: 5,
                                }}>

                                <Text
                                    style={{
                                        fontWeight: 'bold',
                                        color: Assets.colors.vehicleTypeBg,
                                        fontSize: 11,
                                    }}>
                                    {/*Enter Coupon Code*/}
                                    {"Put coupon (Optional)"}
                                </Text>

                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'1234abc'}
                                    // multiline={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            coupon: text,
                                        });
                                    }}
                                    value={this.state.coupon}
                                    // maxLength={this.props.maxLength}
                                    style={[{
                                        marginStart: 10,
                                        color: 'gray',
                                        // color: '#464646',
                                        fontSize: 16,
                                        width: '35%',
                                        fontWeight: '800',
                                        marginTop: -7,
                                        marginBottom: -7,
                                    }]}/>

                                <TouchableOpacity
                                    style={{
                                        marginStart: 20,
                                        width: Platform.OS === 'ios' ? "23%":"25%",
                                        padding: 10, borderRadius: 20,
                                        backgroundColor: Assets.colors.vehicleTypeBg, alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        this.verifyCoupon();
                                    }}>

                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            color: 'white',
                                            fontSize: Platform.OS === 'ios' ? 10:11,
                                        }}>Apply</Text>


                                </TouchableOpacity>


                            </View>


                            <View
                                style={{
                                    flex: 1, flexDirection: 'column', width: '100%',
                                }}>
                                {/*{this.renderPackageDetailSection()}*/}
                                {/*{this.renderCustomerDetailSection()}*/}

                                {this.renderBillingAddressSection()}

                                <View
                                    style={{width: '100%', alignItems: 'center'}}>
                                    <TouchableOpacity
                                        style={{
                                            margin: 20,
                                            width: '25%',
                                            padding: 10, borderRadius: 20,
                                            backgroundColor: Assets.colors.vehicleTypeBg, alignItems: 'center',
                                        }}
                                        onPress={() => {
                                            this.saveVehicle();
                                        }}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                                fontSize: 11,
                                            }}>Submit</Text>


                                    </TouchableOpacity>

                                </View>

                            </View>


                        </View>
                    </KeyboardAwareScrollView>
                </View>

                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,
        width: '90%',

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 9,
        marginStart: 5,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},
    buttonTextStyle: {
        color: Assets.colors.vehicleTypeBg,
        marginTop: 20,
        marginStart: 20,
        fontWeight: 'bold',
        fontSize: 20,
    },
    Input: {
        color: '#464646',
        fontSize: 10,
        width: '80%',
    }, itemTitleStyle: {
        fontSize: 12, color: 'black', fontWeight: 'bold', marginStart: 15,
    }, itemHeadStyle: {
        fontSize: 12, color: Assets.colors.vehicleTypeBg, fontWeight: 'bold', marginStart: 15,
    }, sectionTitleStyle: {
        fontSize: 15,
        color: Assets.colors.vehicleTypeBg,
        fontWeight: 'bold',
        marginTop: 20,
        marginStart: 20,
    },
    itemRowStyle: {alignItems: 'center',
    flexDirection: 'row',
    flex: 0.95,
    marginTop: 5,
    marginBottom: 5
},
    Alert_Main_View: {

        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        height: 200,
        width: '90%',
        borderRadius: 10,
        resizeMode: 'contain',

    },

    Alert_Title: {

        fontSize: 18,
        color: 'black',
        textAlign: 'center',
        padding: 10,
        height: '28%',
    },

    Alert_Message: {

        fontSize: 16,
        color: 'white',
        textAlign: 'center',
        padding: 10,
        borderRadius: 5,
        width: 200,
        backgroundColor: Assets.colors.vehicleTypeBg,
    },

    buttonStyle: {

        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',

    },

    modalTextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 22,
        marginTop: -5,
    },


});
