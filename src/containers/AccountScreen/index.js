import React, {Component} from 'react';
import {View, Image, StyleSheet, Text, StatusBar, TextInput, TouchableOpacity} from 'react-native';
import Assets from '../../assets';
import Logo from '../../components/LogoHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Button1 from '../../components/Button1';
import {API} from '../../constants';
import SimpleToast from 'react-native-simple-toast';
import Loader from '../../components/loader';
import Preference from 'react-native-preference';


export default class SplashScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            isValid: false,
            fName: '',
            lName: '',
            cPassword: '',
            phone: '',
            oldPass: '',
            code: '',
            newPass: '',
            confirmPass: '',
            password: '',
            email: '',

            isLogin: true,
            isRegister: false,
            isForgot: false,
            isVerify: false,
            isNewCred: false,
            isProfile: false,
            authToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvc2hpbnl2aWV3bW90b3JzLmNvbVwvc3RhZ2luZy1hcGlcL3B1YmxpY1wvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE2MDQ3Njc5MTksImV4cCI6MTYwNjI3OTkxOSwibmJmIjoxNjA0NzY3OTE5LCJqdGkiOiJ1UkQ1RW5tS2hZdWhPTHVGIiwic3ViIjoyOCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.injix5pJoL3PHnTcIGpxHxU12NUi4G8Q5g3mXcL8Bhw',


        };
    }

    componentDidMount() {
        let userData = Preference.get('user');
        if (Preference.get('user') != null) {
            console.log('if');
            this.setState({
                isProfile: true,
                isLogin: false,
                isRegister: false,
                isForgot: false,
                authToken: userData.access_token,
                email: userData.user_info.email,
                fName: userData.user_info.first_name,
                lName: userData.user_info.last_name,
                phone: userData.user_info.phone,
            });
        } else {
            console.log('else');

            this.setState({
                isProfile: false,
                isLogin: true,
                isRegister: false,
                isForgot: false,

            });
        }

    }

    userLogin() {
        const {email, password} = this.state;
        this.setState({loading: true});
        console.log('searchVehicles');
        console.log('API URL:  ' + API.Filter);
        let requestBody = new FormData();
        requestBody.append('password', password);
        requestBody.append('email', email);

        console.log(requestBody);
        fetch(API.Login, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.status) {
                this.setState({loading: false});
                /* let data = response.data;

                 if (data.result.length > 0) {
                     this.props.navigation.navigate('Search', {
                         vehicleList: data.result,
                     });
                 } else {
                     SimpleToast.show('Data not found');
                 }*/
                Preference.set('user', response);
                this.setState({
                    isLogin: false,
                    isRegister: false,
                    isForgot: false,
                    isProfile: true,
                    authToken: response.access_token,
                    email: response.user_info.email,
                    fName: response.user_info.first_name,
                    lName: response.user_info.last_name,
                    phone: response.user_info.phone,
                });


            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
                SimpleToast.show('Error: ' + response.message);
            }
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    userForgotPass() {
        const {email} = this.state;
        this.setState({loading: true});
        console.log('searchVehicles');
        console.log('API URL:  ' + API.Filter);
        let requestBody = new FormData();
        requestBody.append('email', email);

        console.log(requestBody);
        fetch(API.Forgot, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.status) {

                this.setState({
                    loading: false,
                    isLogin: false,
                    isRegister: false,
                    isForgot: false,
                    isProfile: false,
                    isVerify: true,
                    isNewCred: false,
                });

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
            SimpleToast.show(response.message + '');

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }


    userForgotPassVerify() {
        const {code} = this.state;
        this.setState({loading: true});
        console.log('userForgotPassVerify');
        console.log('API URL:  ' + API.ForgotCodeVerify);
        let requestBody = new FormData();
        requestBody.append('code', code);

        console.log(requestBody);
        fetch(API.ForgotCodeVerify, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            // console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.status) {

                this.setState({
                    loading: false,
                    isLogin: false,
                    isRegister: false,
                    isForgot: false,
                    isProfile: false,
                    isVerify: false,
                    isNewCred: true,
                });

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
            SimpleToast.show(response.message + '');

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }


    userForgotPassUpdate() {
        const {
            email,
            newPass,
            confirmPass,
        } = this.state;
        this.setState({loading: true});
        console.log('userForgotPassVerify');
        console.log('API URL:  ' + API.ForgotPassUpdate);
        let requestBody = new FormData();
        requestBody.append('email', email);
        requestBody.append('password', newPass);
        requestBody.append('password_confirmation', confirmPass);

        console.log(requestBody);
        fetch(API.ForgotPassUpdate, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            // console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.status) {

                this.setState({
                    loading: false,
                    isLogin: true,
                    isRegister: false,
                    isForgot: false,
                    isProfile: false,
                    isVerify: false,
                    isNewCred: false,
                });

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
            SimpleToast.show(response.message + '');

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    userRegister() {
        const {email, password, fName, lName, cPassword, phone} = this.state;
        this.setState({loading: true});
        console.log('searchVehicles');
        console.log('API URL:  ' + API.Register);
        let requestBody = new FormData();
        requestBody.append('first_name', fName);
        requestBody.append('last_name', lName);
        requestBody.append('email', email);
        requestBody.append('phone', phone);
        requestBody.append('password', password);
        requestBody.append('password_confirmation', cPassword);

        console.log(requestBody);
        fetch(API.Register, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.status) {
                this.setState({
                    loading: false,
                    isLogin: true,
                    isRegister: false,
                    isForgot: false,
                    isProfile: false,

                    fName: '',
                    lName: '',
                    cPassword: '',
                    phone: '',
                    oldPass: '',
                    code: '',
                    newPass: '',
                    confirmPass: '',
                    password: '',
                    email: '',
                });
                SimpleToast.show(response.message);

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
                SimpleToast.show('Error: ' + response.errors[0]);
            }
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    updateProfile() {

        const {email, password, fName, lName, cPassword, phone, oldPass} = this.state;
        this.setState({loading: true});
        console.log('searchVehicles');
        console.log('API URL:  ' + API.UpdateProfile);
        let requestBody = new FormData();
        requestBody.append('first_name', fName);
        requestBody.append('last_name', lName);
        requestBody.append('email', email);
        requestBody.append('phone', phone);
        requestBody.append('password', password);
        requestBody.append('password_confirmation', cPassword);
        requestBody.append('old_password', oldPass);

        console.log(requestBody);
        fetch(API.UpdateProfile, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + this.state.authToken,
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));


            if (response.success) {
                this.setState({
                    loading: false,
                    isLogin: false,
                    isRegister: false,
                    isForgot: false,
                    isProfile: true,
                });
                SimpleToast.show(response.message);

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
                SimpleToast.show('Error: ' + response.error);
            }

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    userLogout() {
        Preference.clear('user');
        this.setState({
            isLogin: true,
            isRegister: false,
            isForgot: false,
            isProfile: false,
            authToken: '',
            email: '',
            fName: '',
            lName: '',
            phone: '',
        });
    }

    validateLogin() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.state.email.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Email');
        } else if (reg.test(this.state.email) === false) {
            SimpleToast.show('Email Format Is Not Valid');
        } else if (this.state.password.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Password');
        } else if (this.state.password.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Password Must Be At Least 6 Characters Long');
        } else {
            this.state.isValid = true;
            this.userLogin();

        }
    }

    validateCode() {

        if (this.state.code.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Code');
        }
        /* else if (this.state.password.length < 6) {
              this.state.isValid = false;
              SimpleToast.show('Password Must Be At Least 6 Characters Long');
          } */
        else {
            this.state.isValid = true;
            this.userForgotPassVerify();

        }
    }

    validatePassUpdate() {

        if (this.state.newPass.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter New Password');
        } else if (this.state.newPass.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Password Must Be At Least 6 Characters Long');
        } else if (this.state.confirmPass.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter New Password Again');
        } else if (this.state.confirmPass.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Password Must Be At Least 6 Characters Long');
        }else if (this.state.confirmPass !== this.state.newPass) {
            this.state.isValid = false;
            SimpleToast.show('Password Must Be Same');
        } else {
            this.state.isValid = true;
            this.userForgotPassUpdate();

        }
    }

    validateForgot() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.state.email.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Email');
        } else if (reg.test(this.state.email) === false) {
            SimpleToast.show('Email Format Is Not Valid');
        } else {
            this.state.isValid = true;
            this.userForgotPass();
        }
    }

    validateRegister() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.state.fName.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter First Name');
        } else if (this.state.lName.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Last Name');
        } else if (this.state.phone.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Phone Number');
        } else if (this.state.email.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Email');
        } else if (reg.test(this.state.email) === false) {
            SimpleToast.show('Email Format Is Not Valid');
        } else if (this.state.password.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Password');
        } else if (this.state.password.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Password Must Be At Least 6 Characters Long');
        } else {
            this.state.isValid = true;
            this.userRegister();
        }
    }

    validateUpdateProfile() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.state.email.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Email');
        } else if (reg.test(this.state.email) === false) {
            SimpleToast.show('Email Format Is Not Valid');
        } else if (this.state.oldPass.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Old Password');
        } else if (this.state.password.length > 0 && this.state.password.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Password Must Be At Least 6 Characters Long');
        } else if (this.state.cPassword.length > 0 && this.state.cPassword.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Confirm Password Must Be At Least 6 Characters Long');
        } else if (this.state.oldPass.length < 6) {
            this.state.isValid = false;
            SimpleToast.show('Old Password Must Be At Least 6 Characters Long');
        } else {
            this.state.isValid = true;
            this.updateProfile();

        }
    }

    renderLogin() {
        return (
            <View style={styles.mainContainer}>
                <Logo/>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column', justifyContent: 'center',
                            alignItems: 'center',
                        }}>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                alignItems: 'center', width: '90%', margin: 50, padding: 30, borderRadius: 5,
                                // shadowColor: '#000000',
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 3,
                                // },
                                // shadowRadius: 5,
                                // shadowOpacity: 1.0,
                            }}>

                            <Text style={styles.buttonTextStyle}>{'Login'}</Text>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Email}/>
                                <TextInput
                                    keyboardType={'email-address'}
                                    placeholder={'Email'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            email: text,
                                        });
                                    }}
                                    value={this.state.email}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}
                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Password'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            password: text,
                                        });
                                    }}
                                    value={this.state.password}
                                    placeholderTextColor={"gray"}

                                    // maxLength={this.props.maxLength}

                                    style={styles.Input}/>
                            </View>


                            <Button1
                                buttonStyle={{marginTop: 50, padding: 5, width: '100%'}}
                                onPress={() => {
                                }}
                                text1={'Login'}
                                onPress={() => {
                                    this.validateLogin();
                                }}
                            />

                            <View
                                style={{flexDirection: 'row', margin: 10, paddingBottom: 15, width: '100%'}}>
                                <TouchableOpacity>
                                    <Image source={Assets.icons.Checked}
                                           style={{
                                               resizeMode: 'contain', width: 15, height: 15, marginEnd: 5,
                                           }}/>
                                </TouchableOpacity>
                                <Text
                                    style={{
                                        color: Assets.colors.vehicleTypeBg,

                                        fontSize: 10,
                                        fontWeight: 'bold',
                                    }}>
                                    {'Keep me signed in '}
                                </Text>

                                <TouchableOpacity
                                    style={{
                                        flexDirection: 'row',
                                        borderColor: Assets.colors.vehicleTypeBg,
                                        paddingStart: 10,
                                        alignItems: 'center',
                                        position: 'absolute',
                                        right: 5,
                                    }}
                                    onPress={() => {
                                        this.setState({
                                            isLogin: false,
                                            isRegister: false,
                                            isForgot: true,
                                            isProfile: false,
                                            isVerify: false,
                                            isNewCred: false,

                                        });
                                    }}
                                >

                                    <Text
                                        style={{
                                            color: Assets.colors.grayText,
                                            fontSize: 10,
                                        }}>{'Forgot Password?'}
                                    </Text>
                                </TouchableOpacity>

                            </View>

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Register'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: false,
                                        isRegister: true,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />
                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    renderForgot() {
        return (
            <View style={styles.mainContainer}>
                <View>
                    <Logo/>
                    <TouchableOpacity
                        style={{
                            position: 'absolute', left: 10, top: 5,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                        <Image style={{width: 30, height: 30}} resizeMode={'contain'}
                               source={Assets.icons.Back}/>
                    </TouchableOpacity>
                </View>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column', justifyContent: 'center',
                            alignItems: 'center',
                        }}>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                alignItems: 'center', width: '90%', margin: 50, padding: 30, borderRadius: 5,
                                // shadowColor: '#000000', height: '100%', marginTop: 200,
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 3,
                                // },
                                // shadowRadius: 5,
                                // shadowOpacity: 1.0,
                            }}>

                            <Text style={styles.buttonTextStyle}>{'Forgot Password?'}</Text>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Email}/>
                                <TextInput
                                    keyboardType={'email-address'}
                                    placeholder={'Email'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            email: text,
                                        });
                                    }}
                                    value={this.state.email}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <Button1
                                buttonStyle={{marginTop: 30, padding: 5, width: '100%'}}
                                onPress={() => {
                                }}
                                text1={'Send'}
                                onPress={() => {
                                    this.validateForgot();
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Register'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: false,
                                        isRegister: true,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Login'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: true,
                                        isRegister: false,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }


    renderForgotNewCred() {
        return (
            <View style={styles.mainContainer}>
                <View>
                    <Logo/>
                    <TouchableOpacity
                        style={{
                            position: 'absolute', left: 10, top: 5,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                        <Image style={{width: 30, height: 30}} resizeMode={'contain'}
                               source={Assets.icons.HamB}/>
                    </TouchableOpacity>
                </View>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column', justifyContent: 'center',
                            alignItems: 'center',
                        }}>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                alignItems: 'center', width: '90%', margin: 50, padding: 30, borderRadius: 5,
                                // shadowColor: '#000000', height: '100%', marginTop: 200,
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 3,
                                // },
                                // shadowRadius: 5,
                                // shadowOpacity: 1.0,
                            }}>

                            <Text style={styles.buttonTextStyle}>{'New Credentials'}</Text>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'New password'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            newPass: text,
                                        });
                                    }}
                                    value={this.state.newPass}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 5, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Confirm password'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            confirmPass: text,
                                        });
                                    }}
                                    value={this.state.confirmPass}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <Button1
                                buttonStyle={{marginTop: 30, padding: 5, width: '100%'}}
                                onPress={() => {
                                }}
                                text1={'Send'}
                                onPress={() => {
                                    this.validatePassUpdate();
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Register'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: false,
                                        isRegister: true,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Login'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: true,
                                        isRegister: false,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }


    renderForgotVerify() {
        return (
            <View style={styles.mainContainer}>
                <View>
                    <Logo/>
                    <TouchableOpacity
                        style={{
                            position: 'absolute', left: 10, top: 5,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                        <Image style={{width: 30, height: 30}} resizeMode={'contain'}
                               source={Assets.icons.Back}/>
                    </TouchableOpacity>
                </View>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column', justifyContent: 'center',
                            alignItems: 'center',
                        }}>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                alignItems: 'center', width: '90%', margin: 50, padding: 30, borderRadius: 5,
                                // shadowColor: '#000000', height: '100%', marginTop: 200,
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 3,
                                // },
                                // shadowRadius: 5,
                                // shadowOpacity: 1.0,
                            }}>

                            <Text style={styles.buttonTextStyle}>{'Verification code sent to your email'}</Text>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Email}/>
                                <TextInput
                                    keyboardType={'number-pad'}
                                    placeholder={'Code'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            code: text,
                                        });
                                    }}
                                    value={this.state.code}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <Button1
                                buttonStyle={{marginTop: 30, padding: 5, width: '100%'}}
                                onPress={() => {
                                }}
                                text1={'Send'}
                                onPress={() => {
                                    this.validateCode();
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Register'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: false,
                                        isRegister: true,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Login'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: true,
                                        isRegister: false,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    renderRegister() {
        return (
            <View style={styles.mainContainer}>
                <Logo/>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column',
                            alignItems: 'center',
                        }}>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                alignItems: 'center', width: '90%', padding: 30, borderRadius: 5,
                                // shadowColor: '#000000',
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 3,
                                // },
                                // shadowRadius: 5,
                                // shadowOpacity: 1.0,
                            }}>

                            <Text style={styles.buttonTextStyle}>{'Register'}</Text>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Name}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'First Name'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            fName: text,
                                        });

                                    }}
                                    value={this.state.fName}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Name}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Last Name'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            lName: text,
                                        });
                                    }}
                                    value={this.state.lName}
                                    placeholderTextColor={"gray"}
                                    // maxLength={this.props.maxLength}

                                    style={styles.Input}/>
                            </View>


                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Phone}/>
                                <TextInput
                                    keyboardType={'phone-pad'}
                                    placeholder={'Phone'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            phone: text,
                                        });
                                    }}
                                    value={this.state.phone}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Email}/>
                                <TextInput
                                    keyboardType={'email-address'}
                                    placeholder={'Email'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            email: text,
                                        });
                                    }}
                                    value={this.state.email}
                                    placeholderTextColor={"gray"}
                                    // maxLength={this.props.maxLength}

                                    style={styles.Input}/>
                            </View>


                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={' Password '}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            password: text,
                                        });
                                    }}
                                    value={this.state.password}
                                    placeholderTextColor={"gray"}
                                    // maxLength={this.props.maxLength}

                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={' Confirm Password '}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            cPassword: text,
                                        });
                                    }}
                                    value={this.state.cPassword}
                                    placeholderTextColor={"gray"}
                                    // maxLength={this.props.maxLength}
                                    style={styles.Input}/>
                            </View>


                            <Button1
                                buttonStyle={{marginTop: 50, padding: 5, width: '100%'}}
                                onPress={() => {
                                }}
                                text1={'Register'}
                                onPress={() => {
                                    this.validateRegister();
                                }}
                            />

                            <Button1
                                buttonStyle={{
                                    marginTop: 1, width: '100%',
                                    backgroundColor: 'white', borderColor: Assets.colors.vehicleTypeBg, borderWidth: 1,
                                }}
                                textStyle={{color: Assets.colors.vehicleTypeBg}}
                                text1={'Login'}
                                onPress={() => {
                                    this.setState({
                                        isLogin: true,
                                        isRegister: false,
                                        isForgot: false,
                                        isProfile: false,
                                        isVerify: false,
                                        isNewCred: false,
                                    });
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    renderUpdateProfile() {
        return (
            <View style={styles.mainContainer}>
                <Logo/>
                <View
                    style={{flexDirection: 'row-reverse', margin: 5, width: '99%'}}>
                    <TouchableOpacity
                        style={{
                            flexDirection: 'row', margin: 5, padding: 7,
                            backgroundColor: 'white',
                            borderColor: Assets.colors.vehicleTypeBg,
                            borderWidth: 1, borderRadius: 30,
                        }}
                        onPress={() => {
                            this.userLogout();
                        }}
                    >
                        <Image style={{
                            width: 15, height: 15,
                            margin: 3, marginEnd: 1,
                        }}
                               resizeMode={'contain'}
                               source={Assets.icons.Logout}/>
                        <Text
                            style={{

                                fontSize: 13, color: Assets.colors.vehicleTypeBg,
                                fontWeight: 'bold',
                            }}>
                            Logout
                        </Text>
                    </TouchableOpacity>


                </View>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column',
                            alignItems: 'center',
                        }}>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                alignItems: 'center', width: '90%', padding: 30, borderRadius: 5,
                                // shadowColor: '#000000',
                                // shadowOffset: {
                                //     width: 0,
                                //     height: 3,
                                // },
                                // shadowRadius: 5,
                                // shadowOpacity: 1.0,
                            }}>

                            <Text style={styles.buttonTextStyle}>{'Profile'}</Text>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Name}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'First Name'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            fName: text,
                                        });
                                    }}
                                    value={this.state.fName}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}


                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Name}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Last Name'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            lName: text,
                                        });
                                    }}
                                    value={this.state.lName}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Phone}/>
                                <TextInput
                                    keyboardType={'phone-pad'}
                                    placeholder={'Phone'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            phone: text,
                                        });
                                    }}
                                    value={this.state.phone}
                                    maxLength={11}
                                    placeholderTextColor={"gray"}


                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Email}/>
                                <TextInput
                                    keyboardType={'email-address'}
                                    placeholder={'Email'}
                                    secureTextEntry={false}
                                    onChangeText={(text) => {
                                        this.setState({
                                            email: text,
                                        });
                                    }}
                                    value={this.state.email}
                                    editable={false}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Password'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            password: text,
                                        });
                                    }}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>

                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Confirm Password'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            cPassword: text,
                                        });
                                    }}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <View
                                style={{
                                    width: '100%', flexDirection: 'row',
                                    borderRadius: 30, borderWidth: 0.3, height: 35, marginTop: 20, alignItems: 'center',
                                }}>
                                <Image style={{width: 10, height: 10, marginStart: 15, marginTop: 0, marginEnd: 1}}
                                       resizeMode={'contain'}
                                       source={Assets.icons.Password}/>
                                <TextInput
                                    keyboardType={'default'}
                                    placeholder={'Old Password'}
                                    secureTextEntry={true}
                                    onChangeText={(text) => {
                                        this.setState({
                                            oldPass: text,
                                        });
                                    }}
                                    // maxLength={this.props.maxLength}
                                    placeholderTextColor={"gray"}

                                    style={styles.Input}/>
                            </View>


                            <Button1
                                buttonStyle={{marginTop: 50, padding: 5, width: '100%'}}
                                onPress={() => {
                                }}
                                text1={'Update'}
                                onPress={() => {
                                    this.validateUpdateProfile();
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                {this.state.isLogin ?
                    this.renderLogin() : this.state.isForgot ?
                        this.renderForgot() : this.state.isRegister ?
                            this.renderRegister() : this.state.isProfile ? this.renderUpdateProfile() : this.state.isVerify ?
                                this.renderForgotVerify(): this.state.isNewCred ?
                                    this.renderForgotNewCred():<View/>}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 9,
        marginStart: 5,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},
    buttonTextStyle: {
        color: Assets.colors.vehicleTypeBg,
        // marginTop: 0,
        margin: 20,
        fontWeight: 'bold',
        fontSize: 20,
    },
    Input: {
        color: '#464646',
        fontSize: 14,
        width: '80%',
        marginStart:10
    },

});
