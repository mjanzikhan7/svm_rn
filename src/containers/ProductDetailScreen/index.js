import React, {Component} from 'react';
import {View, Image, StyleSheet, Text, StatusBar, FlatList, TouchableOpacity, TextInput,Linking} from 'react-native';
import Logo from '../../components/LogoHeader';
import DropDown from '../../components/DropDown';
import Button from '../../components/Button';
import Assets from '../../assets';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {Picker} from 'native-base';
import {API} from '../../constants';
import Loader from '../../components/loader';
import Button1 from '../../components/Button1';
import SimpleToast from 'react-native-simple-toast';


export default class ProductDetailScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            selectedVehicle: ' Car',
            searchResultsNo: '25,350',
            isFinanceOption: false,
            isCreditCheck: false,
            isCheckHistory: true,

            isFeatures: true,
            isSellerNotes: false,
            isFinanceCheck: false,

            isFb: true,
            isYt: false,
            isTw: false,
            isIn: false,
            vId: this.props.navigation.getParam("id"),
            // vId: 26,

            coverImage: '',
            car_images: [
                /*      {
                          id: 0,
                          image: Assets.icons.CarThumb,
                          imageSelected: Assets.icons.CarThumb,

                      },
                      {
                          id: 1,
                          image: Assets.icons.CarThumb,
                          imageSelected: Assets.icons.CarThumb,

                      },

                      {
                          id: 2,
                          image: Assets.icons.CarThumb,
                          imageSelected: Assets.icons.CarThumb,

                      },
                      {
                          id: 3,
                          image: Assets.icons.CarThumb,
                          imageSelected: Assets.icons.CarThumb,

                      },
                      {
                          id: 4,
                          image: Assets.icons.CarThumb,
                          imageSelected: Assets.icons.CarThumb,

                      },
                      {
                          id: 5,
                          image: Assets.icons.CarThumb,
                          imageSelected: Assets.icons.CarThumb,
                      },*/
            ],
            brand_id: '',
            title: '',
            category: '',
            year: '',
            engine: '',
            mileage: '',
            owner: '',
            brand_name: '',
            brand_model_name: '',
            price: '',
            ulez: '',
            exterior_color: '',
            interior_color: '',
            body_type_name: '',
            fuel_types_name: '',
            transmission_type: '',
            drive_mode: '',
            condtions: '',
            history: null,
            mpg: null,
            phone: '',
            email: '',
            postcode: null,
            website: '',
            user_type: '',
            user_profile_image: '',
            registered_date: '',
            main_image_url: '',
            note: '',
            feature: [
                  /*"Bluetooth Hands Free",
                  " Alloy Wheels",
                  " £20 Road TAX",
                  " Electric Windows",
                  " Air Conditioning",
                  " Remote Central Locking",
                  " Finance Available",
                  " 12 Months Free AA Breakdown Cover",
                  " HPI Clear",
                  " Call us on 02079983379",
                  " MOT History",
                  " Power Assisted Steering",
                  " Electric Mirrors",
                  " Isofix Child Seat Anchor point",
                  " Full Service History",
                  " Front Fog Lamps",
                  " Colour Coded Bumpers",
                  " Colour Coded Mirrors",
                  " Alarm",
                  " Immobiliser",
                  " ABS",
                  " Traction Control",
                  " Centre Console",
                  " Driver Airbag",
                  " Passenger Airbag",
                  " Multiple Airbags",
                  " Cruise Control",
                  " Rear Wash Wipe",
                  " High level brake light",
                  " Computer",
                  " Rear Head Rests",
                  " Multi function Steering",
                  " Radio CD",
                  " CD Player",
                  " CD MP3 AUX",
                  " 2 Owners from New",
                  " Any Inspection Welcome",
                  " 17\" Alloys",
                  " AA Warranty Available",
                  " Excellent Bodywork And Interior",
                  " 5 days Insurance Cover Available",
                  " Part Exchange Welcome",
                  " www.mexcars.co.uk",
                  " MOT October 2020 With No Advisory",
                  " Low Group Tax",
                  " Low Group Insurance",
                  " More Extras.......",
                  " Spare Key",
                  " Just Been Serviced",
                  " More Cars In Stock",
                  " Rear Load Cover",
                  " "*/
            ],

            customerMessage:'',
            customerName:'',
            customerEmail:'',
            customerPhone:'',
            customerPostal:'',


        };
    };

    componentDidMount() {
        this.getProductDetail();
    }

    getProductDetail() {
        this.setState({loading: true});

        fetch(API.VehicleDetails + 'id=' + this.state.vId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',

            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    this.setState({
                        car_images: data.car_images,
                        brand_id: data.brand_id,
                        title: data.title,
                        category: data.category,
                        year: data.year,
                        engine: data.engine,
                        mileage: data.mileage,
                        owner: data.owner,
                        brand_name: data.brand_name,
                        brand_model_name: data.brand_model_name,
                        price: data.price,
                        ulez: data.ulez,
                        exterior_color: data.exterior_color,
                        interior_color: data.interior_color,
                        body_type_name: data.body_type_name,
                        fuel_types_name: data.fuel_types_name,
                        transmission_type: data.transmission_type,
                        drive_mode: data.drive_mode,
                        condtions: data.condtions,
                        history: data.history,
                        mpg: data.mpg,
                        phone: data.phone,
                        email: data.email,
                        postcode: data.postcode,
                        website: data.website+"",
                        user_type: data.user_type,
                        user_profile_image: data.user_profile_image,
                        registered_date: data.registered_date,
                        main_image_url: data.main_image_url,
                        note: data.note,
                        feature: data.feature,

                    });

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    openSmsUrl(phone: string, body: string): Promise<any> {
        return Linking.openURL(`sms:${phone}${this.getSMSDivider()}body=${body}`);
    }
    getSMSDivider(): string {
        return Platform.OS === "ios" ? "&" : "?";
    }

    render() {
        return (
            <View style={style.mainContainer}>
                <View>
                    <Logo/>
                    <TouchableOpacity
                        style={{
                            position:"absolute",left:10,top:5}}
                        onPress={()=>{
                            this.props.navigation.goBack()
                        }}>
                        <Image style={{width:30,height:30}}
                               resizeMode={'contain'}
                               source={Assets.icons.Back}/>
                    </TouchableOpacity>
                </View>
                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flexDirection: 'column',
                            width: '100%',
                            // alignItems:"center"
                        }}>
                        <View
                            style={{
                                width: '100%',
                                height: 230,
                                alignItems: 'center',
                            }}>
                            <Image
                                source={{uri: this.state.main_image_url}}
                                // source={Assets.images.SearchBg}
                                style={style.imagePreviewStyle}/>
                        </View>

                        <View style={style.styleVehicleTypes}>
                            <FlatList
                                style={{}}
                                data={this.state.car_images}
                                keyExtractor={item => item.id}
                                horizontal={true}
                                extraData={this.props}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}
                                removeClippedSubvisews={false}
                                renderItem={({item, index}) => {
                                    return this.renderCarImages(item, index);
                                }}
                            />
                        </View>

                        <View
                            style={{width: '100%', flexDirection: 'row', height: 30, backgroundColor: 'white'}}>

                            <TouchableOpacity
                                style={{
                                    flexDirection: 'row',
                                    padding: 5, position: 'absolute',
                                    right: 10,
                                    borderColor: Assets.colors.violet,
                                    borderWidth: 0.2, borderRadius: 10,
                                }}>

                                <Image source={Assets.icons.Insurance}
                                       style={{
                                           resizeMode: 'contain', width: 15, height: 15, marginEnd: 5,
                                       }}/>

                                <Text
                                    style={{
                                        color: Assets.colors.violet,

                                        fontSize: 9,
                                        fontWeight: 'bold',
                                    }}>
                                    {'Get an Insurance quote'}
                                </Text>

                            </TouchableOpacity>

                        </View>


                        <View
                            style={{
                                width: '100%', flexDirection: 'row',
                                backgroundColor: 'white', padding: 5,
                            }}>

                            <Text
                                style={{
                                    color: Assets.colors.black,
                                    fontSize: 20,
                                    fontWeight: 'bold',
                                    marginTop: -10,
                                }}>
                                {'Car Details'}
                            </Text>


                            <View
                                style={{
                                    flexDirection: 'row',
                                    position: 'absolute',
                                    right: 10,
                                }}>
                                <TouchableOpacity
                                    style={{
                                        borderColor: Assets.colors.toggleBg,
                                        borderWidth: 0.2,
                                        borderRadius: 10, padding: 5,
                                        marginEnd: 5,
                                        backgroundColor: this.state.isFinanceOption ? Assets.colors.toggleBg : Assets.colors.white,
                                    }}
                                    onPress={() => {
                                        this.toggleFinanceOption();

                                    }}>

                                    <Text
                                        style={{
                                            color: this.state.isFinanceOption ? Assets.colors.white : Assets.colors.toggleBg,
                                            fontSize: 8,
                                        }}>
                                        {'Finance option'}
                                    </Text>

                                </TouchableOpacity>


                                <TouchableOpacity
                                    style={{
                                        borderColor: Assets.colors.toggleBg,
                                        borderWidth: 0.2,
                                        borderRadius: 10, padding: 5,
                                        marginEnd: 5,
                                        backgroundColor: this.state.isCreditCheck ? Assets.colors.toggleBg : Assets.colors.white,
                                    }}
                                    onPress={() => {
                                        this.toggleCreditCheck();

                                    }}>

                                    <Text
                                        style={{
                                            color: this.state.isCreditCheck ? Assets.colors.white : Assets.colors.toggleBg,
                                            fontSize: 8,
                                        }}>
                                        {'Credit check'}
                                    </Text>

                                </TouchableOpacity>


                                <TouchableOpacity
                                    style={{
                                        borderColor: Assets.colors.toggleBg,
                                        borderWidth: 0.2,
                                        borderRadius: 10, padding: 5,
                                        marginEnd: 5,
                                        backgroundColor: this.state.isCheckHistory ? Assets.colors.toggleBg : Assets.colors.white,
                                    }}
                                    onPress={() => {
                                        this.toggleCheckHistory();


                                    }}>

                                    <Text
                                        style={{
                                            color: this.state.isCheckHistory ? Assets.colors.white : Assets.colors.toggleBg,
                                            fontSize: 8,
                                        }}>
                                        {'Check history'}
                                    </Text>

                                </TouchableOpacity>

                            </View>

                        </View>

                        {/*Car Details Section*/}

                        <View
                            style={{flexDirection: 'column', width: '100%', alignItems: 'center'}}>

                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle1}>
                                    <Image source={Assets.icons.Body}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Body'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.body_type_name != null ? this.state.body_type_name : 'N/A'}

                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle1}>
                                    <Image source={Assets.icons.Stock}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Stock ID'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {'N/A'}
                                    </Text>
                                </View>


                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.Millage}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Mileage'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.mileage != null ? this.state.mileage : 'N/A'}
                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.Mpg}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'MPG'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {'N/A'}
                                    </Text>
                                </View>


                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle1}>
                                    <Image source={Assets.icons.Fuel}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Fuel Type'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.fuel_types_name != null ? this.state.fuel_types_name : 'N/A'}

                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle1}>
                                    <Image source={Assets.icons.UlezGolden}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'ULEZ'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.ulez != null ? this.state.ulez : 'N/A'}

                                    </Text>
                                </View>


                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.Engine}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Engine'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.engine != null ? this.state.engine : 'N/A'}
                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.Drive}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Drive'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.drive_mode != null ? this.state.drive_mode : 'N/A'}

                                    </Text>
                                </View>

                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.Transmission}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Transmission'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.transmission_type != null ? this.state.transmission_type : 'N/A'}
                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.Like}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'CAT'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.category != null ? this.state.category : 'N/A'}
                                    </Text>
                                </View>

                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle1}>
                                    <Image source={Assets.icons.ExteriorColor}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Exterior Color'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.exterior_color != null ? this.state.exterior_color : 'N/A'}
                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle1}>
                                    <Image source={Assets.icons.Registration}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Registration'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.registered_date != null ? this.state.registered_date : 'N/A'}
                                    </Text>
                                </View>

                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.InteriorColor}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Interior Color'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.interior_color != null ? this.state.interior_color : 'N/A'}
                                    </Text>
                                </View>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.History}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'History'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.history != null ? this.state.history : 'N/A'}
                                    </Text>
                                </View>

                            </View>


                            <View
                                style={style.featureRowStyle}>

                                <View
                                    style={style.featureBgStyle2}>
                                    <Image source={Assets.icons.InteriorColor}
                                           style={style.featureIconStyle}/>
                                    <Text
                                        style={style.featureTextLabelStyle}>
                                        {'Owner'}
                                    </Text>
                                    <Text
                                        numberOfLines={1}
                                        style={style.featureTextValueStyle}>
                                        {this.state.owner != null ? this.state.owner : 'N/A'}
                                    </Text>
                                </View>

                                {/*<View
                                    style={style.featureBgStyle2}>

                                </View>*/}

                            </View>
                        </View>


                        {/*Customer Info Section*/}
                        <View
                            style={{
                                flexDirection: 'row', width: '96%',
                                alignItems: 'center',
                                justifyItems: 'center', backgroundColor: Assets.colors.featureBg2,
                                marginStart: 7, marginEnd: 5,
                                paddingBottom:10,
                                borderBottomColor: Assets.colors.vehicleTypeBg, borderBottomWidth: 3,
                            }}>

                            <View
                                style={style.userDetailBgStyle}>

                                <View
                                    style={{flexDirection: 'column'}}>
                                   {/* <Text
                                        style={style.userDetailTextLabelStyle}>
                                        {'Robins & Day'}
                                    </Text>
                                    <Text
                                        style={[style.userDetailTextLabelStyle, {fontSize: 5}]}>
                                        {'BY PSA RETAIL'}
                                    </Text>*/}
                                    <Image style={{width:130,height:70}} resizeMode={'contain'}
                                           source={{uri:this.state.user_profile_image}}/>
                                    <Text
                                        style={[style.userDetailTextLabelStyle, {fontSize: 8, marginTop: 3}]}>
                                        {this.state.user_type}
                                    </Text>

                                    <View
                                        style={{flexDirection: 'row'}}>
                                        <Text
                                            style={[style.userDetailTextLabelStyle, {fontSize: 8, marginTop: 3}]}>
                                            {/*{'4.5 Reviews 524'}*/}
                                        </Text>

                                    </View>
                                </View>
                            </View>


                            <View
                                style={[style.userDetailBgStyle2, {}]}>

                                <View
                                    style={{flexDirection: 'column'}}>

                                    <TouchableOpacity
                                        style={{flexDirection: 'row'}}
                                    onPress={()=>{

                                        var scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:';
                                        // var url = scheme + this.state.lat + ',' + this.state.long;
                                        // Linking.openURL(url);

                                        // Linking.openURL('https://www.google.com/maps:'+this.state.lat+","+this.state.long);

                                    }}>
                                        <Image source={Assets.icons.Location}
                                               style={style.featureIconStyle}/>
                                        <Text
                                            style={[style.userDetailTextLabelStyle2]}>
                                            {'Location Map'}
                                        </Text>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        style={{flexDirection: 'row'}}
                                    onPress={()=>{
                                        if(this.state.website.length>2){


                                        Linking.canOpenURL(this.state.website).then(supported => {
                                            if (supported) {
                                                Linking.openURL(this.state.website);
                                            } else {
                                                https://developers.facebook.com/docs/android/getting-started/
                                                    console.log('Don\'t know how to open URL: ' + this.state.website);
                                                SimpleToast.show('Don\'t know how to open URL: ' + this.state.website);
                                            }
                                        });
                                        }
                                    }}>
                                        <Image source={Assets.icons.Web}
                                               style={style.featureIconStyle}/>
                                        <Text
                                            style={[style.userDetailTextLabelStyle2]}>
                                            {this.state.website !== "null"?this.state.website:""}
                                        </Text>

                                    </TouchableOpacity>

                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                        }}>


                                        <TouchableOpacity
                                            style={{
                                                flexDirection: 'row',
                                                backgroundColor: Assets.colors.vehicleTypeBg,
                                                borderRadius: 14,
                                                alignItems: 'center',
                                                width: 90,
                                                marginEnd:10
                                            }}
                                        onPress={()=>{
                                            Linking.openURL(`tel:${this.state.phone}`);
                                        }}>

                                            <View
                                                style={{
                                                    backgroundColor: Assets.colors.white,
                                                    borderRadius: 20, margin: 2,
                                                }}>
                                                <Image source={Assets.icons.Contact}
                                                       style={[style.featureIconStyle, {width: 10, height: 10}]}/>
                                            </View>

                                            <Text
                                                style={[{
                                                    color: 'white', fontSize: 8,
                                                    margin: 5,
                                                }]}>
                                                {'Contact Seller'}
                                            </Text>

                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            style={{
                                                flexDirection: 'row',
                                                borderColor: Assets.colors.violet,
                                                borderWidth:0.5,
                                                borderRadius: 14,
                                                alignItems: 'center',
                                                width: 90,
                                            }}
                                        onPress={()=>{
                                            this.openSmsUrl(this.state.phone, "Your Message")
                                        }}>
                                            <View
                                                style={{
                                                    borderRadius: 20,
                                                }}>
                                                <Image source={Assets.icons.Message}
                                                       style={[style.featureIconStyle, {width: 20,
                                                           height: 20,margin: 1,
                                                       resizeMode: "cover"}]}/>
                                            </View>

                                            <Text
                                                style={[{
                                                    color: Assets.colors.violet, fontSize: 8,
                                                    margin: 5,
                                                }]}>
                                                {'Send Message'}
                                            </Text>

                                        </TouchableOpacity>




                                    </View>
                                </View>


                            </View>

                        </View>


                        {/*Second Selection Bar*/}

                        <View
                            style={{
                                width: '100%', flexDirection: 'row',
                                backgroundColor: 'white', padding: 5,
                                marginTop: 15,
                                height:30
                            }}>

                            {/*Options Section*/}
                            <View
                                style={{
                                    flexDirection: 'row',
                                    position: 'absolute',
                                    left: 7,
                                }}>

                                <TouchableOpacity
                                    style={{
                                        width:80,
                                        padding: 5,
                                        alignItems:"center",
                                        // borderColor: Assets.colors.toggleBg,
                                        // borderWidth: 0.2,
                                        borderRadius: 10,
                                        marginEnd: 5,
                                        backgroundColor: this.state.isFeatures ? Assets.colors.vehicleTypeBg : Assets.colors.featureBg2,
                                    }}
                                    onPress={() => {
                                        this.toggleFeatures();
                                    }}>

                                    <Text
                                        style={{
                                            color: this.state.isFeatures ? Assets.colors.white : Assets.colors.grayText,
                                            fontSize: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {'Features'}
                                    </Text>

                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{
                                        width:80,
                                        alignItems:"center",
                                        // borderColor: Assets.colors.toggleBg,
                                        // borderWidth: 0.2,
                                        borderRadius: 10, padding: 5,
                                        marginEnd: 5,
                                        backgroundColor: this.state.isSellerNotes ? Assets.colors.vehicleTypeBg : Assets.colors.featureBg2,
                                    }}
                                    onPress={() => {
                                        this.toggleSellerNote();

                                    }}>

                                    <Text
                                        style={{
                                            color: this.state.isSellerNotes ? Assets.colors.white : Assets.colors.grayText,
                                            fontSize: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {'Seller Notes'}
                                    </Text>

                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{
                                        width:80,
                                        alignItems:"center",
                                        // borderColor: Assets.colors.toggleBg,
                                        // borderWidth: 0.2,
                                        borderRadius: 10, padding: 5,
                                        marginEnd: 5,
                                        backgroundColor: this.state.isFinanceCheck ? Assets.colors.vehicleTypeBg : Assets.colors.featureBg2,
                                    }}
                                    onPress={() => {
                                        this.toggleFinance();


                                    }}>

                                    <Text
                                        style={{
                                            color: this.state.isFinanceCheck ? Assets.colors.white : Assets.colors.grayText,
                                            fontSize: 8,
                                        }}
                                        numberOfLines={1}
                                    >
                                        {'Contact Seller'}
                                        {/*{'Finance Check'}*/}
                                    </Text>

                                </TouchableOpacity>

                            </View>


                            {/*Social Share Section*/}
                            <View
                                style={{
                                    flexDirection: 'row',
                                    position: 'absolute',
                                    right: 7,
                                }}>

                                <Text
                                    style={{
                                        color: Assets.colors.grayText,
                                        fontSize: 8,margin:5
                                    }}>
                                    {'Share: '}
                                </Text>
                                <TouchableOpacity
                                    style={{
                                        alignItems:"center",
                                        marginEnd: 5,
                                    }}
                                    onPress={() => {
                                        this.toggleFb();
                                    }}>
                                    <Image
                                        source={this.state.isFb?Assets.icons.FbSelected:Assets.icons.FbNotSelected}
                                        style={{resizeMode:"contain",
                                        height:20, width:20}}/>

                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{
                                        alignItems:"center",
                                        marginEnd: 5,
                                    }}
                                    onPress={() => {
                                        this.toggleTw();
                                    }}>

                                    <Image
                                        source={this.state.isTw?Assets.icons.TwitterSelected:
                                            Assets.icons.TwitterNotSelected}
                                        style={{resizeMode:"contain",
                                            height:20, width:20}}/>

                                </TouchableOpacity>


                               {/* <TouchableOpacity
                                    style={{
                                        alignItems:"center",
                                        marginEnd:5
                                    }}
                                    onPress={() => {
                                        this.toggleIn();
                                    }}>

                                    <Image
                                        source={this.state.isIn?Assets.icons.InstaSelected:
                                            Assets.icons.InstaNotSelected}
                                        style={{resizeMode:"contain",
                                            height:20, width:20}}/>

                                </TouchableOpacity>*/}

                               {/* <TouchableOpacity
                                    style={{
                                        alignItems:"center",
                                        // marginEnd: 5,
                                    }}
                                    onPress={() => {
                                        this.toggleYt();
                                    }}>

                                    <Image
                                        source={this.state.isYt?Assets.icons.YtSelected:Assets.icons.YtNotSelected}
                                        style={{resizeMode:"contain",
                                        height:20, width:20}}/>

                                </TouchableOpacity>*/}
                            </View>

                        </View>

                        {/*Feature View*/}
                        {this.state.isFeatures?
                            <View style={style.styleRecentVehicles}>
                                <FlatList
                                    style={{width:"100%"}}
                                    data={this.state.feature}
                                    keyExtractor={item => item.id}
                                    horizontal={false}
                                    extraData={this.props}
                                    numColumns={2}
                                    showsHorizontalScrollIndicator={false}
                                    contentContainerStyle={{
                                        alignItems: 'center',
                                        justifyContent: 'center',

                                    }}
                                    removeClippedSubvisews={false}
                                    renderItem={({item, index}) => {
                                        return this.renderFeatures(item, index);
                                    }}
                                />
                            </View>:<View/>
                        }

                        {this.state.isSellerNotes?
                            <View style={style.styleNotesView}>
                                <Text
                                style={{fontSize:10, textAlign: "justify"}}>
                                    {this.state.note}
                                </Text>

                            </View>:<View/>
                        }

                        {this.state.isFinanceCheck?
                            <View style={style.styleFinanceCheckView}>
                                {/*Name and Email Section*/}
                                <View
                                style={{flexDirection:"row", width:"100%",
                                    alignItems:"center",margin:20}}>
                                    <View
                                        style={{
                                            flex:0.45, flexDirection: 'row',
                                            borderRadius: 30, height: 30,
                                            justifyContent:"center",marginEnd: 10,
                                            marginTop: 10, alignItems: 'center',backgroundColor:"white"
                                        }}>
                                        <Image style={{width: 8, height: 8, marginStart: 1, marginTop: 0, marginEnd: 1}}
                                               resizeMode={'contain'}
                                               source={Assets.icons.Name}/>
                                        <TextInput
                                            keyboardType={'default'}
                                            placeholder={'First Name, Last Name *'}
                                            secureTextEntry={false}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    customerName: text,
                                                });
                                            }}
                                            // maxLength={this.props.maxLength}


                                            style={style.Input}/>
                                    </View>


                                    <View
                                        style={{
                                            flex:0.45, flexDirection: 'row',
                                            borderRadius: 30,  height: 30,
                                            justifyContent:"center",
                                            marginTop: 10, alignItems: 'center',backgroundColor:"white"
                                        }}>
                                        <Image style={{width: 8, height: 8, marginStart: 1, marginTop: 0, marginEnd: 1}}
                                               resizeMode={'contain'}
                                               source={Assets.icons.Email}/>
                                        <TextInput
                                            keyboardType={'email-address'}
                                            placeholder={'Your Email Address *'}
                                            secureTextEntry={false}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    customerEmail: text,
                                                });
                                            }}
                                            // maxLength={this.props.maxLength}


                                            style={style.Input}/>
                                    </View>


                                </View>

                                {/*Postal code and Phone section*/}
                                <View
                                    style={{flexDirection:"row", width:"100%",
                                        alignItems:"center",margin:20,
                                        marginTop: -10}}>
                                    <View
                                        style={{
                                            flex:0.45, flexDirection: 'row',
                                            borderRadius: 30,  height: 30,
                                            justifyContent:"center",marginEnd: 10,
                                            marginTop: 0, alignItems: 'center',backgroundColor:"white"
                                        }}>
                                        <Image style={{width: 8, height: 8, marginStart: 1, marginTop: 0, marginEnd: 1}}
                                               resizeMode={'contain'}
                                               source={Assets.icons.Location}/>
                                        <TextInput
                                            keyboardType={'default'}
                                            placeholder={'Postal Code*'}
                                            secureTextEntry={false}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    customerPostal: text,
                                                });
                                            }}
                                            // maxLength={this.props.maxLength}


                                            style={style.Input}/>
                                    </View>


                                    <View
                                        style={{
                                            flex:0.45, flexDirection: 'row',
                                            borderRadius: 30, height: 30,
                                            justifyContent:"center",
                                            marginTop: 0, alignItems: 'center',backgroundColor:"white"
                                        }}>
                                        <Image style={{width: 8, height: 8, marginStart: 1, marginTop: 0, marginEnd: 1}}
                                               resizeMode={'contain'}
                                               source={Assets.icons.Phone}/>
                                        <TextInput
                                            keyboardType={'phone-pad'}
                                            placeholder={'Your Phone *'}
                                            secureTextEntry={false}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    customerPhone: text,
                                                });
                                            }}
                                             maxLength={11}


                                            style={style.Input}/>
                                    </View>


                                </View>

                                {/*Message section*/}
                                <View
                                    style={{flexDirection:"row",
                                        width:"100%",
                                        margin:20, marginTop: -10}}>
                                    <View
                                        style={{
                                            flex:0.9, flexDirection: 'row',
                                            borderRadius: 10,  height:60,
                                            marginTop: 0,backgroundColor:"white", alignItems:"center"
                                        }}>
                                        <TextInput
                                            keyboardType={'default'}
                                            placeholder={'Your Message *'}
                                            secureTextEntry={false}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    customerMessage: text,
                                                });
                                            }}
                                            multiline={true}
                                            // maxLength={this.props.maxLength}


                                            style={{
                                                maxHeight: 80,
                                                color: '#464646',
                                                fontSize: 10,
                                                position:"absolute",
                                                left:5,
                                                right:5,top:5,bottom:5,

                                            }}/>
                                    </View>

                                </View>

                                {/*Submit Button*/}
                                <Button1
                                    buttonStyle={{margin: 20, padding: 5, width: '90%'}}
                                    onPress={() => {
                                    }}
                                    text1={'Submit'}
                                    onPress={() => {
                                        this.validateUpdateProfile();
                                    }}
                                />

                            </View>:<View/>
                        }


                    </View>
                </KeyboardAwareScrollView>

                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    validateUpdateProfile() {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.state.customerName.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Name');
        }else if (this.state.customerEmail.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Email');
        } else if (reg.test(this.state.customerEmail) === false) {
            SimpleToast.show('Email Format Is Not Valid');
        } else if (this.state.customerPostal.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Postal Code');
        } else if (this.state.customerMessage.length === 0) {
            this.state.isValid = false;
            SimpleToast.show('Please Enter Message');
        }  else {
            this.state.isValid = true;
             this.contactSeller();

        }
    }

    contactSeller() {

        const {customerEmail,customerName,customerPhone,customerMessage,customerPostal} = this.state;
        this.setState({loading: true});
        console.log('contactSeller API URL:  ' + API.UpdateProfile);
        let requestBody = new FormData();
        requestBody.append('full_name', customerName);
        requestBody.append('postcode', customerPostal);
        requestBody.append('phone', customerPhone);
        requestBody.append('email', customerEmail);
        requestBody.append('message', customerMessage);
        requestBody.append('vehicle_id', this.state.vId);

        console.log(requestBody);
        fetch(API.ContactSeller, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                // 'Content-Type': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log(' contactSeller response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('contactSeller Response: ' + JSON.stringify(response));
            if (response.success) {
                this.setState({
                    loading: false,
                });
                SimpleToast.show(response.message);

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
                SimpleToast.show('Error: ' + response.message);
            }

        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    renderCarImages(item, index) {

        return (
            <TouchableOpacity
                onPress={() => {
                    this.setState({
                        main_image_url: item,
                    });
                }}
                style={{margin: 10, borderRadius: 5}}>
                <Image source={{uri: item}}
                       style={{resizeMode: 'contain', width: 100, height: 80, borderRadius: 30}}/>

            </TouchableOpacity>

        );
    }

    renderCheckHistory(item, index) {

        return (
            <View
                style={{
                    flexDirection: 'row',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    width: '45%',
                    // alignItems: 'center',
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 5, flex: 1,
                        justifyContent: 'center',
                    }}>
                    <Image source={Assets.icons.CarThumb}
                           style={{resizeMode: 'contain', width: 80, height: 80, borderRadius: 0}}/>
                </View>


                <Text
                    style={style.recentTextStyle1}>
                    2019 Volkswagen Jeet
                </Text>
                <Text
                    style={style.recentTextStyle2}>
                    $18,545
                </Text>

                <View
                    style={{flexDirection: 'row', marginTop: 10}}>
                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Road}
                               style={style.recentIconStyle}/>
                        <Text
                            style={style.recentTextStyle3}>
                            72,368
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Manual}
                               style={style.recentIconStyle}/>
                        <Text
                            style={style.recentTextStyle3}>
                            Manual
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Road}
                               style={style.recentIconStyle}/>
                        <Text
                            style={style.recentTextStyle3}>
                            72,368
                        </Text>

                    </View>


                </View>

                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        padding: 5, flex: 1,
                        justifyContent: 'center',
                        margin: -5,
                        marginTop: 5,
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        borderBottomStartRadius: 3,
                        borderBottomEndRadius: 3,
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('ProductDetailScreen');
                    }}>
                    <Text
                        style={style.recentTextStyle1}>
                        More Details
                    </Text>
                </TouchableOpacity>

            </View>

        );
    }
    renderFeatures(item, index) {

        return (
            <View
                style={{
                    flexDirection: 'row',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 3,
                    borderWidth: 0.2,
                    borderRadius: 3,
                    width: '48%',
                    backgroundColor:Assets.colors.featureBg1,
                    // backgroundColor:Assets.colors.vehicleTypeBg,
                     alignItems: 'center',
                }}>


                {/*<Image source={Assets.icons.Tick}
                       style={{resizeMode: 'contain',
                           width: 10, height: 10,
                           borderRadius: 0, margin:5}}/>*/}
                <Text
                    numberOfLines={1}
                    style={{color:"black", fontSize:8, margin:5, width:"70%"}}>
                    {item}
                </Text>



            </View>

        );
    }

    toggleFinanceOption() {
        this.setState({
            isFinanceOption: true,
            isCreditCheck: false,
            isCheckHistory: false,
        });
    }

    toggleCreditCheck() {
        this.setState({
            isFinanceOption: false,
            isCreditCheck: true,
            isCheckHistory: false,
        });
    }

    toggleCheckHistory() {
        this.setState({
            isFinanceOption: false,
            isCreditCheck: false,
            isCheckHistory: true,

        });
    }

    toggleFeatures() {
        this.setState({
            isFeatures: true,
            isSellerNotes: false,
            isFinanceCheck: false,
        });
    }

    toggleSellerNote() {
        this.setState({
            isFeatures: false,
            isSellerNotes: true,
            isFinanceCheck: false,
        });
    }

    toggleFinance() {
        this.setState({
            isFeatures: false,
            isSellerNotes: false,
            isFinanceCheck: true,

        });
    }


    toggleFb() {
        this.setState({
            isFb: true,
            isYt: false,
            isTw: false,
            isIn: false,
        });
    }

    toggleYt() {
        this.setState({
            isFb: false,
            isYt: true,
            isTw: false,
            isIn: false,
        });
    }

    toggleTw() {
        this.setState({
            isFb: false,
            isYt: false,
            isTw: true,
            isIn: false,

        });
    }
    toggleIn() {
        this.setState({
            isFb: false,
            isYt: false,
            isTw: false,
            isIn: true,

        });
    }


}
const style = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.white,

    },
    styleFeatureListView: {
        backgroundColor: Assets.colors.white,

    },
    styleNotesView: {
        backgroundColor: Assets.colors.featureBg2,
        marginStart:10,
        marginEnd:10,
        marginBottom:10,
        // height:50,
        padding:10,
    },
    styleFinanceCheckView: {
        backgroundColor: Assets.colors.featureBg2,
        flexDirection:"column",
    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 9,
        marginStart: 5,
    },
    imagePreviewStyle: {resizeMode: 'cover', width: '100%', height: '100%', borderRadius: 0},
    featureIconStyle: {resizeMode: 'contain', width: 15, height: 15, margin: 5},

    featureRowStyle: {flexDirection: 'row', width: '98%'},

    featureBgStyle1: {
        flexDirection: 'row', width: '48%', alignItems: 'center',
        backgroundColor: Assets.colors.featureBg1, margin: 3,
    },
    featureBgStyle2: {
        flexDirection: 'row', width: '48%', alignItems: 'center',
        backgroundColor: Assets.colors.featureBg2, margin: 3,
    },
    featureTextLabelStyle: {
        color: Assets.colors.black,
        fontSize: 9,
        fontWeight: 'bold',
        width: '40%',
    },
    featureTextValueStyle: {
        color: Assets.colors.black,
        fontSize: 9,
        fontWeight: 'bold',
        width: '35%',
        marginStart: 20,
        // height:30
    },
    userDetailBgStyle: {
        flexDirection: 'row', width: '40%', alignItems: 'center',
        margin: 3,
    },
    userDetailBgStyle2: {
        flexDirection: 'row',
        width: '57%', alignItems: 'center',
        margin: 3,
    },
    userDetailTextLabelStyle: {
        color: Assets.colors.grayText,
        fontSize: 16,
        width: '90%',
        fontWeight: 'bold',

    },
    userDetailTextLabelStyle2: {
        color: Assets.colors.vehicleTypeBg,
        width: '80%',
        fontSize: 9,
        fontWeight:"bold",
        margin: 5,

    },
    Input: {
        color: '#464646',
        fontSize: 10,
        marginBottom:-2,
        width: '80%',
    },

});

