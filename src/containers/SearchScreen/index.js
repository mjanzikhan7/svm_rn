import React, {Component, Fragment} from 'react';
import {View, Image, StyleSheet, Text, StatusBar, FlatList, TouchableOpacity, ScrollView} from 'react-native';
import Logo from '../../components/LogoHeader';
import DropDown from '../../components/DropDown';
import Button from '../../components/Button';
import Assets from '../../assets';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {Picker} from 'native-base';
import Preference from 'react-native-preference';
import {API} from '../../constants';
import SimpleToast from 'react-native-simple-toast';
import Loader from '../../components/loader';
import ScrollContentViewNativeComponent
    from 'react-native/Libraries/Components/ScrollView/ScrollContentViewNativeComponent';
import {SafeAreaView} from 'react-navigation';

// import {NavigationActions, StackActions, NavigationRoute} from 'react-navigation';
/*import Preference from 'react-native-preference';
import _ from 'lodash';*/


/*const resetAction = StackActions.reset({
    index: 0,
    // actions: [NavigationActions.navigate({routeName: 'UserBottomTab'})],
    actions: [NavigationActions.navigate({routeName: 'Landing'})],
});*/

let _unsubscribe;
export default class SearchScreen extends Component {
    constructor(props) {

        super(props);
        this.state = {
            /*
                        listVehicleType: [
                            {
                                id: 0,
                                image: Assets.icons.Car,
                                imageSelected: Assets.icons.CarSelected,
                                placeholder: 'Car',
                                value: 'car',
                                isSelected: false,
                            },
                            {
                                id: 1,
                                image: Assets.icons.Bike,
                                imageSelected: Assets.icons.BikeSelected,
                                placeholder: 'Bike',
                                value: 'bike',
                                isSelected: false,
                            },

                            {
                                id: 2,
                                image: Assets.icons.Van,
                                imageSelected: Assets.icons.VanSelected,
                                placeholder: 'Van',
                                value: 'van',
                                isSelected: false,
                            },
                            {
                                id: 3,
                                image: Assets.icons.Truck,
                                imageSelected: Assets.icons.TruckSelected,
                                placeholder: 'Truck',
                                value: 'truck',
                                isSelected: false,
                            },
                            {
                                id: 4,
                                image: Assets.icons.LongTruck,
                                imageSelected: Assets.icons.LongTruckSelected,
                                placeholder: 'Long Truck',
                                value: 'long_truck',
                                isSelected: false,
                            },
                            {
                                id: 5,
                                image: Assets.icons.Carvan,
                                imageSelected: Assets.icons.CarvanSelected,
                                placeholder: 'Caravan',
                                value: 'Caravan',
                                isSelected: false,
                            },


                        ],
            */
            loading: false,
            selectedVehicle: ' Car',
            /*searchResultsNo: this.props.navigation.getParam('totalCars', ''),
            vehicleList: this.props.navigation.getParam('vehicleList'),
            params: this.props.navigation.getParam('params'),
            nav: this.props.navigation.getParam('nav', 'search'),*/

            searchResultsNo: "",
            vehicleList: [],
            params: {},
            nav: "",
            offset: 0,
            isGrid: true,


        };
        this.onEndReachedCalledDuringMomentum = true;

    }

    /* componentDidMount() {
         // Listen for screen focus event
         this.props.navigation.addListener('didFocus', this.onScreenFocus)
     }

     // Called when our screen is focused
     onScreenFocus = () => {
         // Screen was focused, our on focus logic goes here
         this.reloadHomePage()
     }*/

    componentDidMount() {
        // SimpleToast.show("componentDidMount")


        const {navigation} = this.props;
        this.focusListener = navigation.addListener('didFocus', payload => {
            this.setState({
                nav: this.props.navigation.getParam('nav', 'search'),

            });
            setTimeout(() => {

                if (this.state.nav === 'search') {
                    // SimpleToast.show('componentDidMount focus listner call: Search')
                    this.searchVehicles(0);
                } else if (this.state.nav === 'filter') {
                    this.setState({
                        searchResultsNo: this.props.navigation.getParam('totalCars', ''),
                        vehicleList: this.props.navigation.getParam('vehicleList'),
                        params: this.props.navigation.getParam('params'),
                    });
                    // SimpleToast.show('componentDidMount focus listner call: Filter')
                } else if (this.state.nav === 'home') {
                    this.setState({
                        searchResultsNo: this.props.navigation.getParam('totalCars', ''),
                        vehicleList: this.props.navigation.getParam('vehicleList'),
                        params: this.props.navigation.getParam('params'),
                    });
                    // SimpleToast.show('componentDidMount focus listner call: Home');
                }

            }, 300);


            // this.myFun();

        });


        // const { navigation } = this.props;
        // _unsubscribe = navigation.addListener('focus', () => {
        //     SimpleToast.show("addListener")
        //
        //     /* setTimeout(() => {
        //          console.log("focusListener called")
        //          this.setState({
        //              isNoteList:true
        //          })
        //          this.getUserWhoCreateNotes()
        //      }, 1000);*/
        //     this.myFun()
        // });
        // this.getUserWhoCreateNotes()
    }


    myFun() {
        // SimpleToast.show(this.props.navigation.getParam('vehicleList').length + '');
        if (this.state.vehicleList != null && this.state.vehicleList.length > 0 &&
            this.props.navigation.getParam('vehicleList') != null && this.props.navigation.getParam('vehicleList').length > 0) {
            this.setState({
                vehicleList: this.props.navigation.getParam('vehicleList'),
            });

        } else {
            this.setState({
                vehicleList: [],
            });
            this.searchVehicles(0);
        }
        console.log('\n\n\n Received: ' + this.state.vehicleList);
    }

    searchVehicles(offset) {

        let newOffset = offset + this.state.offset;
        let navParams = this.state.params;
        this.setState({loading: true});

        let requestBody = new FormData();
        if (this.state.nav === 'search') {
            requestBody.append('offset', newOffset);
        } else if (this.state.nav === 'filter') {
            requestBody.append('vehicle_id', navParams.vehicle_id);
            requestBody.append('brand_id', navParams.brand_id);
            requestBody.append('model_id', navParams.model_id);
            requestBody.append('year', navParams.year);
            requestBody.append('mileage', navParams.mileage);
            requestBody.append('transmission_id', navParams.transmission_id);
            requestBody.append('minprice', navParams.minprice);
            requestBody.append('maxprice', navParams.maxprice);
            requestBody.append('feature', navParams.feature);
            requestBody.append('offset', (this.state.offset + offset));
        } else if (this.state.nav === 'home') {
            requestBody.append('vehicle_id', navParams.vehicle_id);
            requestBody.append('brand_id', navParams.brand_id);
            requestBody.append('model_id', navParams.model_id);
            requestBody.append('offset', (this.state.offset + offset));
        }


        console.log('\nParams:\n' + requestBody);
        fetch(API.Filter, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.success) {
                this.setState({loading: false});
                let data = response.data;

                if (data.result.length > 0) {
                    this.handleSearchItems(data.result);
                    this.setState({
                        // vehicleList: data.result,
                        searchResultsNo: data.total_match,
                    });
                } else {
                    SimpleToast.show('Data not found');
                }

            } else {
                console.log('Error: ' + response.message);
            }
            this.setState({
                loading: false,
                offset: newOffset,
            });
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    handleSearchItems(data) {
        let temp = this.state.vehicleList;
        /*temp = this.state.listVehicleMake;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }*/
        // SimpleToast.show("zxvs: "+data.length)
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
            // console.log("\n asdgasga"+data[x].id+"\t");

        }

        this.setState({
            vehicleList: temp,
        });
        /*  setTimeout(() => {
              console.log("\n\n\n>>> New listVehicleMake"+
                  JSON.stringify(this.state.listVehicleMake))
          },300)*/

    }

    onEndReached = ({distanceFromEnd}) => {
        // SimpleToast.show('sdstga');
        this.searchVehicles(10);

        if (!this.onEndReachedCalledDuringMomentum) {
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    render() {
        // this.forceUpdate()
        return (
            <View style={style.mainContainer}>
                <Logo/>
                <TouchableOpacity
                    style={{
                        position:"absolute",left:10,top:5}}
                    onPress={()=>{
                        this.props.navigation.openDrawer()
                    }}>
                    <Image style={{width:25,height:25, marginTop:2.5,
                        tintColor:Assets.colors.vehicleTypeBg}}
                           resizeMode={'contain'}
                           source={Assets.icons.HamB}/>
                </TouchableOpacity>

                {/*<View style={style.styleVehicleTypes}>
                    <FlatList
                        style={{}}
                        data={this.state.listVehicleType}
                        keyExtractor={item => item.id}
                        horizontal={true}
                        extraData={this.props}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        removeClippedSubvisews={false}
                        renderItem={({item, index}) => {
                            return this.renderVehicleTypes(item, index);
                        }}
                    />
                </View>*/}

                {/* <ScrollView
                    style={{flex: 1}}
                    // contentContainerStyle={{flex: 1}}
                >*/}
                {/*<View>*/}

                {/*    <View
                            style={style.styleRecentSection}>*/}

                <View
                    style={{flexDirection: 'row', margin: 10, paddingBottom: 15, flex: 1, height: '20%'}}>
                    <Text
                        style={{
                            color: 'black',
                            margin: 5,
                            fontSize: 13,
                            position: 'absolute',
                            left: 0,
                        }}>
                        {'Search Result: ' + this.state.searchResultsNo}
                    </Text>

                    <View
                        style={{
                            flexDirection: 'row',
                            borderColor: Assets.colors.vehicleTypeBg,
                            margin: 5,
                            paddingStart: 10,
                            paddingEnd: 10,
                            alignItems: 'center',
                            position: 'absolute',
                            right: 0,

                        }}>

                        <Text
                            style={{
                                color: 'black',
                                fontSize: 13,
                                marginEnd: 5,
                            }}>
                            Filter
                        </Text>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    isGrid: true,
                                });
                            }}>
                            <Image source={Assets.icons.Grid}
                                   style={{
                                       resizeMode: 'contain', width: 15, height: 15, marginEnd: 5,
                                   }}/>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                this.setState({
                                    isGrid: false,
                                });
                            }}>
                            <Image source={Assets.icons.List}
                                   style={{resizeMode: 'contain', width: 17, height: 17}}/>
                        </TouchableOpacity>


                    </View>

                </View>

                {/*<View style={style.styleRecentVehicles}>*/}
                <FlatList
                    style={{

                        // height: 3000,
                    }}
                    data={this.state.vehicleList}
                    keyExtractor={item => item.id}
                    horizontal={false}
                    extraData={this.state}
                    numColumns={this.state.isGrid ? 2 : 1}
                    key={(this.state.isGrid ? 2 : 1)}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{
                    }}
                    removeClippedSubvisews={false}
                    renderItem={({item, index}) => {
                        if (this.state.isGrid) {
                            return this.renderRecentlyViewed(item, index);
                        } else {
                            return this.renderListView(item, index);
                        }
                    }}
                    onEndReachedThreshold={0.4}
                    onEndReached={this.onEndReached.bind(this)}

                />
                {/*</View>*/}


                {/*</View>*/}

                {/*</View>*/}
                {/*</ScrollView>*/}
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }


    updateRecentlyViewedVehicles(item) {
        let newArray = [];

        if (Preference.get('recently') === undefined) {
            newArray.push(item);
        } else {
            // console.log(Preference.get("recently"))
            newArray = Preference.get('recently');
            newArray.push(item);
        }
        console.log('New Array: ' + JSON.stringify(newArray));
        // this.setState({
        //     recentlyViewedVehicles:newArray
        // })

        Preference.clear('recently');
        Preference.set('recently', newArray);

    }

    renderRecentlyViewed(item, index) {

        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'column',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    flex: 0.5,
                    // width: '45%',
                    // alignItems: 'center',
                }}
                onPress={() => {
                    this.updateRecentlyViewedVehicles(item);
                    this.props.navigation.navigate('ProductDetailScreen', {
                        id: item.id,
                    });
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 1, flex: 1,
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={item.image_url != null ? {uri: item.image_url} : {uri: item.main_image_url}}
                        // source={Assets.icons.CarThumb1}
                        style={{resizeMode: 'cover', width: 150, height: 120, borderRadius: 5}}/>
                </View>


                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle1}>
                    {item.title}

                </Text>
                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle2}>
                    {'£' + item.price}
                </Text>

                <View
                    style={{flexDirection: 'row', marginTop: 10}}>
                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Road}
                               style={[style.recentIconStyle, {marginTop: 1}]}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.mileage}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Manual}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.transmission_types}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Engine2}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.engine}
                        </Text>

                    </View>


                </View>


                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        padding: 5, flex: 1,
                        justifyContent: 'center',
                        margin: -5,
                        marginTop: 5,
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        borderBottomStartRadius: 3,
                        borderBottomEndRadius: 3,
                    }}
                    onPress={() => {
                        this.updateRecentlyViewedVehicles(item);

                        this.props.navigation.navigate('ProductDetailScreen', {
                            id: item.id,
                        });
                    }}>
                    <Text
                        style={style.recentTextStyle1}>
                        More Details
                    </Text>
                </TouchableOpacity>

            </TouchableOpacity>

        );
    }

    renderListView(item, index) {

        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'row',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    flex: 1,
                    // width: '45%',
                    // alignItems: 'center',
                }}
                onPress={() => {
                    this.updateRecentlyViewedVehicles(item);
                    this.props.navigation.navigate('ProductDetailScreen', {
                        id: item.id,
                    });
                }}>
                <View
                    style={{
                        flexDirection: 'column',
                        padding: 1,
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={item.image_url != null ? {uri: item.image_url} : {uri: item.main_image_url}}
                        // source={Assets.icons.CarThumb1}
                        style={{resizeMode: 'cover', width: 130, height: 100, borderRadius: 5}}/>
                </View>

                <View
                    style={{
                        flexDirection: 'column',
                        padding: 1, flex: 1, marginStart: 5,
                        // justifyContent: 'center',
                    }}>

                    <Text
                        numberOfLines={2}
                        style={[style.recentTextStyle1, {
                            margin: 5,
                        }]}>
                        {item.title}

                    </Text>

                    <Text
                        numberOfLines={1}
                        style={[style.recentTextStyle2, {
                            margin: 5,
                        }]}>
                        {'£' + item.price}
                    </Text>

                    <View
                        style={{flexDirection: 'row', marginTop: 10}}>
                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 0.3, padding: 5,
                            }}>
                            <Image source={Assets.icons.Road}
                                   style={[style.recentIconStyle, {marginTop: 1}]}/>
                            <Text
                                numberOfLines={1}
                                style={style.recentTextStyle3}>
                                {item.mileage}
                            </Text>

                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 0.3, padding: 5,
                            }}>
                            <Image source={Assets.icons.Manual}
                                   style={style.recentIconStyle}/>
                            <Text
                                numberOfLines={1}
                                style={style.recentTextStyle3}>
                                {item.transmission_types}
                            </Text>

                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 0.3, padding: 5,
                            }}>
                            <Image source={Assets.icons.Engine2}
                                   style={style.recentIconStyle}/>
                            <Text
                                numberOfLines={1}
                                style={style.recentTextStyle3}>
                                {item.engine}
                            </Text>

                        </View>


                    </View>

                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            padding: 5, flex: 1,
                            justifyContent: 'center',
                            margin: 5,
                            backgroundColor: Assets.colors.vehicleTypeBg,
                            borderRadius: 3,
                            // borderBottomStartRadius: 3,
                            // borderBottomEndRadius: 3,
                        }}
                        onPress={() => {
                            this.updateRecentlyViewedVehicles(item);

                            this.props.navigation.navigate('ProductDetailScreen', {
                                id: item.id,
                            });
                        }}>
                        <Text
                            style={style.recentTextStyle1}>
                            More Details
                        </Text>
                    </TouchableOpacity>
                </View>


            </TouchableOpacity>

        );
    }


}
const style = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,
        flexDirection: 'column',
        width: '100%',
        flex: 1,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
        flex: 1,
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,
        fontWeight: 'bold',

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,
        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 8,
        marginStart: 2,
        // width:40
        minWidth: 30,
        maxWidth: 45,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},

});

