import React, {Component} from 'react';
import {View, Image, StyleSheet, Text, StatusBar, TextInput, TouchableOpacity, FlatList} from 'react-native';
import Assets from '../../assets';
import Logo from '../../components/LogoHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Button1 from '../../components/Button1';
import {API} from '../../constants';
import SimpleToast from 'react-native-simple-toast';
import Loader from '../../components/loader';
import PackageButton from '../../components/PackageButton';
import PackageFeatureItem from '../../components/PackageFeatureItem';
import Preference from 'react-native-preference';


export default class Packages extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            password: '',
            email: '',
            isValid: false,
            fName: '',
            lName: '',
            cPassword: '',
            phone: '',
            oldPass: '',
            listVehicleType: [
                {
                    id: 1,
                    image: Assets.icons.Car,
                    imageSelected: Assets.icons.CarSelected,
                    placeholder: 'Car',
                    value: '1',
                    isSelected: true,
                },
                {
                    id: 2,
                    image: Assets.icons.Bike,
                    imageSelected: Assets.icons.BikeSelected,
                    placeholder: 'Motor Bike',
                    value: '2',
                    isSelected: false,
                },

                {
                    id: 3,
                    image: Assets.icons.Van,
                    imageSelected: Assets.icons.VanSelected,
                    placeholder: 'Van',
                    value: '3',
                    isSelected: false,
                },
                {
                    id: 4,
                    image: Assets.icons.Truck,
                    imageSelected: Assets.icons.TruckSelected,
                    placeholder: 'Motor Home',
                    value: '4',
                    isSelected: false,
                },
                {
                    id: 5,
                    image: Assets.icons.Carvan,
                    imageSelected: Assets.icons.CarvanSelected,
                    placeholder: 'Caravan',
                    value: '5',
                    isSelected: false,
                },
                {
                    id: 6,
                    image: Assets.icons.LongTruck,
                    imageSelected: Assets.icons.LongTruckSelected,
                    placeholder: 'Truck',
                    value: '6',
                    isSelected: false,
                },


            ],
            vehicleTypeList: [],
            priceRangelist: [],
            pricePlanslist: [],
            priceRangeImage: '',
            selectedVehicle: 'Car',
            selectedVehicleValue: '1',
            selectedPriceRangeValue: '',

            isSilver: false,
            isGold: false,
            isPlatinum: false,
            isDiamond: false,
            isPriceRange: true,
            vehicle_price_id:'',
            vehicle_plan_id:'',

            authToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvc2hpbnl2aWV3bW90b3JzLmNvbVwvc3RhZ2luZy1hcGlcL3B1YmxpY1wvYXBpXC92MVwvYXV0aFwvbG9naW4iLCJpYXQiOjE2MDQ3Njc5MTksImV4cCI6MTYwNjI3OTkxOSwibmJmIjoxNjA0NzY3OTE5LCJqdGkiOiJ1UkQ1RW5tS2hZdWhPTHVGIiwic3ViIjoyOCwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.injix5pJoL3PHnTcIGpxHxU12NUi4G8Q5g3mXcL8Bhw',


        };
    }

    componentDidMount() {
        /*   let userData = Preference.get('user')
           if (Preference.get('user')!= null){
               console.log("if")
               this.setState({
                   isProfile:true,
                   isLogin:false ,
                   isRegister: false,
                   isForgot: false,
                   authToken:userData.access_token,
                   email: userData.user_info.email,
                   fName: userData.user_info.first_name,
                   lName: userData.user_info.last_name,
                   phone: userData.user_info.phone,
               })
           }else{
               console.log("else")

               this.setState({
                   isProfile:false,
                   isLogin:true ,
                   isRegister: false,
                   isForgot: false,

               })
           }*/

        this.getVehicleTypes();
    }

    getVehicleTypes() {

        this.setState({loading: true});

        fetch(API.VehicleType, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
            },
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.success) {
                this.setState({
                    vehicleTypeList: response.data,
                    priceRangeImage: response.data[0].image_url,
                    priceRangelist: response.data[0].vehicle_price,
                    loading: false,
                });


            } else {

                this.setState({
                    loading: false,
                    vehicleTypeList: [],
                    priceRangelist: [],
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    getSubscriptionPlans() {

        this.setState({loading: true});

        fetch(API.SubscriptionPlans + this.state.selectedPriceRangeValue,
            {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
            }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.success) {
                let plans = [];
                for (let i = 0; i < response.data.length; i++) {
                    let tmp = {
                        id: response.data[i].id,
                        title: response.data[i].title,
                        days: response.data[i].days,
                        footer_header: response.data[i].footer_header,
                        price: response.data[i].price,
                        isChecked: false,
                        features: response.data[i].features,
                    };
                    plans.push(tmp);
                }
                this.setState({
                    pricePlanslist: plans,
                    isPriceRange: !this.state.isPriceRange,
                    loading: false,
                });


            } else {

                this.setState({
                    loading: false,
                    vehicleTypeList: [],
                    priceRangelist: [],
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    renderVehicleTypes(item, index) {

        return (
            <TouchableOpacity
                onPress={() => {
                    console.log(item.id);
                    let array = this.state.listVehicleType;
                    let array2 = [];
                    for (let i = 0; i < array.length; i++) {
                        let tmp = array[i];
                        if (tmp.id === item.id) {
                            tmp.isSelected = true;
                        } else {
                            tmp.isSelected = false;
                        }

                        array2.push(tmp);
                    }
                    this.setState({
                        listVehicleType: array2,
                        selectedVehicle: item.placeholder,
                        selectedVehicleValue: item.value,
                        priceRangeImage: this.state.vehicleTypeList[index].image_url,
                        priceRangelist: this.state.vehicleTypeList[index].vehicle_price,
                        isPriceRange: true,
                        // isPriceRange: !this.state.isPriceRange,
                    });
                    // if (this.state.pricePlanslist.length>0){
                    //     this.setState({
                    //         isPriceRange: !this.state.isPriceRange,
                    //     })
                    // }

                    // setTimeout(() => {
                    //     this.getVehicleBrands()
                    // }, 300);


                }}
                style={{
                    marginStart: this.state.width > 600 ? 10 : 0,
                    marginEnd: this.state.width > 600 ? 10 : 0,
                }}>
                {/*style={{marginStart:widthToDp("3.5%"), marginEnd:widthToDp('3.5%')}}>*/}
                <Image source={item.isSelected ? item.imageSelected : item.image}
                       style={{resizeMode: 'contain', width: 35, height: 35, borderRadius: 0}}/>
            </TouchableOpacity>

        );
    }

    renderPriceRangeItems(item, index) {

        return (
            <TouchableOpacity
                onPress={() => {
                    this.setState({
                        selectedPriceRangeValue: item.id,
                        vehicle_price_id:item.id
                    });

                    setTimeout(() => {
                        this.getSubscriptionPlans();
                    }, 300);


                }}
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: 130,
                    height: 150,
                    margin: 15,
                    padding: 10,
                    elevation: 3,
                    // borderWidth:0.2,
                    borderColor: 'white',
                }}>
                <Image source={{uri: this.state.priceRangeImage}}
                       style={{resizeMode: 'contain', width: 80, height: 80, borderRadius: 0}}/>

                <Text
                    style={{
                        width: '90%',
                        textAlign: 'center',
                        color: Assets.colors.vehicleTypeBg,
                        fontWeight: 'bold',
                    }}
                    multiline={2}>
                    {/*{item.title}*/}
                </Text>
            </TouchableOpacity>

        );
    }

    renderPricePlanItems(item, index) {

        return (
            <View
                style={{
                    alignItems: 'center',
                    justifyContent: 'center',
                    flex: 1,
                    flexDirection: 'column',

                    // width: '80%',
                    // height: 150,
                    // margin: 15,
                    // padding: 10,
                    // elevation: 3,
                    // // borderWidth:0.2,
                    // borderColor: 'white',
                }}>
                <PackageButton
                    text1={item.title}
                    text2={'Private Seller'}
                    arow={item.isChecked ? Assets.icons.ArrowDown : Assets.icons.ArrowRight}
                    arowStyle={item.isChecked ? {width: 20, height: 20} : {}}
                    buttonStyle={{
                        backgroundColor: item.isChecked ? Assets.colors.black : Assets.colors.vehicleTypeBg,
                        borderBottomLeftRadius: item.isChecked ? 0 : 5,
                        borderBottomRightRadius: item.isChecked ? 0 : 5,
                    }}
                    onPress={() => {
                        let changedList = this.state.pricePlanslist;
                        changedList[index].isChecked = !changedList[index].isChecked;
                        this.setState({
                            pricePlanslist: changedList,

                        });
                    }}
                />
                {item.isChecked ? this.renderPackageView(
                    item.id,
                    item.features,
                    item.footer_header,
                    item.price,
                    item.title) : <View/>}
            </View>

        );
    }

    renderPackageFeatureItem(item, index) {
// SimpleToast.show(""+item.name)
        return (
            <View
                style={{
                    /*alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',*/
                    // flex: 1,

                    width: '80%',
                    // height: 150,
                    // margin: 15,
                    // padding: 10,
                    // elevation: 3,
                    // // borderWidth:0.2,
                    // borderColor: 'white',
                }}>
                <PackageFeatureItem
                    icon={item.status ? Assets.icons.Tick1 : Assets.icons.Cross}
                    text1={'' + item.name}
                    iconStyle={{width: 10, height: 10}}
                    // textStyle
                />
            </View>

        );
    }


    renderPriceRanges() {
        return (
            <View style={[styles.mainContainer, {
                flex: 1,
                // justifyContent:"center", alignItems:"center"
            }]}>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',

                        }}>

                        <Text style={styles.buttonTextStyle}>{'Please select your car price'}</Text>

                        <View
                            style={{
                                flexDirection: 'column', justifyContent: 'center', width: '90%',
                            }}>


                            <FlatList
                                style={{}}
                                data={this.state.priceRangelist}
                                numColumns={2}
                                keyExtractor={item => item.id}
                                horizontal={false}
                                extraData={this.props}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{
                                    alignItems: 'center',
                                    justifyContent: 'space-evenly',
                                    width: '100%',
                                }}
                                removeClippedSubvisews={false}
                                renderItem={({item, index}) => {
                                    return this.renderPriceRangeItems(item, index);
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    renderPackageView(planId,features, footer, price, planName) {
        // SimpleToast.show(""+features.length)
        return (<View
            style={{
                backgroundColor: Assets.colors.featureBg1,
                flexDirection: 'column',
                alignItems: 'center',
                flex: 0.8,
                width: '80%',
                marginBottom: 10,
            }}>
            <FlatList
                style={{}}
                data={features}
                numColumns={1}
                key={(1)}
                keyExtractor={item => item.id}
                horizontal={false}
                extraData={this.props}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                    // alignItems: 'center',
                    // justifyContent: 'space-evenly',
                    // width: '100%',
                }}
                removeClippedSubvisews={false}
                renderItem={({item, index}) => {
                    return this.renderPackageFeatureItem(item, index);
                }}
            />


            <Text style={{
                color: Assets.colors.black,
                // marginTop: 0,
                margin: 5,
                fontWeight: 'bold',
                fontSize: 18,
            }}>
                {" "}
                {/*{'£' + price}*/}
            </Text>

            <Button1
                buttonStyle={{
                    marginTop: 10, width: '50%',
                    backgroundColor: 'black',
                    // borderColor:Assets.colors.vehicleTypeBg,borderWidth:1
                }}
                textStyle={{color: Assets.colors.white}}
                text1={'Get Now'}
                onPress={() => {
                    // SimpleToast.show('ddsfs');
                    this.setState({
                        vehicle_plan_id:planId
                    })
                    this.props.navigation.navigate('BuildUp',{
                        selectedVehicle: this.state.selectedVehicle,
                        selectedVehicleValue: this.state.selectedVehicleValue,
                        vehicle_plan_id:planId,
                        vehicle_price_id:this.state.vehicle_price_id,
                        planPrice:price,
                        planName:planName
                    });
                }}
            />

            <Text style={{
                color: Assets.colors.black,
                // marginTop: 0,
                margin: 15,
                fontSize: 12,
            }}>{'' + footer}</Text>


        </View>);
    }

    renderLogin() {
        return (
            <View style={styles.mainContainer}>

                <KeyboardAwareScrollView>
                    <View
                        style={{
                            flex: 1, flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',

                        }}>
                        <View
                            style={{width: '100%'}}>
                            <Text style={styles.buttonTextStyle}>{'Packages'}</Text>
                        </View>

                        <View
                            style={{
                                flex: 1, flexDirection: 'column', justifyContent: 'center',
                                width: '80%',
                            }}>

                            <FlatList
                                style={{}}
                                data={this.state.pricePlanslist}
                                numColumns={1}
                                key={(1)}
                                keyExtractor={item => item.id}
                                horizontal={false}
                                extraData={this.props}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{
                                    // alignItems: 'center',
                                    // justifyContent: 'space-evenly',
                                    // width: '100%',
                                }}
                                removeClippedSubvisews={false}
                                renderItem={({item, index}) => {
                                    return this.renderPricePlanItems(item, index);
                                }}
                            />


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Logo/>
                <View style={styles.styleVehicleTypes}>
                    <FlatList
                        style={{}}
                        data={this.state.listVehicleType}
                        keyExtractor={item => item.id}
                        horizontal={true}
                        extraData={this.props}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            alignItems: 'center',
                            justifyContent: 'space-evenly',
                            width: '100%',
                        }}
                        removeClippedSubvisews={false}
                        renderItem={({item, index}) => {
                            return this.renderVehicleTypes(item, index);
                        }}
                    />
                </View>
                {this.state.isPriceRange ? this.renderPriceRanges() : this.renderLogin()}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 9,
        marginStart: 5,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},
    buttonTextStyle: {
        color: Assets.colors.vehicleTypeBg,
        margin: 20,
        fontWeight: 'bold',
        fontSize: 20,
    },
    Input: {
        color: '#464646',
        fontSize: 10,
        width: '80%',
    },

});
