import React, {Component} from 'react';
import {View, Image, StyleSheet, Text, StatusBar, FlatList, TouchableOpacity, ScrollView} from 'react-native';
import Logo from '../../components/LogoHeader';
import DropDown from '../../components/DropDown';
import Button from '../../components/Button';
import Assets from '../../assets';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {Picker} from 'native-base';
import Preference from 'react-native-preference';
import {API} from '../../constants';
import SimpleToast from 'react-native-simple-toast';
import Loader from '../../components/loader';

// import {NavigationActions, StackActions, NavigationRoute} from 'react-navigation';
/*import Preference from 'react-native-preference';
import _ from 'lodash';*/


/*const resetAction = StackActions.reset({
    index: 0,
    // actions: [NavigationActions.navigate({routeName: 'UserBottomTab'})],
    actions: [NavigationActions.navigate({routeName: 'Landing'})],
});*/


export default class SearchScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            /*
                        listVehicleType: [
                            {
                                id: 0,
                                image: Assets.icons.Car,
                                imageSelected: Assets.icons.CarSelected,
                                placeholder: 'Car',
                                value: 'car',
                                isSelected: false,
                            },
                            {
                                id: 1,
                                image: Assets.icons.Bike,
                                imageSelected: Assets.icons.BikeSelected,
                                placeholder: 'Bike',
                                value: 'bike',
                                isSelected: false,
                            },

                            {
                                id: 2,
                                image: Assets.icons.Van,
                                imageSelected: Assets.icons.VanSelected,
                                placeholder: 'Van',
                                value: 'van',
                                isSelected: false,
                            },
                            {
                                id: 3,
                                image: Assets.icons.Truck,
                                imageSelected: Assets.icons.TruckSelected,
                                placeholder: 'Truck',
                                value: 'truck',
                                isSelected: false,
                            },
                            {
                                id: 4,
                                image: Assets.icons.LongTruck,
                                imageSelected: Assets.icons.LongTruckSelected,
                                placeholder: 'Long Truck',
                                value: 'long_truck',
                                isSelected: false,
                            },
                            {
                                id: 5,
                                image: Assets.icons.Carvan,
                                imageSelected: Assets.icons.CarvanSelected,
                                placeholder: 'Caravan',
                                value: 'Caravan',
                                isSelected: false,
                            },


                        ],
            */
            loading: false,
            selectedVehicle: ' Car',
            searchResultsNo: this.props.navigation.getParam('totalCars', ''),
            vehicleList: this.props.navigation.getParam('vehicleList'),
            params: this.props.navigation.getParam('params'),
            nav: this.props.navigation.getParam('nav', 'search'),
            offset: 0,
            isGrid: true,


        };
        this.onEndReachedCalledDuringMomentum = true;


    }

    componentDidMount() {
        if (this.state.vehicleList != null && this.state.vehicleList.length > 0) {

        } else {
            this.searchVehicles(0);
        }
        console.log('\n\n\n Received: ' + this.state.vehicleList);
    }

    searchVehicles(offset) {

        let navParams = this.state.params;
        this.setState({loading: true});

        let requestBody = new FormData();
        if (this.state.nav === 'search') {
            requestBody.append('offset', (this.state.offset + offset));
        } else if (this.state.nav === 'filter') {
            requestBody.append('vehicle_id', navParams.vehicle_id);
            requestBody.append('brand_id', navParams.brand_id);
            requestBody.append('model_id', navParams.model_id);
            requestBody.append('year', navParams.year);
            requestBody.append('mileage', navParams.mileage);
            requestBody.append('transmission_id', navParams.transmission_id);
            requestBody.append('minprice', navParams.minprice);
            requestBody.append('maxprice', navParams.maxprice);
            requestBody.append('feature', navParams.feature);
            requestBody.append('offset', (this.state.offset + offset));
        } else if (this.state.nav === 'home') {
            requestBody.append('vehicle_id', navParams.vehicle_id);
            requestBody.append('brand_id', navParams.brand_id);
            requestBody.append('model_id', navParams.model_id);
            requestBody.append('offset', (this.state.offset + offset));
        }


        console.log('\nParams:\n' + requestBody);
        fetch(API.Filter, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.success) {
                this.setState({loading: false});
                let data = response.data;

                if (data.result.length > 0) {
                    this.setState({
                        vehicleList: data.result,
                        searchResultsNo: data.total_match,
                    });
                } else {
                    SimpleToast.show('Data not found');
                }

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }


    onEndReached = ({distanceFromEnd}) => {
        if (!this.onEndReachedCalledDuringMomentum) {
            SimpleToast.show('sdstga');
            this.searchVehicles(10);
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    render() {
        return (
            <View style={style.mainContainer}>
                <Logo/>
                {/*<View style={style.styleVehicleTypes}>
                    <FlatList
                        style={{}}
                        data={this.state.listVehicleType}
                        keyExtractor={item => item.id}
                        horizontal={true}
                        extraData={this.props}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                        removeClippedSubvisews={false}
                        renderItem={({item, index}) => {
                            return this.renderVehicleTypes(item, index);
                        }}
                    />
                </View>*/}

                <KeyboardAwareScrollView>
                    <View>

                        <View
                            style={style.styleRecentSection}>

                            <View
                                style={{flexDirection: 'row', margin: 10, paddingBottom: 15, flex:1}}>
                                <Text
                                    style={{
                                        color: 'black',
                                        margin: 5,
                                        fontSize: 13,
                                        position: 'absolute',
                                        left: 0,
                                    }}>
                                    {'Search Result: ' + this.state.searchResultsNo}
                                </Text>

                                <View
                                    style={{
                                        flexDirection: 'row',
                                        borderColor: Assets.colors.vehicleTypeBg,
                                        margin: 5,
                                        paddingStart: 10,
                                        paddingEnd: 10,
                                        alignItems: 'center',
                                        position: 'absolute',
                                        right: 0,

                                    }}>

                                    <Text
                                        style={{
                                            color: 'black',
                                            fontSize: 13,
                                            marginEnd: 5,
                                        }}>
                                        Filter
                                    </Text>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                isGrid: true,
                                            });
                                        }}>
                                        <Image source={Assets.icons.Grid}
                                               style={{
                                                   resizeMode: 'contain', width: 15, height: 15, marginEnd: 5,
                                               }}/>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => {
                                            this.setState({
                                                isGrid: false,
                                            });
                                        }}>
                                        <Image source={Assets.icons.List}
                                               style={{resizeMode: 'contain', width: 17, height: 17}}/>
                                    </TouchableOpacity>


                                </View>

                            </View>

                            <View style={style.styleRecentVehicles}>
                                <FlatList
                                    style={{flex: 1, flexGrow: 1}}
                                    data={this.state.vehicleList}
                                    keyExtractor={item => item.id}
                                    horizontal={false}
                                    extraData={this.state}
                                    numColumns={this.state.isGrid ? 2 : 1}
                                    key={(this.state.isGrid ? 2 : 1)}
                                    showsHorizontalScrollIndicator={false}
                                    contentContainerStyle={{
                                        // alignItems: 'center',
                                        justifyContent: 'center', flex: 1,
                                    }}
                                    removeClippedSubvisews={false}
                                    renderItem={({item, index}) => {
                                        if (this.state.isGrid) {
                                            return this.renderRecentlyViewed(item, index);
                                        } else {
                                            return this.renderListView(item, index);
                                        }
                                    }}
                                    /* ListFooterComponent={() => {
                                         return (<View>
                                           <Text>Loading</Text>
                                         </View>);
                                     }}*/
                                    onMomentumScrollBegin={() => {
                                        this.onEndReachedCalledDuringMomentum = false;
                                    }}
                                    onEndReachedThreshold={0.5}
                                    onEndReached={this.onEndReached.bind(this)}

                                />
                            </View>


                        </View>

                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    renderVehicleTypes(item, index) {

        return (
            <TouchableOpacity
                onPress={() => {
                    console.log(item.id);
                    let array = this.state.listVehicleType;
                    let array2 = [];
                    for (let i = 0; i < array.length; i++) {
                        let tmp = array[i];
                        if (tmp.id === item.id) {
                            tmp.isSelected = !tmp.isSelected;
                        } else {
                            tmp.isSelected = false;
                        }

                        array2.push(tmp);
                    }
                    this.setState({
                        listVehicleType: array2,
                        selectedVehicle: item.placeholder,
                    });


                }}
                style={{marginStart: 20}}>
                <Image source={item.isSelected ? item.imageSelected : item.image}
                       style={{resizeMode: 'contain', width: 35, height: 35, borderRadius: 0}}/>
            </TouchableOpacity>

        );
    }

    updateRecentlyViewedVehicles(item) {
        let newArray = [];

        if (Preference.get('recently') === undefined) {
            newArray.push(item);
        } else {
            // console.log(Preference.get("recently"))
            newArray = Preference.get('recently');
            newArray.push(item);
        }
        console.log('New Array: ' + JSON.stringify(newArray));
        // this.setState({
        //     recentlyViewedVehicles:newArray
        // })

        Preference.clear('recently');
        Preference.set('recently', newArray);

    }

    renderRecentlyViewed(item, index) {

        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'column',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    flex: 0.5,
                    // width: '45%',
                    // alignItems: 'center',
                }}
                onPress={() => {
                    this.updateRecentlyViewedVehicles(item);
                    this.props.navigation.navigate('ProductDetailScreen', {
                        id: item.id,
                    });
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 1, flex: 1,
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={item.image_url != null ? {uri: item.image_url} : {uri: item.main_image_url}}
                        // source={Assets.icons.CarThumb1}
                        style={{resizeMode: 'cover', width: 150, height: 120, borderRadius: 5}}/>
                </View>


                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle1}>
                    {item.title}

                </Text>
                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle2}>
                    {'£' + item.price}
                </Text>

                <View
                    style={{flexDirection: 'row', marginTop: 10}}>
                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Road}
                               style={[style.recentIconStyle, {marginTop: 1}]}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.mileage}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Manual}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.transmission_types}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Ulez}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.ulez}
                        </Text>

                    </View>


                </View>


                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        padding: 5, flex: 1,
                        justifyContent: 'center',
                        margin: -5,
                        marginTop: 5,
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        borderBottomStartRadius: 3,
                        borderBottomEndRadius: 3,
                    }}
                    onPress={() => {
                        this.updateRecentlyViewedVehicles(item);

                        this.props.navigation.navigate('ProductDetailScreen', {
                            id: item.id,
                        });
                    }}>
                    <Text
                        style={style.recentTextStyle1}>
                        More Details
                    </Text>
                </TouchableOpacity>

            </TouchableOpacity>

        );
    }


    renderListView(item, index) {

        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'row',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    flex: 1,
                    // width: '45%',
                    // alignItems: 'center',
                }}
                onPress={() => {
                    this.updateRecentlyViewedVehicles(item);
                    this.props.navigation.navigate('ProductDetailScreen', {
                        id: item.id,
                    });
                }}>
                <View
                    style={{
                        flexDirection: 'column',
                        padding: 1,
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={item.image_url != null ? {uri: item.image_url} : {uri: item.main_image_url}}
                        // source={Assets.icons.CarThumb1}
                        style={{resizeMode: 'cover', width: 130, height: 100, borderRadius: 5}}/>
                </View>

                <View
                    style={{
                        flexDirection: 'column',
                        padding: 1, flex: 1, marginStart: 5,
                        // justifyContent: 'center',
                    }}>

                    <Text
                        numberOfLines={2}
                        style={[style.recentTextStyle1, {
                            margin: 5,
                        }]}>
                        {item.title}

                    </Text>

                    <Text
                        numberOfLines={1}
                        style={[style.recentTextStyle2,{
                            margin: 5,
                        }]}>
                        {'£' + item.price}
                    </Text>

                    <View
                        style={{flexDirection: 'row', marginTop: 10}}>
                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 0.3, padding: 5,
                            }}>
                            <Image source={Assets.icons.Road}
                                   style={[style.recentIconStyle, {marginTop: 1}]}/>
                            <Text
                                numberOfLines={1}
                                style={style.recentTextStyle3}>
                                {item.mileage}
                            </Text>

                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 0.3, padding: 5,
                            }}>
                            <Image source={Assets.icons.Manual}
                                   style={style.recentIconStyle}/>
                            <Text
                                numberOfLines={1}
                                style={style.recentTextStyle3}>
                                {item.transmission_types}
                            </Text>

                        </View>

                        <View
                            style={{
                                flexDirection: 'row',
                                flex: 0.3, padding: 5,
                            }}>
                            <Image source={Assets.icons.Ulez}
                                   style={style.recentIconStyle}/>
                            <Text
                                numberOfLines={1}
                                style={style.recentTextStyle3}>
                                {item.ulez}
                            </Text>

                        </View>


                    </View>

                    <TouchableOpacity
                        style={{
                            flexDirection: 'row',
                            padding: 5, flex: 1,
                            justifyContent: 'center',
                            margin: 5,
                            backgroundColor: Assets.colors.vehicleTypeBg,
                            borderRadius: 3,
                            // borderBottomStartRadius: 3,
                            // borderBottomEndRadius: 3,
                        }}
                        onPress={() => {
                            this.updateRecentlyViewedVehicles(item);

                            this.props.navigation.navigate('ProductDetailScreen', {
                                id: item.id,
                            });
                        }}>
                        <Text
                            style={style.recentTextStyle1}>
                            More Details
                        </Text>
                    </TouchableOpacity>
                </View>


            </TouchableOpacity>

        );
    }


}
const style = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,
        flexDirection: 'column',
        width: '100%',
        flex: 1,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
        flex:1
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,
        fontWeight: "bold",

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,
        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 8,
        marginStart: 2,
        // width:40
        minWidth: 30,
        maxWidth: 45,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},

});

