import React, {Component} from 'react';
import {View, Image, StyleSheet, Text, StatusBar, FlatList, TouchableOpacity, TextInput} from 'react-native';
import Logo from '../../components/LogoHeader';
import DropDown from '../../components/DropDown';
import Button from '../../components/Button';
import Assets from '../../assets';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {Picker} from 'native-base';
import Button1 from '../../components/Button1';
import AppInputField from '../../components/AppInputField';

// import {NavigationActions, StackActions, NavigationRoute} from 'react-navigation';
/*import Preference from 'react-native-preference';
import _ from 'lodash';*/


/*const resetAction = StackActions.reset({
    index: 0,
    // actions: [NavigationActions.navigate({routeName: 'UserBottomTab'})],
    actions: [NavigationActions.navigate({routeName: 'Landing'})],
});*/


export default class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {



        };

    }

    componentDidMount() {
    }


    render() {
        return (
            <View style={styles.mainContainer}>
                <Logo/>

                <KeyboardAwareScrollView>
                    <View
                    style={{flex:1,flexDirection:"column", justifyContent:"center",
                        alignItems:"center",
                        }}>

                      <View
                          style={{flex:1,flexDirection:"column", justifyContent:"center",
                              alignItems:"center", width: "90%", margin:50, padding:30, borderRadius: 5,
                              shadowColor: '#000000',
                              shadowOffset: {
                                  width: 0,
                                  height: 3
                              },
                              shadowRadius: 5,
                              shadowOpacity: 1.0}}>

                          <Text style={styles.buttonTextStyle}>{"Login"}</Text>

                          <View
                              style={{width:"100%",padding:5,flexDirection:"row",
                                  borderRadius: 30,borderWidth:0.2, height: 45, marginTop: 20}}>
                              <Image style={{width:15,height:15,marginStart:15,marginTop: 7,marginEnd:1,}}
                                     resizeMode={'contain'}
                                     source={Assets.icons.Email}/>
                              <TextInput
                                  keyboardType={"email-address"}
                                  placeholder={"Email"}
                                  secureTextEntry={false}
                                  onChangeText={(text)=>{
                                  }
                                  }
                                  // maxLength={this.props.maxLength}

                                  style={styles.Input}/>
                          </View>

                          <View
                              style={{width:"100%",padding:5,flexDirection:"row",
                                  borderRadius: 30,borderWidth:0.2, height: 45, marginTop: 20}}>
                              <Image style={{width:15,height:15,marginStart:15,marginTop: 7,marginEnd:1,}} resizeMode={'contain'}
                                     source={Assets.icons.Password}/>
                              <TextInput
                                  keyboardType={"default"}
                                  placeholder={"Password"}
                                  secureTextEntry={true}
                                  onChangeText={(text)=>{
                                  }}
                                  // maxLength={this.props.maxLength}

                                  style={styles.Input}/>
                          </View>


                          <Button1
                              buttonStyle={{marginTop: 50,padding:10, width:"100%"}}
                              onPress={() => {}}
                              text1={'Login'}
                              // onPress={()=>{
                              //     this.props.navigation.navigate("UserBottomTab")
                              // }}
                          />

                          <View
                              style={{flexDirection: 'row', margin: 10, paddingBottom: 15, width:"100%"}}>
                              <TouchableOpacity>
                                  <Image source={Assets.icons.Checked}
                                         style={{resizeMode: 'contain', width: 15, height: 15,marginEnd:5
                                         }}/>
                              </TouchableOpacity>
                              <Text
                                  style={{
                                      color: Assets.colors.vehicleTypeBg,

                                      fontSize: 14,
                                      fontWeight:"bold"
                                  }}>
                                  {"Keep me signed in "}
                              </Text>

                              {/*<View
                                  style={{
                                      flexDirection:"row",
                                      borderColor: Assets.colors.vehicleTypeBg,
                                      margin: 5,
                                      paddingStart: 10,
                                      paddingEnd: 10,
                                      alignItems: 'center',
                                      position: 'absolute',
                                      right: 0,

                                  }}>

                                  <Text
                                      style={{
                                          color: Assets.colors.grayText,
                                          fontSize: 14,
                                      }}>{"Forgot Password?"}
                                  </Text>
                              </View>*/}

                          </View>
                      </View>





                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }





}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 9,
        marginStart: 5,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},
    buttonTextStyle: {
        color:Assets.colors.vehicleTypeBg,
        // marginTop: 0,
        margin: 20,
        fontWeight:"bold",
        fontSize:28,
    },
    Input: {
        color: '#464646',
        fontSize: 14,
        width:"80%"
    },

});

