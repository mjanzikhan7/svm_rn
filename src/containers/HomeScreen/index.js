import React, {Component} from 'react';
import {
    View,
    Image,
    StyleSheet,
    Text,
    StatusBar,
    FlatList,
    TouchableOpacity,
    TextInput,
    Dimensions,
} from 'react-native';
import Logo from '../../components/LogoHeader';
import DropDown from '../../components/DropDown';
import Button from '../../components/Button';
import Assets from '../../assets';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import {Picker} from 'native-base';
import {API} from '../../constants';
import Loader from '../../components/loader';
import SimpleToast from 'react-native-simple-toast';
import Preference from 'react-native-preference';
import {widthToDp,heightToDp} from './../../utils'
import Animated from 'react-native-reanimated';


// import {NavigationActions, StackActions, NavigationRoute} from 'react-navigation';
/*import Preference from 'react-native-preference';
import _ from 'lodash';*/

/*const resetAction = StackActions.reset({
    index: 0,
    // actions: [NavigationActions.navigate({routeName: 'UserBottomTab'})],
    actions: [NavigationActions.navigate({routeName: 'Landing'})],
});*/


export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            listVehicleType: [
                {
                    id: 1,
                    image: Assets.icons.Car,
                    imageSelected: Assets.icons.CarSelected,
                    placeholder: 'Car',
                    value: '1',
                    isSelected: true,
                },
                {
                    id: 2,
                    image: Assets.icons.Bike,
                    imageSelected: Assets.icons.BikeSelected,
                    placeholder: 'Motor Bike',
                    value: '2',
                    isSelected: false,
                },

                {
                    id: 3,
                    image: Assets.icons.Van,
                    imageSelected: Assets.icons.VanSelected,
                    placeholder: 'Van',
                    value: '3',
                    isSelected: false,
                },
                {
                    id: 4,
                    image: Assets.icons.Truck,
                    imageSelected: Assets.icons.TruckSelected,
                    placeholder: 'Motor Home',
                    value: '4',
                    isSelected: false,
                },
                {
                    id: 5,
                    image: Assets.icons.Carvan,
                    imageSelected: Assets.icons.CarvanSelected,
                    placeholder: 'Caravan',
                    value: '5',
                    isSelected: false,
                },
                {
                    id: 6,
                    image: Assets.icons.LongTruck,
                    imageSelected: Assets.icons.LongTruckSelected,
                    placeholder: 'Truck',
                    value: '6',
                    isSelected: false,
                },


            ],
            featuredList: [],
            vehicleBrandsList: [
                {
                    "id": 0,
                    "name": "Select Brand",
                    "image": "aermacchi.png",
                    "vehicle_id": 2,
                    "status": 1
                },
            ],
            vehicleModelList: [{
                "id": 0,
                "name": "Select Model",
                "image": "aermacchi.png",
                "vehicle_id": 2,
                "status": 1
            },
            ],
            total_vehicles: '',
            loading: false,
            selectedVehicle: ' Car',
            selectedVehicleValue: '1',
            selectedBrand: '',
            selectedBrandValue: '0',
            selectedModel: '',
            selectedModelValue: '0',
            postalCode: '',
            listMiles: [

                {
                    id: 1,
                    name: 'less than 20 miles',
                    value: '20',
                },
                {
                    id: 4,
                    name: 'less than 30 miles',
                    value: '30',
                },
                {
                    id: 5,
                    name: 'less than 40 miles',
                    value: '40',
                },

            ],
            recentlyViewedVehicles:Preference.get("recently"),
            width:'',
            height:'',

        };

    }

    componentDidMount() {
        this.getVehicleBrands();
        this.getBrandModels();
        this.getFeaturedVehicles();

        this.setState({
            recentlyViewedVehicles:Preference.get("recently"),
            width:Dimensions.get('window').width,
            height:Dimensions.get('window').height

        })
        console.log("Did:  "+ JSON.stringify(Dimensions.get('window')))


    }


    componentWillMount() {
        this.setState({
            recentlyViewedVehicles:Preference.get("recently")
        })
        console.log("Will:  "+ Preference.get("recently"))
    }
    forceUpdate(callback: () => void) {
        super.forceUpdate(callback);
        console.log("\n\n\n\n\n\nforceUpdate:  "+ Preference.get("recently"))

    }

    getVehicleBrands() {
        this.setState({loading: true});

        fetch(API.GetBrandsWithVehicleType + 'vehicle_id=' + this.state.selectedVehicleValue, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let temp = [];
                    /* temp = this.state.vehicleBrandsList
                     if (temp.length>1){
                         for (let x= 1; x<temp.length; x++){
                             temp.pop()
                         }
                     }*/
                    temp.push( {
                        "id": 0,
                        "name": "Select Brand",
                        "image": "aermacchi.png",
                        "vehicle_id": 2,
                        "status": 1
                    },)
                    for (let x= 0; x<data.data.length; x++){
                        temp.push(data.data[x])
                    }

                    this.setState({
                        vehicleBrandsList: temp,
                    });
                    if( parseInt(data.total_vehicles+"")>0){
                        // SimpleToast.show(data.total_vehicles+"")
                        this.setState({
                            total_vehicles: data.total_vehicles
                        })
                    }
                    if (data.data.length>0){
                        this.setState({
                            // selectedBrandValue:data.data[0].id
                        })
                    }else{
                        this.setState({
                            selectedBrandValue:"0"
                        })
                    }
                    this.getFeaturedVehicles();
                    setTimeout(() => {
                        this.getBrandModels()
                    }, 300);


                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    getBrandModels() {
        this.setState({loading: true});

        fetch(API.GetBrandModels + 'brand_id=' + this.state.selectedBrandValue, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('getBrandModels Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let temp = [];
                    /* temp = this.state.vehicleModelList
                     if (temp.length>1){
                         for (let x= 1; x<temp.length; x++){
                             temp.pop()
                         }
                     }*/
                    temp.push({
                        "id": 0,
                        "name": "Select Model",
                        "image": "aermacchi.png",
                        "vehicle_id": 2,
                        "status": 1
                    },)
                    for (let x= 0; x<data.data.length; x++){
                        temp.push(data.data[x])
                    }
                    this.setState({
                        vehicleModelList: temp,
                    });

                    if( parseInt(data.total_vehicles+"")>0){
                        // SimpleToast.show(data.total_vehicles+"")
                        this.setState({
                            total_vehicles: data.total_vehicles
                        })
                    }

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    getTotalVehicles() {
        this.setState({loading: true});
        const { selectedBrandValue,selectedModelValue } = this.state;
        let requestBody = new FormData();
        requestBody.append('brand_id', selectedBrandValue);
        requestBody.append('model_id', selectedModelValue);
        fetch(API.TotalVehicles, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('getTotalVehicles Response: ' + JSON.stringify(response));

                if (response.success) {
                    let data = response.data;
                    this.setState({loading: false});


                    if( parseInt(data.total_vehicles+"")>0){
                        // SimpleToast.show(data.total_vehicles+"")
                        this.setState({
                            total_vehicles: data.total_vehicles
                        })
                    }else{
                        this.setState({
                            total_vehicles: ""
                        })
                    }

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    getFeaturedVehicles() {
        this.setState({loading: true});

        fetch(API.FeaturedVehicles + 'vehicle_type_id=' + this.state.selectedVehicleValue, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;

                    this.setState({
                        featuredList: data,
                    });


                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    searchVehicles() {
        const {selectedVehicleValue, selectedBrandValue,selectedModelValue } = this.state;
        this.setState({loading: true});
        console.log('searchVehicles');
        console.log('API URL:  ' + API.Filter);
        let requestBody = new FormData();
        requestBody.append('vehicle_id', selectedVehicleValue);
        requestBody.append('brand_id', selectedBrandValue);
        requestBody.append('model_id', selectedModelValue);
        requestBody.append('offset', '0');

        let params = {
            vehicle_id:selectedVehicleValue,
            brand_id:selectedBrandValue,
            model_id:selectedModelValue,
            offset:"0",
        }

        console.log(requestBody);
        fetch(API.Filter, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.success) {
                this.setState({loading: false});
                let data = response.data;

                if (data.result.length > 0) {
                    this.props.navigation.navigate('Search', {
                        vehicleList: data.result,
                        totalCars:data.total_match,
                        params:params,
                        nav:"home"
                    });
                } else {
                    SimpleToast.show('Data not found');
                }

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    brandsItems() {

        let countries = this.state.vehicleBrandsList;
        //   console.log(  this.state.vehicleBrandsList)
        return (countries.map((item, index) => {
            return (<Picker.Item
                label={item.name} value={item.id}/>);
        }));
    }
    onValueChange(value: string) {
        console.log(value)
        this.setState({
            selectedBrandValue: value
        });

        setTimeout(() => {
            this.getBrandModels()
        }, 100);

    }

    onChangeModel(value: string) {
        console.log(value)
        this.setState({
            selectedModelValue: value
        });
        setTimeout(() => {
            this.getTotalVehicles()
        }, 100);

    }

    dropBrandModels() {
        let countries = this.state.vehicleModelList;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}/>);
        }));
    }

    dropMiles() {
        let countries = this.state.listMiles;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item}/>);
        }));
    }

    getV(){
        let totalVehicles = this.state.total_vehicles;
        if(parseInt(totalVehicles)>0){
            return totalVehicles;
        }else{
            return "";
        }
    }

    render() {
        return (
            <View style={style.mainContainer}>
                <Logo/>
                <TouchableOpacity
                    style={{
                        position:"absolute",left:10,top:5}}
                    onPress={()=>{
                        this.props.navigation.openDrawer()
                    }}>
                    <Image style={{width:25,height:25, marginTop:2.5,
                        tintColor:Assets.colors.vehicleTypeBg}}
                           resizeMode={'contain'}
                           source={Assets.icons.HamB}/>
                </TouchableOpacity>

                <View style={style.styleVehicleTypes}>
                    <FlatList
                        style={{}}
                        data={this.state.listVehicleType}
                        keyExtractor={item => item.id}
                        horizontal={true}
                        extraData={this.props}
                        showsHorizontalScrollIndicator={false}
                        contentContainerStyle={{
                            alignItems: 'center',
                            justifyContent: 'space-evenly',
                            width:"100%"
                        }}
                        removeClippedSubvisews={false}
                        renderItem={({item, index}) => {
                            return this.renderVehicleTypes(item, index);
                        }}
                    />
                </View>


                <KeyboardAwareScrollView>
                    <View>
                        <View style={style.styleSearchSection}>
                            <Image source={Assets.images.SearchBg}
                                   style={{resizeMode: 'cover', height: 220, width: '100%'}}/>

                            <View
                                style={[{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}]}>

                                <Text
                                    style={style.styleSearchHeading}>Search your ideal car</Text>
                                <View
                                    style={{
                                        alignItems: 'center',
                                        width: '100%',
                                    }}>

                                    <View
                                        style={{flexDirection: 'row', width: '90%', margin: 5, marginTop: 20}}>
                                        <DropDown
                                            style={{fontSize:9,}}

                                            dropDownItems={this.brandsItems()}
                                            // selectedValue={this.state.selectedBrandValue}
                                            placeholder={"Select Brand"}
                                            // placeholder={this.state.selectedBrand}
                                            // onValueChange ={this.onChangeBrand.bind(this)}
                                            selectedValue={this.state.selectedBrandValue}
                                            onValueChange={this.onValueChange.bind(this)}
                                        />
                                        <DropDown

                                            pickerStyle={{marginStart: 15}}
                                            dropDownItems={this.dropBrandModels()}
                                            selectedValue={this.state.selectedModelValue}
                                            placeholder={"Select Model"}
                                            // placeholder={this.state.selectedModel}
                                            onValueChange={this.onChangeModel.bind(this)}
                                        />
                                    </View>

                                    {/*  <View
                                        style={{flexDirection: 'row', width: '70%', margin: 5}}>
                                        <DropDown/>
                                        <View
                                            style={{
                                                width: '47%',
                                                backgroundColor: 'white',
                                                borderRadius: 20,
                                                borderWidth: 0.2,
                                                height: 27,
                                                padding: -5,
                                            }}>

                                            <TextInput
                                                keyboardType={'default'}
                                                placeholder={'Post Code'}
                                                secureTextEntry={false}
                                                onChangeText={(text) => {
                                                }}
                                                // maxLength={this.props.maxLength}

                                                style={style.Input}/>
                                        </View>


                                        <DropDown
                                            pickerStyle={{marginStart: 15}}
                                            dropDownItems={this.dropMiles()}
                                        />
                                    </View>*/}

                                    <Button
                                        buttonStyle={{width:"50%", marginTop: 20}}
                                        onPress={() => {
                                            this.searchVehicles();
                                        }}
                                        // text1={'Search'}
                                        text2={"Search "+this.getV()+ " "+this.state.selectedVehicle + 's'}
                                        buttonTextStyle={{fontSize: 11}}
                                    />

                                    {/*<View
                                        style={{flexDirection: 'row', width: '70%', margin: 5}}>
                                        <TouchableOpacity
                                            style={{position: 'absolute', left: 20}}>
                                            <Text style={style.textStyle}>More Options</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                            style={{position: 'absolute', right: 20, flexDirection: 'row'}}>
                                            <Image source={Assets.icons.Ulez}
                                                   style={{
                                                       resizeMode: 'cover',
                                                       height: 7, width: 7,
                                                       tintColor: Assets.colors.white,
                                                       margin: 3,
                                                   }}/>
                                            <Text style={style.textStyle}>Reset</Text>
                                        </TouchableOpacity>
                                    </View>*/}


                                </View>


                            </View>

                        </View>

                        {this.state.recentlyViewedVehicles != null && this.state.recentlyViewedVehicles.length>0?
                            <View
                                style={style.styleRecentSection}>

                                <View
                                    style={{flexDirection: 'row', margin: 10, height: 30}}>
                                    <Text
                                        style={{
                                            color: Assets.colors.vehicleTypeBg,
                                            margin: 2,
                                            fontSize: 18,
                                            position: 'absolute',
                                            left: 0,
                                        }}>
                                        Recently Viewed
                                    </Text>
                                    {/*
                                    <TouchableOpacity
                                        style={{
                                            borderColor: Assets.colors.vehicleTypeBg,
                                            borderWidth: 1,
                                            margin: 5,
                                            borderRadius: 20,
                                            paddingStart: 10,
                                            paddingEnd: 10,
                                            alignItems: 'center',
                                            position: 'absolute',
                                            right: 0,

                                        }}>

                                        <Text
                                            style={{
                                                color: Assets.colors.vehicleTypeBg,
                                                fontSize: 13,
                                            }}>
                                            View All
                                        </Text>

                                    </TouchableOpacity>*/}

                                </View>

                                <View style={style.styleRecentVehicles}>
                                    <FlatList
                                        style={{}}
                                        data={this.state.recentlyViewedVehicles}
                                        // data={this.state.listVehicleType}
                                        keyExtractor={item => item.id}
                                        horizontal={true}
                                        extraData={this.props}
                                        showsHorizontalScrollIndicator={false}
                                        contentContainerStyle={{
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}
                                        removeClippedSubvisews={false}
                                        renderItem={({item, index}) => {
                                            // return this.renderFeaturedVehicle(item, index);
                                            return this.renderRecentlyViewed(item, index);
                                        }}
                                    />
                                </View>


                            </View>:<View/>
                        }


                        <View
                            style={style.styleRecentSection}>

                            <View
                                style={{flexDirection: 'row', margin: 10, height: 30}}>
                                <Text
                                    style={{
                                        color: Assets.colors.vehicleTypeBg,
                                        margin: 2,
                                        fontSize: 18,
                                        position: 'absolute',
                                        left: 0,
                                    }}>
                                    {'Featured ' + this.state.selectedVehicle + 's'}
                                </Text>

                                {/* <TouchableOpacity
                                    style={{
                                        borderColor: Assets.colors.vehicleTypeBg,
                                        borderWidth: 1,
                                        margin: 5,
                                        borderRadius: 20,
                                        paddingStart: 10,
                                        paddingEnd: 10,
                                        alignItems: 'center',
                                        position: 'absolute',
                                        right: 0,

                                    }}>

                                    <Text
                                        style={{
                                            color: Assets.colors.vehicleTypeBg,
                                            fontSize: 13,
                                        }}>
                                        View All
                                    </Text>

                                </TouchableOpacity>*/}

                            </View>

                            <View style={style.styleRecentVehicles}>
                                <FlatList
                                    style={{}}
                                    data={this.state.featuredList}
                                    keyExtractor={item => item.id}
                                    horizontal={true}
                                    extraData={this.props}
                                    showsHorizontalScrollIndicator={false}
                                    contentContainerStyle={{
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                    }}
                                    removeClippedSubvisews={false}
                                    renderItem={({item, index}) => {
                                        return this.renderFeaturedVehicle(item, index);
                                    }}
                                />
                            </View>


                        </View>


                    </View>
                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }

    renderVehicleTypes(item, index) {

        return (
            <TouchableOpacity
                onPress={() => {
                    console.log(item.id);
                    let array = this.state.listVehicleType;
                    let array2 = [];
                    for (let i = 0; i < array.length; i++) {
                        let tmp = array[i];
                        if (tmp.id === item.id) {
                            tmp.isSelected = true;
                        } else {
                            tmp.isSelected = false;
                        }

                        array2.push(tmp);
                    }
                    this.setState({
                        listVehicleType: array2,
                        selectedVehicle: item.placeholder,
                        selectedVehicleValue: item.value,
                    });
                    setTimeout(() => {
                        this.getVehicleBrands()
                    }, 300);




                }}
                style={{marginStart:this.state.width>600?10:0,
                    marginEnd:this.state.width>600?10:0, }}>
                {/*style={{marginStart:widthToDp("3.5%"), marginEnd:widthToDp('3.5%')}}>*/}
                <Image source={item.isSelected ? item.imageSelected : item.image}
                       style={{resizeMode: 'contain', width: 35, height: 35, borderRadius: 0}}/>
            </TouchableOpacity>

        );
    }

    renderRecentlyViewed(item, index) {

        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'column',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    // alignItems: 'center',
                }}
                onPress={() => {
                    this.props.navigation.navigate('ProductDetailScreen', {
                        id: item.id,
                    });
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 1, flex: 1,
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={item.image_url!= null?{uri: item.image_url}:{uri:item.main_image_url}}
                        // source={Assets.icons.CarThumb1}
                        style={{resizeMode: 'cover', width: 170, height: 120, borderRadius: 5}}/>
                </View>


                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle1}>
                    {item.title}

                </Text>
                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle2}>
                    {'£' + item.price}
                </Text>

                <View
                    style={{flexDirection: 'row', marginTop: 10}}>
                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Road}
                               style={[style.recentIconStyle,{marginTop: 1}]}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.mileage}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Manual}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.transmission_types}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Engine1}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.engine}
                        </Text>

                    </View>


                </View>


                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        padding: 5, flex: 1,
                        justifyContent: 'center',
                        margin: -5,
                        marginTop: 5,
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        borderBottomStartRadius: 3,
                        borderBottomEndRadius: 3,
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('ProductDetailScreen', {
                            id: item.id,
                        });
                    }}>
                    <Text
                        numberOfLines={1}
                        style={[style.recentTextStyle1,{flex:1,textAlign:'center'}]}>
                        More Details
                    </Text>
                </TouchableOpacity>

                {/*<View
                    style={{
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        position: 'absolute',
                        top: 5,
                        left: 5,
                        paddingStart: 5,
                        paddingEnd: 5,
                        justifyContent: 'center',
                    }}>
                    <Text
                        style={{color: Assets.colors.white, fontSize: 9}}>
                        FEATURED
                    </Text>
                </View>*/}

            </TouchableOpacity>

        );
    }

    updateRecentlyViewedVehicles(item){
        let newArray = []

        if (Preference.get("recently") === undefined){
            newArray.push(item)
        }
        else{
            newArray = Preference.get("recently")
            newArray.push(item)
        }
        console.log("New Array: "+ JSON.stringify(newArray))
        this.setState({
            recentlyViewedVehicles:newArray
        })

        Preference.clear("recently")
        Preference.set("recently", newArray);

    }
    renderFeaturedVehicle(item, index) {

        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'column',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.2,
                    padding: 5,
                    borderRadius: 3,
                    // alignItems: 'center',
                }}
                onPress={() => {
                    this.updateRecentlyViewedVehicles(item)
                    this.props.navigation.navigate('ProductDetailScreen', {
                        id: item.id,
                    });
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        padding: 1, flex: 1,
                        justifyContent: 'center',
                    }}>
                    <Image
                        source={item.image_url!= null?{uri: item.image_url}:{uri:item.main_image_url}}
                        // source={Assets.icons.CarThumb1}
                        style={{resizeMode: 'cover', width: 170, height: 120, borderRadius: 5}}/>
                </View>


                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle1}>
                    {item.title}

                </Text>
                <Text
                    numberOfLines={1}
                    style={style.recentTextStyle2}>
                    {'£' + item.price}
                </Text>

                <View
                    style={{flexDirection: 'row', marginTop: 10}}>
                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Road}
                               style={[style.recentIconStyle,{marginTop: 1}]}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.mileage}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Manual}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.transmission_types}
                        </Text>

                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            flex: 0.3, padding: 5,
                        }}>
                        <Image source={Assets.icons.Engine1}
                               style={style.recentIconStyle}/>
                        <Text
                            numberOfLines={1}
                            style={style.recentTextStyle3}>
                            {item.engine}
                        </Text>
                    </View>


                </View>


                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        padding: 5, flex: 1,
                        justifyContent: 'center',
                        margin: -5,
                        marginTop: 5,
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        borderBottomStartRadius: 3,
                        borderBottomEndRadius: 3,
                    }}
                    onPress={() => {
                        this.updateRecentlyViewedVehicles(item)

                        this.props.navigation.navigate("ProductDetailScreen",{
                            id:item.id
                        })
                    }}>
                    <Text
                        style={[style.recentTextStyle1,{flex:1,textAlign:'center'}]}>
                        More Details
                    </Text>
                </TouchableOpacity>

                <View
                    style={{
                        backgroundColor: Assets.colors.vehicleTypeBg,
                        position: 'absolute',
                        top: 10,
                        left: 10,
                        paddingStart: 5,
                        paddingEnd: 5,
                        justifyContent: 'center',
                    }}>
                    <Text
                        style={{color: Assets.colors.white, fontSize: 9}}>
                        FEATURED
                    </Text>
                </View>

            </TouchableOpacity>

        );
    }
}
const style = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
        flexDirection: 'column',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18,
        fontWeight: 'bold', margin: 20,marginTop: 30
    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,
        width:150

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 8,
        marginStart: 2,
        // width:40
        minWidth:30,
        maxWidth:45
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},
    Input: {
        color: '#464646',
        fontSize: 11,
        width: '90%',
        margin: -5,
        marginStart: 10,
    },

});
