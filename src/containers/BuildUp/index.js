import React, {Component} from 'react';
import {
    View, Image, StyleSheet, Text, StatusBar, TextInput,
    TouchableOpacity, FlatList, Dimensions,
} from 'react-native';
import Assets from '../../assets';
import Logo from '../../components/LogoHeader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Button1 from '../../components/Button1';
import {API} from '../../constants';
import SimpleToast from 'react-native-simple-toast';
import Loader from '../../components/loader';
import PackageButton from '../../components/PackageButton';
import PackageFeatureItem from '../../components/PackageFeatureItem';
import Preference from 'react-native-preference';
import DropDown from '../../components/DropDown1';
import CarSpecItemInput from '../../components/CarSpecItemInput';
import CarSpecItemPicker from '../../components/CarSpecItemPicker';
import {Picker} from 'native-base';
import CropImagePicker from 'react-native-image-crop-picker';
import Modal from 'react-native-modal';
import moment from 'moment';
// import DatePicker from 'react-native-datepicker'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {NavigationActions, StackActions} from 'react-navigation';



const {width, height} = Dimensions.get('window');

const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: 'AppDrawerNavigator'})],
});
export default class BuildUp extends Component {

    constructor(props) {
        super(props);
        this.state = {

            loading: false,
            cameraModal: false,
            listVehicleType: [

                {
                    id: 0,
                    name: ' ',
                    // name: 'Select vehicle',
                    value: '20',
                },


            ],
            listVehicleMake: [

                {
                    id: 0,
                    name: ' ',
                    // name: 'Select Make',
                    value: '20',
                },


            ],
            listVehicleModel: [

                {
                    id: 1,
                    name: ' ',
                    // name: 'Select Model',
                    value: '20',
                },

            ],
            listVehicleYear: [

                {
                    id: 1,
                    reg_year: ' ',
                    // reg_year: 'Select Year',
                    value: '20',
                },

            ],
            listVehicleTransmission: [

                {
                    id: 0,
                    name: ' ',
                    // name: 'Select Transmission',
                    value: '20',
                },

            ],
            listVehicleCondition: [

                {
                    id: 0,
                    name: ' ',
                    // name: 'Select Condition',
                    value: '20',
                },


            ],

            listExteriorColor: [

                {
                    id: 0,
                    color_name: ' ',
                    // color_name: 'Select Exterior',
                },

            ],
            listInteriorColor: [

                {
                    id: 0,
                    color_name: ' ',
                    // color_name: 'Select Interior',
                },

            ],
            listDriveMode: [
                {
                    id: 0,
                    drive_mode_name: ' ',
                    // drive_mode_name: 'Select Drive Mode',
                },

            ],
            listCategory: [
                {
                    id: 0,
                    name: ' ',
                    // name: 'Select Category',
                    slug: 'category-a-E4Scw-42',
                    status: 1,
                    count: 0,
                },

            ],
            listUlez: [
                {
                    id: 0,
                    name: ' ',
                    // name: 'Select ULEZ',
                },

            ],
            listMileage: [
                {
                    id: 0,
                    text: ' ',
                    // text: 'Select Mileage',
                },

            ],
            listFuelType: [
                {
                    id: 0,
                    name: ' ',
                    // name: 'Select Fuel Type',
                },

            ],
            listEngineSize: [
                {
                    id: 0,
                    engine_size: ' ',
                    // engine_size: 'Select Engine Size',
                },

            ],
            listBodyType: [
                {
                    id: 0,
                    name: ' ',
                    // name: 'Select Body Type',
                },

            ],
            listUserType: [
                {
                    id: 0,
                    name: ' ',
                    // name: 'Select User Type',
                },

            ],
            listFeature: [
                {
                    id: 0,
                    name: ' ',
                    // name: 'Search Features',
                },
            ],
            selectedFeatures: [],
            selectedImages: [],


            selectedVType: this.props.navigation.getParam("selectedVehicleValue"),
            selectedVTypeName: this.props.navigation.getParam("selectedVehicle"),
            // selectedVType: '',
            selectedVMake: '',
            selectedVModel: '',
            selectedVY: '',
            selectedVTrm: '',
            selectedVC: '',
            selectedFeature: '',
            sellerNote: '',
            price: '',
            title:'',


            selectedExterior: '',
            selectedInterior: '',
            selectedDriveMode: '',
            selectedCategory: '',
            selectedUlez: '',
            selectedMileage: '',
            selectedFuelType: '',
            selectedEngineSize: '',
            selectedBodyType: '',
            selectedUserType: '',

            engine:"",
            mileage:'',
            history:'',
            owners:'',
            regDate:'',
            isDateModal:false,

        };
    }

    componentDidMount() {
        this.getFilterList();
        this.getFeatureList();
        this.getVehicleBrands()
        this.getBrandModels()

    }


    getVehicleBrands() {
        this.setState({loading: true});

        fetch(API.GetBrandsWithVehicleType + 'vehicle_id=' + this.state.selectedVType, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('\n\nVM Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data.data;
                    /*let temp = [];
                    temp = this.state.vehicleBrandsList
                    if (temp.length>1){
                        for (let x= 1; x<temp.length; x++){
                            temp.pop()
                        }
                    }
                    for (let x= 0; x<data.data.length; x++){
                        temp.push(data.data[x])
                    }
                    this.setState({
                        vehicleBrandsList: temp,
                        total_vehicles: data.total_vehicles,
                    });
                    if (data.data.length>0){
                        this.setState({
                            // selectedBrandValue:data.data[0].id
                        })
                    }else{
                        this.setState({
                            selectedBrandValue:"0"
                        })
                    }*/
                    this.handleVehicleMaker(data);


                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    getBrandModels() {
        this.setState({loading: true});

        fetch(API.GetBrandModels + 'brand_id=' + this.state.selectedVMake, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('getBrandModels Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    /*let temp = [];
                    temp = this.state.vehicleModelList
                    if (temp.length>1){
                        for (let x= 1; x<temp.length; x++){
                            temp.pop()
                        }
                    }
                    for (let x= 0; x<data.data.length; x++){
                        temp.push(data.data[x])
                    }
                    this.setState({
                        vehicleModelList: temp,
                        total_vehicles: data.total_vehicles,
                    });*/
                    this.handleVehicleModel(data.data);

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    handleFeatures(data) {
        let temp = [];
        temp = this.state.listFeature;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listFeature: temp,
        });

    }

    handleVehicleType(data) {
        let temp = [];
        temp = this.state.listVehicleType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleType: temp,
        });

    }

    handleVehicleMaker(data) {
        let temp = [];
        /*temp = this.state.listVehicleMake;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }*/
        temp.push(
            {
                id: 0,
                name: ' ',
                // name: 'Select Make',
                value: '20',
            },
        )
        // SimpleToast.show("zxvs: "+data.length)
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
            // console.log("\n asdgasga"+data[x].id+"\t");

        }

        this.setState({
            listVehicleMake: temp,
        });
      /*  setTimeout(() => {
            console.log("\n\n\n>>> New listVehicleMake"+
                JSON.stringify(this.state.listVehicleMake))
        },300)*/

    }

    handleVehicleModel(data) {
        let temp = [];
        temp.push(
            {
                id: 0,
                name: ' ',
                // name: 'Select Model',
                value: '20',
            },
        )
       /* temp = this.state.listVehicleModel;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }*/
        // SimpleToast.show("zxvs: "+data.length)

        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
            // console.log("\n asdgasga"+data[x].id+"\t");

        }

        this.setState({
            listVehicleModel: temp,
        });

     /*   setTimeout(() => {
            console.log("\n\n\n>>> New listVehicleMake"+
                JSON.stringify(this.state.listVehicleModel))
        },300)*/
    }

    handleVehicleyear(data) {
        let temp = [];
        temp = this.state.listVehicleYear;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleYear: temp,
        });

    }

    handleVehicleTransmission(data) {
        let temp = [];
        temp = this.state.listVehicleTransmission;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleTransmission: temp,
        });

    }

    handleVehicleCondition(data) {
        let temp = [];
        temp = this.state.listVehicleCondition;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleCondition: temp,
        });

    }

    handleVehicleExteriorColor(data) {
        let temp = [];
        temp = this.state.listExteriorColor;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }
        this.setState({
            listExteriorColor: temp,
        });

    }

    handleVehicleInteriorColor(data) {
        let temp = [];
        temp = this.state.listInteriorColor;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listInteriorColor: temp,
        });

    }

    handleVehicleDriveMode(data) {
        let temp = [];
        temp = this.state.listDriveMode;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listDriveMode: temp,
        });

    }

    handleVehicleCategory(data) {
        let temp = [];
        temp = this.state.listCategory;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listCategory: temp,
        });

    }

    handleVehicleUlez(data) {
        let temp = [];
        temp = this.state.listUlez;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listUlez: temp,
        });

    }

    handleVehicleMilage(data) {
        let temp = [];
        temp = this.state.listMileage;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listMileage: temp,
        });

    }

    handleVehicleFuelType(data) {
        let temp = [];
        temp = this.state.listFuelType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listFuelType: temp,
        });

    }

    handleVehicleEngineSize(data) {
        let temp = [];
        temp = this.state.listEngineSize;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listEngineSize: temp,
        });

    }

    handleVehicleBodyType(data) {
        let temp = [];
        temp = this.state.listBodyType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listBodyType: temp,
        });

    }

    handleVehicleUserType(data) {
        let temp = [];
        temp = this.state.listUserType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listUserType: temp,
        });

    }

    getFilterList() {
        this.setState({loading: true});

        fetch(API.FilterLists, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let years = [];
                    for (let x = 0; x < response.data.year.length; x++) {
                        years.push({
                            reg_year: response.data.year[x].reg_year + '',
                        });
                    }
                    /*  this.setState({
                          listVehicleType: response.data.vehicles,
                          listVehicleMake: response.data.brands,
                          listVehicleModel: response.data.models,
                          listVehicleYear: years,
                          listVehicleTransmission: response.data.transmition_type,
                          listVehicleCondition: response.data.condition,
                      });*/

                    this.handleVehicleType(response.data.vehicles);
                    this.handleVehicleMaker(response.data.brands);
                    this.handleVehicleModel(response.data.models);
                    this.handleVehicleyear(years);
                    this.handleVehicleTransmission(response.data.transmition_type);
                    this.handleVehicleCondition(response.data.condition);
                    this.handleVehicleExteriorColor(response.data.exterior_color);
                    this.handleVehicleInteriorColor(response.data.interior_color);
                    this.handleVehicleDriveMode(response.data.drive_mode);
                    this.handleVehicleCategory(response.data.category);
                    this.handleVehicleUlez(response.data.ulez);
                    this.handleVehicleMilage(response.data.mileage);
                    this.handleVehicleFuelType(response.data.fuel_type);
                    this.handleVehicleEngineSize(response.data.engine_size);
                    this.handleVehicleBodyType(response.data.body_type);
                    this.handleVehicleUserType(response.data.user_type);

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    getFeatureList() {
        this.setState({loading: true});

        fetch(API.FeatureList, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    this.handleFeatures(response.data);

                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    dropVehicleFeatures() {
        let countries = this.state.listFeature;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.name}
            />);
        }));
    }

    dropVehicleType() {
        let countries = this.state.listVehicleType;
        console.log("\n\n\n\nVT: "+JSON.stringify(countries))
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleMake() {
        let countries = this.state.listVehicleMake;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleModel() {
        let countries = this.state.listVehicleModel;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleYear() {
        let countries = this.state.listVehicleYear;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.reg_year} value={item.reg_year}
            />);
        }));
    }

    dropVehicleTransmission() {
        let countries = this.state.listVehicleTransmission;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleCondition() {
        let countries = this.state.listVehicleCondition;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleUserType() {
        let countries = this.state.listUserType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleBodyType() {
        let countries = this.state.listBodyType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleEngineSize() {
        let countries = this.state.listEngineSize;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.engine_size} value={item.id}
            />);
        }));
    }

    dropVehicleFuelType() {
        let countries = this.state.listFuelType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleMileage() {
        let countries = this.state.listMileage;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.text} value={item.id}
            />);
        }));
    }

    dropVehicleUlez() {
        let countries = this.state.listUlez;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleCategory() {
        let countries = this.state.listCategory;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleDriveMode() {
        let countries = this.state.listDriveMode;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.drive_mode_name} value={item.id}
            />);
        }));
    }

    dropVehicleInterior() {
        let countries = this.state.listInteriorColor;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.color_name} value={item.id}
            />);
        }));
    }

    dropVehicleExterior() {
        let countries = this.state.listExteriorColor;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.color_name} value={item.id}
            />);
        }));
    }

    onValueChangeFeat(value: string) {
        let newFeat = this.state.selectedFeatures;
        let newItem = value;
        // let newItem = {name: value};
        newFeat.push(newItem);
        this.setState({
            selectedFeatures: newFeat,
            selectedFeature: value,
        });
        // SimpleToast.show(JSON.stringify(newFeat))
    }

    onValueChangeVT(value: string) {
        this.setState({
            selectedVType: value,

        });
        setTimeout(() => {
            // SimpleToast.show(""+this.state.selectedVType)
            this.getVehicleBrands()

        },300)
        // SimpleToast.show(JSON.stringify(value))
    }

    onValueChangeVMake(value: string) {
        this.setState({
            selectedVMake: value,
        });
        setTimeout(()=>{
            this.getBrandModels()

        },300)
    }

    onValueChangeVModel(value: string) {
        this.setState({
            selectedVModel: value,
        });
    }

    onValueChangeVY(value: string) {
        this.setState({
            selectedVY: value,
        });
    }

    onValueChangeVTrm(value: string) {
        this.setState({
            selectedVTrm: value,
        });
    }

    onValueChangeVC(value: string) {
        this.setState({
            selectedVC: value,
        });
    }

    onValueChangeUT(value: string) {
        this.setState({
            selectedUserType: value,
        });
    }

    onValueChangeBT(value: string) {
        this.setState({
            selectedBodyType: value,
        });
    }

    onValueChangeES(value: string) {
        this.setState({
            selectedEngineSize: value,
        });
    }

    onValueChangeFT(value: string) {
        this.setState({
            selectedFuelType: value,
        });
    }

    onValueChangeMl(value: string) {
        this.setState({
            selectedMileage: value,
        });
    }

    onValueChangeUlz(value: string) {
        this.setState({
            selectedUlez: value,
        });
    }

    onValueChangeCat(value: string) {
        this.setState({
            selectedCategory: value,
        });
    }

    onValueChangeDM(value: string) {
        this.setState({
            selectedDriveMode: value,
        });
    }

    onValueChangeInt(value: string) {
        this.setState({
            selectedInterior: value,
        });
    }

    onValueChangeExt(value: string) {
        this.setState({
            selectedExterior: value,
        });
    }


    renderCarDetailSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Car Details'}
                    </Text>
                    <View
                        style={{alignItems: 'center', margin: 10, borderRadius: 5, borderWidth: 0.1}}>


                        <View
                            style={styles.itemRowStyle}
                        >
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Vehicle *'}</Text>
                               {/* <DropDown
                                    pickerStyle={{
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.2,

                                    }}
                                    style={{
                                        color: 'gray',
                                        fontSize: 9,
                                    }}
                                    dropDownItems={this.dropVehicleType()}
                                    onValueChange={this.onValueChangeVT.bind(this)}
                                    selectedValue={this.state.selectedVType}
                                />*/}
                                <View
                                style={{
                                    flex: 0.47,
                                    marginTop: 5,
                                    marginStart: 5,
                                    // backgroundColor: Assets.colors.featureBg2,
                                    padding: 2,
                                    borderRadius:10,
                                    borderWidth: 0.3,
                                }}>
                                    <Text
                                        style={{
                                            color: 'gray',
                                            fontSize: 16,
                                            marginStart:5,
                                        }}>{this.state.selectedVTypeName}</Text>

                                </View>
                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Make *'}</Text>
                                <DropDown
                                    pickerStyle={{
                                        // margin: 10,
                                        // width: '50%',
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.3,

                                    }}
                                    style={{
                                        color: 'gray',
                                    }}
                                    dropDownItems={this.dropVehicleMake()}
                                    onValueChange={this.onValueChangeVMake.bind(this)}
                                    selectedValue={this.state.selectedVMake}
                                />
                            </View>


                        </View>


                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Model *'}</Text>

                                <DropDown

                                    pickerStyle={{
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.3,

                                    }}
                                    style={{
                                        color: 'gray',
                                        tintColor: Assets.colors.vehicleTypeBg,
                                    }}
                                    dropDownItems={this.dropVehicleModel()}
                                    onValueChange={this.onValueChangeVModel.bind(this)}
                                    selectedValue={this.state.selectedVModel}
                                />
                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Condition *'}</Text>

                                <DropDown

                                    pickerStyle={{
                                        // margin: 10,
                                        // width: '50%',
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.3,

                                    }}
                                    style={{color: 'gray', tintColor: Assets.colors.vehicleTypeBg}}
                                    dropDownItems={this.dropVehicleCondition()}
                                    onValueChange={this.onValueChangeVC.bind(this)}
                                    selectedValue={this.state.selectedVC}
                                />

                            </View>

                        </View>

                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Body *'}</Text>

                                <DropDown

                                    pickerStyle={{
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.3,
                                    }}
                                    style={{
                                        color: 'gray',
                                        tintColor: Assets.colors.vehicleTypeBg,
                                    }}
                                    dropDownItems={this.dropVehicleBodyType()}
                                    onValueChange={this.onValueChangeBT.bind(this)}
                                    selectedValue={this.state.selectedBodyType}
                                />
                            </View>

                            <View
                                style={{flexDirection: 'column', flex: 0.47}}>
                                <Text style={styles.itemTitleStyle}>{'Year *'}</Text>

                                <DropDown

                                    pickerStyle={{
                                        // margin: 10,
                                        // width: '50%',
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.3,


                                    }}
                                    style={{color: 'gray', tintColor: Assets.colors.vehicleTypeBg}}
                                    dropDownItems={this.dropVehicleYear()}
                                    onValueChange={this.onValueChangeVY.bind(this)}
                                    selectedValue={this.state.selectedVY}
                                />

                            </View>


                        </View>


                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderCarSpecsSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    {/*  <Text
                        style={styles.sectionTitleStyle}>
                        {"Car Details"}
                    </Text>*/}
                    <View
                        style={{alignItems: 'center', margin: 10, borderRadius: 5, borderWidth: 0.1}}>
                        <CarSpecItemInput
                            keyboardType={'number-pad'}
                            icon={Assets.icons.Millage}
                            label={'Mileage'}
                            // placeholder={'Enter mileage (mi)'}
                            value={this.state.mileage}
                            onChangeText={(text)=>{
                                this.setState({
                                    mileage:text
                                })
                            }}/>

                        <CarSpecItemPicker
                            icon={Assets.icons.Transmission}
                            label={'Transmition'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleTransmission()}
                            onValueChange={this.onValueChangeVTrm.bind(this)}
                            selectedValue={this.state.selectedVTrm}
                        />

                        <CarSpecItemPicker
                            icon={Assets.icons.Drive}
                            label={'Drive'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleDriveMode()}
                            onValueChange={this.onValueChangeDM.bind(this)}
                            selectedValue={this.state.selectedDriveMode}
                        />
                        <CarSpecItemPicker
                            icon={Assets.icons.Fuel}
                            label={'Fuel Type'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleFuelType()}
                            onValueChange={this.onValueChangeFT.bind(this)}
                            selectedValue={this.state.selectedFuelType}
                        />
                        <CarSpecItemPicker
                            icon={Assets.icons.ExteriorColor}
                            label={'Exterior Color'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleExterior()}
                            onValueChange={this.onValueChangeExt.bind(this)}
                            selectedValue={this.state.selectedExterior}
                        />
                        <CarSpecItemInput
                            icon={Assets.icons.History}
                            label={'History'}
                            // placeholder={'Vehicle History Report'}
                            value={this.state.history}
                            onChangeText={(text)=>{
                                this.setState({
                                    history:text
                                })
                            }}/>
                        <CarSpecItemInput
                            keyboardType={'number-pad'}
                            icon={Assets.icons.Engine}
                            label={'Engine'}
                            // placeholder={'Enter Engine'}
                            value={this.state.engine}
                            onChangeText={(text)=>{
                                this.setState({
                                    engine:text
                                })
                            }}

                        />

                        <CarSpecItemPicker
                            icon={Assets.icons.InteriorColor}
                            label={'Interior Color'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleInterior()}
                            onValueChange={this.onValueChangeInt.bind(this)}
                            selectedValue={this.state.selectedInterior}
                        />
                        <CarSpecItemPicker
                            icon={Assets.icons.Like}
                            label={'CAT'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleCategory()}
                            onValueChange={this.onValueChangeCat.bind(this)}
                            selectedValue={this.state.selectedCategory}
                        />

                        <CarSpecItemInput
                            icon={Assets.icons.Account}
                            label={'Owner'}
                            // placeholder={'Enter Owners'}
                            value={this.state.owners}
                            onChangeText={(text)=>{
                                this.setState({
                                    owners:text
                                })
                            }}/>

                        <CarSpecItemPicker
                            icon={Assets.icons.Ulez}
                            label={'ULEZ'}
                            // placeholder={'Enter mileage (mi)'}
                            dropDownItems={this.dropVehicleUlez()}
                            onValueChange={this.onValueChangeUlz.bind(this)}
                            selectedValue={this.state.selectedUlez}
                        />
{/*
                        <CarSpecItemInput
                            icon={Assets.icons.Registration}
                            label={'Registered'}
                            placeholder={'Enter Date'}
                            value={this.state.regDate}
                            onChangeText={(text)=>{
                                this.setState({
                                    regDate:text
                                })
                            }}/>*/}

                        <TouchableOpacity
                            onPress={()=>{
                                this.setState({
                                    isDateModal:!this.state.isDateModal
                                })
                            }
                            }
                            style={styles.featureBgStyle2}>
                            <Image source={Assets.icons.Registration}
                                   style={styles.featureIconStyle}/>
                            <Text
                                style={styles.featureTextLabelStyle}>
                                {'Registered'}
                            </Text>


                            <TextInput
                                // keyboardType={this.props.keyboardType}
                                // placeholder={'Enter Date'}
                                // secureT*extEntry={this.props.secureTextEntry}
                                onChangeText={(text)=>{
                                    this.setState({
                                        regDate:text
                                    })
                                }}
                                editable={false}
                                value={this.state.regDate}
                                // maxLength={this.props.maxLength}
                                style={styles.featureTextValueStyle}/>
                        </TouchableOpacity>

                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderCarFeaturesSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Select Your Car Features'}
                    </Text>
                    <View
                        style={{
                            // alignItems: 'center',
                            margin: 10,
                            borderRadius: 5,
                            // borderWidth: 0.1
                        }}>
                        <View
                            style={styles.itemRowStyle}>
                            <View
                                style={{flexDirection: 'column', flex: 0.60}}>
                                <DropDown
                                    pickerStyle={{
                                        flex: 0.47,
                                        marginTop: 5,
                                        marginStart: 5,
                                        // backgroundColor: Assets.colors.featureBg2,
                                        // padding: 10,
                                        borderWidth: 0.3,

                                    }}
                                    style={{
                                        color: 'gray',
                                        tintColor: Assets.colors.vehicleTypeBg,
                                    }}
                                    dropDownItems={this.dropVehicleFeatures()}
                                    onValueChange={this.onValueChangeFeat.bind(this)}
                                    selectedValue={this.state.selectedFeature}
                                />
                            </View>


                        </View>

                        <View style={styles.styleRecentVehicles}>
                            <FlatList
                                style={{flex: 1, flexGrow: 1}}
                                data={this.state.selectedFeatures}
                                keyExtractor={item => item.id}
                                horizontal={false}
                                extraData={this.state}
                                numColumns={2}
                                showsHorizontalScrollIndicator={false}
                                contentContainerStyle={{
                                    // alignItems: 'center',
                                    justifyContent: 'center', flex: 1,
                                }}
                                removeClippedSubvisews={false}
                                renderItem={({item, index}) => {

                                    return this.renderSelectedFeatures(item, index);

                                }}


                            />
                        </View>


                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderSellerNoteSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {/*{'Enter Seller Note'}*/}
                    </Text>
                    <View
                        style={{
                            // alignItems: 'center',
                            margin: 10,
                            borderRadius: 5,
                            borderWidth: 0.3,
                            height: 100,
                        }}>

                        <View
                            style={{flexDirection: 'column', flex: 0.90, margin: 5}}>
                            <TextInput
                                keyboardType={'default'}
                                // placeholder={'Enter Seller Note'}
                                multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        sellerNote: text,
                                    });
                                }}
                                value={this.state.sellerNote}

                                // maxLength={this.props.maxLength}
                                style={[styles.featureTextValueStyle]}/>

                        </View>

                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderPriceSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Price *  £'}
                    </Text>
                    <View
                        style={{
                            // alignItems: 'center',
                            margin: 10,
                            borderRadius: 10,
                            borderWidth: 0.3,
                            padding:5
                        }}>

                        <View
                            style={{
                                flexDirection: 'column',
                                flex: 0.90,
                                marginStart: 5,
                                marginEnd: 3,
                            }}>
                            <TextInput
                                keyboardType={'number-pad'}
                                // placeholder={'Enter Seller Note'}
                                // multiline={true}
                                onChangeText={(text) => {
                                    this.setState({
                                        price: text,
                                    });
                                }}
                                value={this.state.price}
                                // maxLength={this.props.maxLength}
                                style={styles.featureTextValueStyle}/>

                        </View>

                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderImageSection() {
        return (
            <View
                style={{}}>

                <KeyboardAwareScrollView>

                    <Text
                        style={styles.sectionTitleStyle}>
                        {'Upload Photo'}
                    </Text>
                    <View
                        style={{
                            // alignItems: 'center',
                            margin: 10,
                            // borderRadius: 10,
                            //  borderWidth: 0.1,
                        }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.toggleCameraModal();
                            }}
                            style={{
                                margin: 5, padding: 10, borderRadius: 10, width: '30%',
                                borderWidth: 1, borderColor: Assets.colors.vehicleTypeBg,
                            }}>
                            <Text
                                style={{color: Assets.colors.vehicleTypeBg}}>
                                Add Image
                            </Text>

                        </TouchableOpacity>

                        <View
                            style={{
                                flexDirection: 'column',
                                flex: 0.90,
                                marginStart: 5,
                                marginEnd: 5,
                            }}>


                            <View
                                //style={style.flatlistView}
                            >
                                <FlatList
                                    // listKey={moment().format('x').toString()}
                                    style={{marginTop: 10}}
                                    data={this.state.selectedImages}
                                    keyExtractor={item => item.id}
                                    horizontal={true}
                                    extraData={this.props}
                                    showsHorizontalScrollIndicator={false}
                                    contentContainerStyle={{alignItems: 'center'}}
                                    removeClippedSubviews={false}
                                    renderItem={({item, index}) => {
                                        return this.renderSelectedImages(item, index);
                                    }}
                                />
                            </View>

                        </View>

                    </View>

                </KeyboardAwareScrollView>

            </View>
        );
    }

    renderSelectedFeatures(item, index) {

        return (
            <View
                style={{
                    flexDirection: 'column',
                    shadowColor: '#c7c7cd',
                    borderColor: '#c7c7cd',
                    margin: 10,
                    borderWidth: 0.3,
                    padding: 5,
                    borderRadius: 3,
                    flex: 0.5,
                }}>

                <View
                    style={{
                        flexDirection: 'row',
                        padding: 1, flex: 1,
                        alignItems: 'center',
                    }}>
                    <Text
                        numberOfLines={1}
                        style={styles.recentTextStyle1}>
                        {item}

                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            let tempList = this.state.selectedFeatures;
                            tempList.splice(index, 1);
                            this.setState({
                                selectedFeatures: tempList,
                            });
                        }
                        }>
                        <Image
                            source={Assets.icons.Cross}
                            style={{resizeMode: 'cover', width: 15, height: 15, borderRadius: 5}}/>
                    </TouchableOpacity>

                </View>

            </View>

        );
    }

    captureImage = () => {

        CropImagePicker.openCamera({

            cropping: true,
            compressImageQuality: 0.2,
            multiple: false,
            freeStyleCropEnabled: true,
            hideBottomControls: true,

        }).then(image => {
            // console.log(image);
            let source1 = {uri: image.path, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.state.selectedImages.push(source1);
            this.forceUpdate();
            // this.setState({
            //     filePath: source1,
            //     // isSelectImage: true,
            //     // profileImageSelected: true,
            //
            // });
            this.toggleCameraModal();
        });
        /*
                ImagePicker.showImagePicker(options, response => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                        alert(response.customButton);
                    } else {
                        let source1 = {uri: response.uri, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
                        // You can also display the image using data:
                        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                            this.setState({
                                filePath:  source1,
                                isSelectImage: true,

                            });


                    }
                });
        */
    };
    pickFromGallery = () => {


        CropImagePicker.openPicker({

            cropping: true,
            compressImageQuality: 0.2,
            multiple: false,
            freeStyleCropEnabled: true,
            hideBottomControls: true,

        }).then(image => {
            console.log(image);
            let source1 = {uri: image.path, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
            this.state.selectedImages.push(source1);
            this.forceUpdate();
            /* this.setState({
                 filePath: source1,
                 isSelectImage: true,
                 profileImageSelected: true,

             });*/
            this.toggleCameraModal();

        });
        /*
                ImagePicker.showImagePicker(options, response => {
                    console.log('Response = ', response);

                    if (response.didCancel) {
                        console.log('User cancelled image picker');
                    } else if (response.error) {
                        console.log('ImagePicker Error: ', response.error);
                    } else if (response.customButton) {
                        console.log('User tapped custom button: ', response.customButton);
                        alert(response.customButton);
                    } else {
                        let source1 = {uri: response.uri, name: moment().format('x') + '.jpeg', type: 'image/jpeg'};
                        // You can also display the image using data:
                        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
                            this.setState({
                                filePath:  source1,
                                isSelectImage: true,

                            });


                    }
                });
        */
    };

    toggleCameraModal() {
        this.setState({cameraModal: !this.state.cameraModal});
    }

    renderSelectedImages(item, index) {
        console.log(JSON.stringify(item,
        ));
        return (
            <View

                style={{margin: 5, borderRadius: 5}}>
                <Image source={{uri: item.uri}} style={{
                    resizeMode: 'cover',
                    width: 150, height: 120, borderRadius: 0,
                }}/>
                <TouchableOpacity
                    style={{
                        position: 'absolute', end: 0,
                        top: -3,
                    }}

                    onPress={() => {
                        let temp = [];
                        for (let i = 0; i < this.state.selectedImages.length; i++) {

                            if (index !== i) {
                                temp.push(this.state.selectedImages[i]);
                            }
                        }
                        this.setState({
                            selectedImages: temp,
                        });
                    }}>
                    <Image source={Assets.icons.Cross} style={{
                        resizeMode: 'contain', width: 15, height: 15,
                    }}/>
                </TouchableOpacity>
            </View>
        );
    }


    render() {
        return (
            <View style={styles.mainContainer}>
                <View>
                    <Logo/>
                    <TouchableOpacity
                        style={{
                            position: 'absolute', left: 10, top: 5,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}>
                        <Image style={{width: 30, height: 30}}
                               resizeMode={'contain'}
                               source={Assets.icons.Back}/>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{
                            position: 'absolute', right: 10, top: 10,
                        }}
                        onPress={() => {
                            this.props.navigation.dispatch(resetAction);
                        }}>
                        <Image style={{width: 20, height: 20, tintColor:Assets.colors.vehicleTypeBg}}
                               resizeMode={'contain'}
                               source={Assets.icons.Home}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.mainContainer}>

                    <KeyboardAwareScrollView>
                        <View
                            style={{
                                flex: 1, flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',

                            }}>
                            <View
                                style={{width: '100%', flexDirection: 'column'}}>
                                <Text style={styles.buttonTextStyle}>{'Build Your Ad'}</Text>
                                <Text style={{
                                    fontSize: 9, color: 'black', marginStart: 20,
                                }}>{'You are currently building a Free Ad'}</Text>
                                <Text style={{
                                    fontSize: 12, color: 'black', marginStart: 20,
                                }}>{'Items marked with * are required fields'}</Text>
                            </View>


                            <View
                                style={{
                                    flex: 1, flexDirection: 'column', width: '100%',
                                }}>
                                <View
                                    style={{
                                        // alignItems: 'center',
                                        margin: 10,
                                        borderRadius: 10,
                                        borderWidth: 0.1,
                                    }}>

                                    <View
                                        style={{
                                            flexDirection: 'column',
                                            flex: 0.90,
                                            marginStart: 5,
                                            marginEnd: 5,
                                        }}>
                                        <TextInput
                                            // keyboardType={'number-pad'}
                                            // placeholder={'Enter Title'}
                                            // multiline={true}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    title: text,
                                                });
                                            }}
                                            value={this.state.title}
                                            // maxLength={this.props.maxLength}
                                            style={styles.featureTextValueStyle}/>

                                    </View>

                                </View>

                                {this.renderCarDetailSection()}
                                {this.renderCarSpecsSection()}

                                <View
                                    style={{
                                        flex: 1, flexDirection: 'row',
                                        width: '95%', height: 0.5, backgroundColor: 'gray',
                                        margin: 10,
                                    }}/>
                                {this.renderCarFeaturesSection()}

                                <View
                                    style={{
                                        flex: 1, flexDirection: 'row',
                                        width: '95%', height: 0.5, backgroundColor: 'gray',
                                        margin: 10,
                                    }}/>

                                {this.renderSellerNoteSection()}
                                {this.renderPriceSection()}
                                {this.renderImageSection()}

                                <View
                                    style={{width: '100%', alignItems: 'center'}}>
                                    <TouchableOpacity
                                        style={{
                                            margin: 20,
                                            width: '25%',
                                            padding: 10, borderRadius: 20,
                                            backgroundColor: Assets.colors.vehicleTypeBg, alignItems: 'center',
                                        }}
                                        onPress={() => {

                                            let data = {
                                                vehicle_price_id:this.props.navigation.getParam("vehicle_price_id"),
                                                vehicle_plan_id:this.props.navigation.getParam("vehicle_plan_id"),
                                                planPrice:this.props.navigation.getParam("planPrice"),
                                                planName:this.props.navigation.getParam("planName"),
                                                vehicle_type_id:this.state.selectedVType,
                                                brand_id:this.state.selectedVMake,
                                                brand_model_id:this.state.selectedVModel,
                                                body_type_id:this.state.selectedBodyType,
                                                drive_mode_id:this.state.selectedDriveMode,
                                                exterior_color_id:this.state.selectedExterior,
                                                engine:this.state.engine,
                                                category_id:this.state.selectedCategory,
                                                description:this.state.sellerNote,
                                                history:this.state.history,
                                                owner:this.state.owners,
                                                title:this.state.title,
                                                // title:this.state.owners,
                                                condtion_id:this.state.selectedVC,
                                                year:this.state.selectedVY,
                                                // title:,
                                                mileage:this.state.mileage,
                                                transmission_type_id:this.state.selectedVTrm,
                                                fuel_type_id:this.state.selectedFuelType,
                                                interior_color_id:this.state.selectedInterior,
                                                ulez_id:this.state.selectedUlez,
                                                 registered_date:this.state.regDate,
                                                 // mpg:this.state.mileage,
                                                featured: this.state.selectedFeatures,
                                                price:this.state.price,
                                                images: this.state.selectedImages,
                                               /* user_id:,
                                                billing_first_name:,
                                                billing_last_name:,
                                                billing_flat_no:,
                                                billing_street_name:,
                                                billing_county:,
                                                billing_town:,
                                                billing_post_code:,*/
                                            };
                                            this.props.navigation.navigate('ApplyCoupon',{
                                                data:data
                                            });
                                        }}>

                                        <Text
                                            style={{
                                                fontWeight: 'bold',
                                                color: 'white',
                                                fontSize: 11,
                                            }}>Continue</Text>


                                    </TouchableOpacity>

                                </View>
                            </View>


                        </View>
                    </KeyboardAwareScrollView>
                    {/*<Loader*/}
                    {/*    visible={this.state.loading}/>*/}
                </View>
                <Modal

                    backdropOpacity={0}
                    visible={this.state.cameraModal}
                    // visible={this.state.Alert_Visibility}
                    onRequestClose={() => {
                        this.toggleCameraModal();
                    }}>


                    <View style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: width,
                        height: height,
                        // marginTop:38,
                        alignSelf: 'center',
                        backgroundColor: '#000000AA',
                    }}>


                        <View style={[styles.Alert_Main_View]}>

                            <TouchableOpacity
                                style={{
                                    position: 'absolute',
                                    right: 10,
                                    top: 10,
                                }}
                                onPress={(section) => {
                                    this.toggleCameraModal();
                                }}>
                                <Image
                                    source={Assets.icons.Cross}
                                    style={{
                                        resizeMode: 'cover',
                                        width: 15,
                                        height: 15,
                                        marginRight: 10,
                                    }}
                                />
                            </TouchableOpacity>

                            <Text style={styles.Alert_Title}>Please select one option</Text>


                            <View style={{width: '100%', height: 2, backgroundColor: '#fff'}}/>


                            <TouchableOpacity
                                onPress={() => {
                                    this.captureImage();
                                }}>
                                <Text style={styles.Alert_Message}> Capture From Camera </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => {
                                    this.pickFromGallery();
                                }}>
                                <Text style={[styles.Alert_Message, {marginTop: 20}]}> Pick From Gallery </Text>
                            </TouchableOpacity>

                            <View style={{width: '100%', height: 1, backgroundColor: '#fff'}}/>


                        </View>

                    </View>


                </Modal>

                <DateTimePickerModal
                    isVisible={this.state.isDateModal}
                    mode="date"
                    onConfirm={(date)=>{

                        this.setState({
                            regDate:moment(date).format('DD-MM-YYYY'),
                            isDateModal:!this.state.isDateModal

                        })
                    }}
                    onCancel={()=>{
                        this.setState({
                            isDateModal:!this.state.isDateModal

                        })
                    }}
                />
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    styleVehicleTypes: {
        backgroundColor: Assets.colors.vehicleTypeBg,

    },
    styleRecentVehicles: {
        backgroundColor: Assets.colors.white,

    },
    styleSearchSection: {
        flexDirection: 'column',
    },
    styleRecentSection: {
        flexDirection: 'column',
    },
    styleSearchHeading: {
        textAlign: 'center', color: 'white', fontSize: 18, fontWeight: 'bold', margin: 20,

    },
    textStyle: {
        color: Assets.colors.white,
        fontSize: 11,

        // marginTop: 0,
    },
    recentTextStyle1: {
        fontSize: 11,
        width: '90%',

        // marginTop: 0,
    },
    recentTextStyle2: {
        color: Assets.colors.vehicleTypeBg,
        fontSize: 11,

        marginTop: 5,
    },
    recentTextStyle3: {
        fontSize: 9,
        marginStart: 5,
    },
    recentIconStyle: {resizeMode: 'contain', width: 10, height: 10, borderRadius: 0},
    buttonTextStyle: {
        color: Assets.colors.vehicleTypeBg,
        marginTop: 20,
        marginStart: 20,
        fontWeight: 'bold',
        fontSize: 20,
    },
    Input: {
        color: '#464646',
        fontSize: 10,
        width: '80%',
    }, itemTitleStyle: {
        fontSize: 12, color: 'black', fontWeight: 'bold', marginStart: 15,
    }, sectionTitleStyle: {
        fontSize: 15,
        color: Assets.colors.vehicleTypeBg,
        fontWeight: 'bold',
        marginTop: 10,
        marginStart: 20,
    },
    itemRowStyle: {alignItems: 'center', flexDirection: 'row', flex: 0.95, marginTop: 5, marginBottom: 5},
    Alert_Main_View: {

        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        height: 200,
        width: '90%',
        borderRadius: 10,
        resizeMode: 'contain',

    },

    Alert_Title: {

        fontSize: 18,
        color: 'black',
        textAlign: 'center',
        padding: 10,
        height: '28%',
    },

    Alert_Message: {

        fontSize: 16,
        color: 'white',
        textAlign: 'center',
        padding: 10,
        borderRadius: 5,
        width: 200,
        backgroundColor: Assets.colors.vehicleTypeBg,
    },

    buttonStyle: {

        width: '50%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',

    },

    modalTextStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 22,
        marginTop: -5,
    },

    featureIconStyle: {resizeMode: 'contain', width: 17, height: 17, margin: 5,marginStart:10,marginEnd:15},

    featureRowStyle: {flexDirection: 'row', width: '100%'},

    featureBgStyle1: {
        flexDirection: 'row', width: '48%', alignItems: 'center',
        backgroundColor: Assets.colors.featureBg1, margin: 3,
    },
    featureBgStyle2: {
        flexDirection: 'row', width: '90%', alignItems: 'center',
        // backgroundColor: Assets.colors.featureBg2,
        margin: 5,borderWidth:0.3, borderRadius:10,
    },
    featureTextLabelStyle: {
        color: Assets.colors.black,
        fontSize: 11,
        fontWeight: 'bold',
        width: '25%',
    },
    featureTextValueStyle: {
        color: 'gray',
        // color: '#464646',
        fontSize: 16,
        width: '45%',
        fontWeight: "800",
        
        // borderBottomWidth: 1,
        // borderBottomColor: '#464646',
        // paddingBottom: -5,
    },

});
