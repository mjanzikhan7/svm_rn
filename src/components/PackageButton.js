import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import Assets from '../assets';
export default class PackageButton extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={[styles.buttonStyle,this.props.buttonStyle]}>
                <View
                style={{
                    flexDirection: "row",
                    flex:1,
                    alignItems: "center",
                    justifyItems: "center",
                }}>
                    <View
                        style={{
                            flexDirection: "column",
                            flex:0.7,
                            margin:5
                        }}>
                        <Text style={[styles.buttonTextStyle,this.props.textStyle]}>{this.props.text1}</Text>
                        <Text style={[styles.buttonTextStyle1,this.props.textStyle1]}>{this.props.text2}</Text>

                    </View>

                    <View
                        style={{
                            flexDirection: "column",
                            flex:0.2    ,
                            alignItems: "center",
                            justifyItems: "center",
                        }}>
                        <Image style={[{width:30,height:30, tintColor:'white'},this.props.arowStyle]} resizeMode={'contain'}
                               source={this.props.arow}/>

                    </View>

                </View>

            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        marginTop:20,
        flexDirection: "column",
        width:"80%",
        padding:3,
        borderRadius:5,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:Assets.colors.vehicleTypeBg
        // marginTop: 0,
    },
    buttonTextStyle: {
        color:Assets.colors.white,
        // marginTop: 0,
        margin: 5,
        fontWeight:"bold",
        fontSize:18
    },
    buttonTextStyle1: {
        color:Assets.colors.white,
        // marginTop: 0,
        margin: 5,
        fontWeight:"bold",
        fontSize:12
    },

})
