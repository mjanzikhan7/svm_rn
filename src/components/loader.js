import React, { Component } from 'react';
import { View, ActivityIndicator, Modal,StyleSheet} from 'react-native';
import * as Assets from '../assets';

export default Loader = props => {
    //console.log('loader is called')
    if(props.visible){
        return (
            <View style={[{
                position: 'absolute',
                top: 0, bottom: 0, left: 0, right: 0,
                alignItems: 'center', justifyContent: 'center',
                backgroundColor: '#80808066'
            }, props.style]}>
                <ActivityIndicator size="large" color={Assets.colors.vehicleTypeBg} />
            </View>
        );
    }
    else{
        return null
    }
}
