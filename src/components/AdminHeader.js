import React, { Component } from 'react';
import {View, TouchableOpacity, Image, Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
export default class AdminHeader extends Component{
    constructor(props){
        super(props)
        this.state={
          
        }
    }

    render(){
        return (
            <LinearGradient colors={['#5F9C00', '#5F9C00', '#0b8505']} style={styles.linearGradient}>
            <View style={styles.headerContainer}>
                <TouchableOpacity 
                    style={{position: 'absolute', left: 5}}
                    onPress={()=>{
                        this.props.leftAction()
                    }}>
                    <Image 
                        style={{resizeMode: 'contain', width: 20, height: 20,}}
                        source={this.props.leftIcon}
                    />
                </TouchableOpacity>
                <Text style={{fontSize: 20, color: 'white'}}>{this.props.headerText}</Text>
                <TouchableOpacity 
                    style={{position: 'absolute', right: 15}}
                    onPress={()=>{
                        this.props.rightAction()
                    } }>
                    <Image 
                        style={{resizeMode: 'contain', width: 23, height: 23,}}
                        source={this.props.rightIcon}
                    />
                </TouchableOpacity>
            </View>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        width: '100%', 
        height: 50, 
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
      },
})