import React, {Component} from 'react';
import {View, TouchableOpacity, Image, Text, StyleSheet, TextInput} from 'react-native';
import Assets from '../assets';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import DropDown from './DropDown';
import {Picker} from 'native-base';
import {API} from '../constants';
import Button1 from './Button1';
import SimpleToast from 'react-native-simple-toast';
import Loader from './loader';
import {NavigationActions, StackActions} from 'react-navigation';

export default class FilterDrawer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            listVehicleType: [

                {
                    id: 0,
                    name: 'Select vehicle',
                    value: '20',
                },


            ],
            listVehicleMake: [

                {
                    id: 0,
                    name: 'Select Brand',
                    value: '20',
                },


            ],
            listVehicleModel: [

                {
                    id: 1,
                    name: 'Select Model',
                    value: '20',
                },

            ],
            listVehicleYear: [

                {
                    id: 1,
                    reg_year: 'Year',
                    value: '20',
                },

            ],
            listVehicleTransmission: [

                {
                    id: 0,
                    name: 'Transmission',
                    value: '20',
                },

            ],
            listVehicleCondition: [

                {
                    id: 0,
                    name: 'Condition',
                    value: '20',
                },


            ],

            selectedVType: '',
            selectedVMake: '',
            selectedVModel: '',
            selectedVY: '',
            selectedVTrm: '',
            selectedVC: '',
            minPrice: '',
            maxPrice: '',
            minMilage: '',
            maxMilage: '',
            additionalFeatures: '',
        };
    }

    componentDidMount() {
        this.getFilterList();
    }

    handleVehicleType(data) {
        let temp = [];
        temp = this.state.listVehicleType;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleType: temp,
        });

    }

    handleVehicleMaker(data) {
        let temp = [];
        temp = this.state.listVehicleMake;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleMake: temp,
        });

    }

    handleVehicleModel(data) {
        let temp = [];
        temp = this.state.listVehicleModel;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleModel: temp,
        });

    }

    handleVehicleyear(data) {
        let temp = [];
        temp = this.state.listVehicleYear;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleYear: temp,
        });

    }

    handleVehicleTransmission(data) {
        let temp = [];
        temp = this.state.listVehicleTransmission;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleTransmission: temp,
        });

    }

    handleVehicleCondition(data) {
        let temp = [];
        temp = this.state.listVehicleCondition;
        if (temp.length > 1) {
            for (let x = 1; x < temp.length; x++) {
                temp.pop();
            }
        }
        for (let x = 0; x < data.length; x++) {
            temp.push(data[x]);
        }

        this.setState({
            listVehicleCondition: temp,
        });

    }

    drawer() {
        this.props.navigation.closeDrawer();
    }


    getFilterList() {
        this.setState({loading: true});

        fetch(API.FilterLists, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let years = [];
                    for (let x = 0; x < response.data.year.length; x++) {
                        years.push({
                            reg_year: response.data.year[x].reg_year + '',
                        });
                    }
                    /*  this.setState({
                          listVehicleType: response.data.vehicles,
                          listVehicleMake: response.data.brands,
                          listVehicleModel: response.data.models,
                          listVehicleYear: years,
                          listVehicleTransmission: response.data.transmition_type,
                          listVehicleCondition: response.data.condition,
                      });*/

                    this.handleVehicleType(response.data.vehicles);
                    this.handleVehicleMaker(response.data.brands);
                    this.handleVehicleModel(response.data.models);
                    this.handleVehicleyear(years);
                    this.handleVehicleTransmission(response.data.transmition_type);
                    this.handleVehicleCondition(response.data.condition);


                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }

    dropVehicleType() {
        let countries = this.state.listVehicleType;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleMake() {
        let countries = this.state.listVehicleMake;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleModel() {
        let countries = this.state.listVehicleModel;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleYear() {
        let countries = this.state.listVehicleYear;
        console.log(this.state.listMiles);
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.reg_year} value={item.reg_year}
            />);
        }));
    }

    dropVehicleTransmission() {
        let countries = this.state.listVehicleTransmission;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    dropVehicleCondition() {
        let countries = this.state.listVehicleCondition;
        return (countries.map((item, index) => {
            return (<Picker.Item label={item.name} value={item.id}
            />);
        }));
    }

    onValueChangeVT(value: string) {
        this.setState({
            selectedVType: value,

        });
        setTimeout(() => {
            this.getVehicleBrands()
        }, 300);
        // SimpleToast.show(JSON.stringify(value))
    }

    onValueChangeVMake(value: string) {
        this.setState({
            selectedVMake: value,
        });
        setTimeout(() => {
            this.getBrandModels()
        }, 300);

    }

    onValueChangeVModel(value: string) {
        this.setState({
            selectedVModel: value,
        });
    }

    onValueChangeVY(value: string) {
        this.setState({
            selectedVY: value,
        });
    }

    onValueChangeVTrm(value: string) {
        this.setState({
            selectedVTrm: value,
        });
    }

    onValueChangeVC(value: string) {
        this.setState({
            selectedVC: value,
        });
    }


    getVehicleBrands() {
        this.setState({loading: true});

        fetch(API.GetBrandsWithVehicleType + 'vehicle_id=' + this.state.selectedVType, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let temp = [];
                    /* temp = this.state.vehicleBrandsList
                     if (temp.length>1){
                         for (let x= 1; x<temp.length; x++){
                             temp.pop()
                         }
                     }*/
                    temp.push( {
                        "id": 0,
                        "name": "Select Brand",
                        "image": "aermacchi.png",
                        "vehicle_id": 2,
                        "status": 1
                    },)
                    for (let x= 0; x<data.data.length; x++){
                        temp.push(data.data[x])
                    }

                    this.setState({
                        listVehicleMake: temp,
                    });

                    if (data.data.length>0){
                        this.setState({
                            // selectedBrandValue:data.data[0].id
                        })
                    }else{
                        this.setState({
                            selectedBrandValue:"0"
                        })
                    }
                    setTimeout(() => {
                        this.getBrandModels()
                    }, 300);


                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }
    getBrandModels() {
        this.setState({loading: true});

        fetch(API.GetBrandModels + 'brand_id=' + this.state.selectedVMake, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(response => response.json())
            .then(response => {

                console.log(response);
                console.log('getBrandModels Response: ' + JSON.stringify(response));

                if (response.success) {
                    this.setState({loading: false});
                    let data = response.data;
                    let temp = [];
                    /* temp = this.state.vehicleModelList
                     if (temp.length>1){
                         for (let x= 1; x<temp.length; x++){
                             temp.pop()
                         }
                     }*/
                    temp.push({
                        "id": 0,
                        "name": "Select Model",
                        "image": "aermacchi.png",
                        "vehicle_id": 2,
                        "status": 1
                    },)
                    for (let x= 0; x<data.data.length; x++){
                        temp.push(data.data[x])
                    }
                    this.setState({
                        listVehicleModel: temp,
                    });


                } else {

                    this.setState({
                        loading: false,
                        // listItem: [],
                    });
                    console.log('Error: ' + response.message);
                }
            })
            .catch(error => {
                this.setState({loading: false});
                console.log('ApiError:', error);
            });
    }
    searchVehicles() {
        const {
            selectedVType,
            selectedVMake,
            selectedVModel,
            selectedVY,
            selectedVTrm,
            selectedVC,
            minPrice,
            maxPrice,
            minMilage,
            maxMilage,
            additionalFeatures,
        } = this.state;
        this.setState({loading: true});
        console.log('searchVehicles');
        console.log('API URL:  ' + API.Filter);
        let requestBody = new FormData();
        requestBody.append('vehicle_id', selectedVType);
        requestBody.append('brand_id', selectedVMake);
        requestBody.append('model_id', selectedVModel);
        requestBody.append('year', selectedVY);
        requestBody.append('min_mileage', minMilage);
        requestBody.append('max_mileage', maxMilage);
        requestBody.append('transmission_id', selectedVTrm);
        requestBody.append('minprice', minPrice);
        requestBody.append('maxprice', maxPrice);
        requestBody.append('feature', additionalFeatures);
        requestBody.append('offset', '0');

        let params = {
            vehicle_id: selectedVType,
            brand_id: selectedVMake,
            model_id: selectedVModel,
            year: selectedVY,
            mileage: minMilage,
            transmission_id: selectedVTrm,
            minprice: minPrice,
            maxprice: maxPrice,
            feature: additionalFeatures,
            offset: '0',
        };

        console.log(requestBody);
        fetch(API.Filter, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: requestBody,
        }).then(response => {
            console.log('response before json:', response);
            return response.json();
        }).then(response => {
            console.log(response);
            console.log('Response: ' + JSON.stringify(response));

            if (response.success) {
                this.setState({loading: false});
                let data = response.data;

                if (data.result.length > 0) {
                    // this.props.navigation.navigate("sell")
                    this.props.navigation.navigate('Search', {
                        vehicleList: data.result,
                        totalCars: data.total_match,
                        params: params,
                        nav: 'filter',

                    });
                    /*this.props.navigation.dispatch(StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({
                            routeName: 'SearchScreen',
                            params: {
                                vehicleList: data.result,
                                totalCars: data.total_match,
                                params: params,
                                nav: 'filter',

                            },
                        })],
                    }));*/
                    this.drawer();
                } else {
                    SimpleToast.show('Data not found');
                }

            } else {

                this.setState({
                    loading: false,
                    // listItem: [],
                });
                console.log('Error: ' + response.message);
            }
            this.resetAll()
        }).catch(error => {
            this.setState({loading: false});
            console.log('ApiError:', error);
        });

    }

    resetAll(){
        this.setState({
            selectedVType: '',
            selectedVMake: '',
            selectedVModel: '',
            selectedVY: '',
            selectedVTrm: '',
            selectedVC: '',
            minPrice: '',
            maxPrice: '',
            minMilage: '',
            maxMilage: '',
            additionalFeatures: '',
        });
    }

    render() {
        return (
            <View>

                <View
                    style={{
                        width: '100%',
                        height: 50, backgroundColor: Assets.colors.vehicleTypeBg, alignItems: 'center',
                        justifyContent: 'center',
                    }}>

                </View>
                <KeyboardAwareScrollView>
                    <View
                        style={{alignItems: 'center'}}>
                        <DropDown

                            pickerStyle={{
                                margin: 10, width: '80%', marginTop: 20,
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10,
                            }}
                            style={{color: Assets.colors.vehicleTypeBg, tintColor: Assets.colors.vehicleTypeBg}}
                            dropDownItems={this.dropVehicleType()}
                            onValueChange={this.onValueChangeVT.bind(this)}
                            selectedValue={this.state.selectedVType}
                            placeholder={"Select Vehicle"}

                        />

                        <DropDown

                            pickerStyle={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10,
                            }}
                            style={{color: Assets.colors.vehicleTypeBg, tintColor: Assets.colors.vehicleTypeBg}}
                            dropDownItems={this.dropVehicleMake()}
                            onValueChange={this.onValueChangeVMake.bind(this)}
                            selectedValue={this.state.selectedVMake}
                            placeholder={"Select Brand"}

                        />

                        <DropDown

                            pickerStyle={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10,
                            }}
                            style={{color: Assets.colors.vehicleTypeBg, tintColor: Assets.colors.vehicleTypeBg}}
                            dropDownItems={this.dropVehicleModel()}
                            onValueChange={this.onValueChangeVModel.bind(this)}
                            selectedValue={this.state.selectedVModel}
                            placeholder={"Select Model"}

                        />

                        <DropDown

                            pickerStyle={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10,
                            }}
                            style={{color: Assets.colors.vehicleTypeBg, tintColor: Assets.colors.vehicleTypeBg}}
                            dropDownItems={this.dropVehicleYear()}
                            onValueChange={this.onValueChangeVY.bind(this)}
                            selectedValue={this.state.selectedVY}
                            placeholder={"Year"}

                        />

                        <View
                            style={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10, flexDirection: 'column', borderRadius: 10,
                            }}>
                            <View
                                style={{
                                    margin: 10, width: '100%',
                                    backgroundColor: Assets.colors.featureBg2,
                                    flexDirection: 'column',
                                }}>

                                <Text
                                    style={{fontSize: 12, color: '#464646'}}
                                >Price</Text>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                    }}>
                                    <TextInput
                                        keyboardType={'number-pad'}
                                        placeholder={'Min'}
                                        secureTextEntry={false}
                                        onChangeText={(text) => {
                                            this.setState({
                                                minPrice: text,
                                            });
                                        }}
                                        value={this.state.minPrice}
                                        // maxLength={this.props.maxLength}
                                        style={styles.Input}/>

                                    <TextInput
                                        keyboardType={'number-pad'}
                                        placeholder={'Max'}
                                        secureTextEntry={false}
                                        onChangeText={(text) => {
                                            this.setState({
                                                maxPrice: text,
                                            });
                                        }}
                                        value={this.state.maxPrice}
                                        // maxLength={this.props.maxLength}
                                        style={[styles.Input, {marginStart: 21}]}/>

                                </View>

                            </View>


                        </View>

                        <DropDown

                            pickerStyle={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10,
                            }}
                            style={{color: Assets.colors.vehicleTypeBg, tintColor: Assets.colors.vehicleTypeBg}}
                            dropDownItems={this.dropVehicleTransmission()}
                            onValueChange={this.onValueChangeVTrm.bind(this)}
                            selectedValue={this.state.selectedVTrm}
                            placeholder={"Transmission"}

                        />

                        <DropDown

                            pickerStyle={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10,
                            }}
                            style={{color: Assets.colors.vehicleTypeBg, tintColor: Assets.colors.vehicleTypeBg}}
                            dropDownItems={this.dropVehicleCondition()}
                            onValueChange={this.onValueChangeVC.bind(this)}
                            selectedValue={this.state.selectedVC}
                            placeholder={"Condition"}

                        />

                        <View
                            style={{
                                margin: 10, width: '80%',
                                backgroundColor: Assets.colors.featureBg2,
                                padding: 10, flexDirection: 'column', borderRadius: 10,
                            }}>
                            <View
                                style={{
                                    margin: 10, width: '100%',
                                    backgroundColor: Assets.colors.featureBg2,
                                    flexDirection: 'column',
                                }}>

                                <Text
                                    style={{fontSize: 12, color: '#464646'}}
                                >Milage</Text>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                    }}>
                                    <TextInput
                                        keyboardType={'number-pad'}
                                        placeholder={'Min'}
                                        secureTextEntry={false}
                                        onChangeText={(text) => {
                                            this.setState({
                                                minMilage: text,
                                            });
                                        }}
                                        value={this.state.minMilage}
                                        // maxLength={this.props.maxLength}
                                        style={styles.Input}/>

                                    <TextInput
                                        keyboardType={'number-pad'}
                                        placeholder={'Max'}
                                        secureTextEntry={false}
                                        onChangeText={(text) => {
                                            this.setState({
                                                maxMilage: text,
                                            });
                                        }}
                                        value={this.state.maxMilage}
                                        // maxLength={this.props.maxLength}
                                        style={[styles.Input, {marginStart: 21}]}/>

                                </View>

                            </View>

                        </View>

                        <Text
                            style={{color: Assets.colors.vehicleTypeBg}}>
                            Additional Features
                        </Text>

                        <View
                            style={{
                                width: '80%', flexDirection: 'row',
                                borderRadius: 30, borderWidth: 0.2, height: 35, marginTop: 20, alignItems: 'center',
                            }}>

                            <TextInput
                                keyboardType={'default'}
                                placeholder={''}
                                secureTextEntry={false}
                                value={this.state.additionalFeatures}
                                onChangeText={(text) => {
                                    this.setState({
                                        additionalFeatures: text,
                                    });
                                }}

                                // maxLength={this.props.maxLength}
                                style={{width: '95%', fontSize: 9, marginStart: 5}}/>
                        </View>

                        <Button1
                            buttonStyle={{marginTop: 10, padding: 5, width: '40%'}}
                            text1={'APPLY'}
                            onPress={() => {
                                this.searchVehicles();
                            }}
                        />

                        <View
                            style={{flexDirection: 'row-reverse', width: '90%', marginTop: 15}}>
                            <TouchableOpacity
                                onPress={() => {
                                   this.resetAll()
                                }}>
                                <Text
                                    style={{fontSize: 12, color: '#464646'}}>RESET ALL</Text>
                            </TouchableOpacity>
                        </View>

                        <View
                            style={{height: 100}}/>

                    </View>

                </KeyboardAwareScrollView>
                <Loader
                    visible={this.state.loading}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        flexDirection: 'column',
        width: '70%',
        padding: 3,
        borderRadius: 30,
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Assets.colors.vehicleTypeBg,
        // marginTop: 0,
    },
    buttonTextStyle: {
        color: Assets.colors.white,
        // marginTop: 0,
    },
    Input: {
        color: '#464646',
        fontSize: 11,
        width: '40%',
        borderBottomWidth: 1,
        borderBottomColor: '#464646',
        paddingBottom: -5,
    },
});
