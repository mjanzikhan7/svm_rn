import React, { Component } from 'react';
import { View, TouchableOpacity, TextInput, Image, Text, StyleSheet } from 'react-native';

export default class AppInputField extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (

            <View style={[styles.ElementsContainer]}>
                <View style={{ justifyContent: "center" }}>
                    <Image source={this.props.fieldIcon} style={[styles.ImageStyle, { height: 15, width: 15 }]} />
                </View>

                    {/*<Text style={styles.TextStyle}>{this.props.fieldText}</Text>*/}
                    <TextInput
                        keyboardType={this.props.fieldKeyboardType}
                        placeholder={this.props.fieldPlaceHolder}
                        secureTextEntry={this.props.fieldTextEntry}
                        onChangeText={this.props.getText}
                        maxLength={this.props.maxLength}

                        style={styles.Input}>
                    </TextInput>

            </View>
        )
    }
}

const styles = StyleSheet.create({

    ElementsContainer: {
        backgroundColor: "white",
        padding: 5,
        borderRadius: 12,
        flexDirection: 'row',
        borderWidth:0.3,
        flex:0.7
    },
    TextStyle: {
        marginLeft: 18,
        fontSize: 14,
        color: '#707070'
    },
    Input: {
        color: '#464646',
        marginLeft: 15,
        fontSize: 14,
        fontWeight: "bold"
    },
    ImageStyle: {
        height: 15,
        width: 15,
        resizeMode: "contain",
        marginTop: 1,
        marginLeft: 5,
    },
})


