import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import Assets from '../assets';
export default class PackageFeatureItem extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View
                style={{flex:1,
                    flexDirection:"row",
                    padding:3,
                    borderRadius:5,
                    alignItems:"center",}}>
                <Image style={[{
                    // flex:0.05,
                    width:12,height:12,margin:5,},this.props.iconStyle]}
                       resizeMode={'contain'}
                       source={this.props.icon}/>

                <Text style={[styles.buttonTextStyle1,this.props.textStyle]}>{this.props.text1}</Text>


            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        flexDirection: "column",
        width:"80%",
        padding:3,
        borderRadius:5,
        margin: 5,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:Assets.colors.vehicleTypeBg
        // marginTop: 0,
    },
    buttonTextStyle: {
        color:Assets.colors.white,
        // marginTop: 0,
        margin: 5,
        fontWeight:"bold",
        fontSize:18
    },
    buttonTextStyle1: {
        // flex:0.8,
        color:Assets.colors.black,
        width:"90%",
        // marginTop: 0,
        margin: 5,
        fontWeight:"bold",
        fontSize:9
    },

})
