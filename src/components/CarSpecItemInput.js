import React, { Component } from 'react';
import {View, TouchableOpacity, Image, Text, StyleSheet, TextInput} from 'react-native';
import Assets from '../assets';
export default class CarSpecItemInput extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View
                style={styles.featureBgStyle2}>
                <Image source={this.props.icon}
                       style={styles.featureIconStyle}/>
                <Text
                    style={styles.featureTextLabelStyle}>
                    {this.props.label}
                </Text>


                <TextInput
                    keyboardType={this.props.keyboardType}
                    placeholder={this.props.placeholder}
                    secureTextEntry={this.props.secureTextEntry}
                    onChangeText={this.props.onChangeText}
                    value={this.props.value}
                    // maxLength={this.props.maxLength}
                    style={styles.featureTextValueStyle}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    featureIconStyle: {resizeMode: 'contain', width: 17, height: 17, margin: 5,marginStart:10,marginEnd:15},

    featureRowStyle: {flexDirection: 'row', width: '100%'},

    featureBgStyle1: {
        flexDirection: 'row', width: '48%', alignItems: 'center',
        backgroundColor: Assets.colors.featureBg1, margin: 3,
    },
    featureBgStyle2: {
        flexDirection: 'row', width: '90%', alignItems: 'center',
        // backgroundColor: Assets.colors.featureBg2,
        margin: 5,borderWidth:0.3, borderRadius:10,
    },
    featureTextLabelStyle: {
        color: Assets.colors.black,
        fontSize: 11,
        fontWeight: 'bold',
        width: '25%',
    },
    featureTextValueStyle: {
        color: 'gray',
        // color: '#464646',
        fontSize: 16,
        width: '45%',
        fontWeight: "800",
        marginTop:-7,
        marginBottom:-7
        // borderBottomWidth: 1,
        // borderBottomColor: '#464646',
        // paddingBottom: -5,
    },
})
