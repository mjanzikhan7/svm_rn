import React, { Component } from 'react';
import { View, TouchableOpacity, TextInput, Image, Text, StyleSheet } from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import Assets from '../assets';

export default class AppInputField extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View
                style={styles.featureBgStyle1}>

                <Text
                    style={styles.featureTextLabelStyle}>
                    {this.props.label}
                </Text>


                <TextInput
                    keyboardType={this.props.keyboardType}
                    placeholder={this.props.placeholder}
                    secureTextEntry={this.props.secureTextEntry}
                    onChangeText={this.props.onChangeText}
                    value={this.props.value}
                    // maxLength={this.props.maxLength}
                    style={styles.featureTextValueStyle}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    featureIconStyle: {resizeMode: 'contain', width: 17, height: 17, margin: 5,marginStart:10,marginEnd:15},

    featureRowStyle: {flexDirection: 'row', width: '100%'},

    featureBgStyle1: {
        flexDirection: 'column', width: '48%',
        // alignItems: 'center',
        // backgroundColor: Assets.colors.white,
        // margin: 3,borderWidth:0.2, borderRadius:10,
    },
    featureBgStyle2: {
        flexDirection: 'column', width: '45%', alignItems: 'center',
        // backgroundColor: Assets.colors.featureBg2,
        margin: 5,borderWidth:0.2, borderRadius:10,
    },
    featureTextLabelStyle: {
        color: Assets.colors.black,
        fontSize: 11,
        fontWeight: 'bold',
        width: '40%',
        marginStart: 15
    },
    featureTextValueStyle: {
        color: 'gray',
        // color: '#464646',
        fontSize: Platform.OS === 'ios' ? 12:12,
        width: '90%',
        fontWeight: "800",
        paddingStart:Platform.OS === 'ios' ? 5:2,
        paddingTop:Platform.OS === 'ios' ? 4:-7,
        paddingBottom:Platform.OS === 'ios' ? 4:-7,
        // borderBottomWidth: 1,
        // borderBottomColor: '#464646',
        // paddingBottom: -5,
        backgroundColor: Assets.colors.white,
        margin: Platform.OS === 'ios' ? 10:3,
        borderWidth:0.2, borderRadius:10,
    },
})


