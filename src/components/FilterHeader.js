import React, { Component } from 'react';
import {View, TouchableOpacity, Image, Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
export default class FilterHeader extends Component{
    constructor(props){
        super(props)
        this.state={

        }
    }

    render(){
        return (
            <LinearGradient colors={['#73AC00', '#6DA800', '#4A8D00']}>
            <View style={styles.headerContainer}>
                <TouchableOpacity 
                    style={{position: 'absolute', left: 15}}
                    onPress={()=>{
                        this.props.leftAction()
                    }}>
                    <Text style={{fontSize: 18, color: 'white'}}> {this.props.leftText}</Text>
                    
                </TouchableOpacity>
                <Text style={{fontSize: 20, color: 'white'}}>{this.props.centerText}</Text>
                <TouchableOpacity 
                    style={{position: 'absolute', right: 15}}
                    onPress={()=>{
                        this.props.rightAction()
                    } }>
                   <Text style={{fontSize: 18, color: 'white'}}> {this.props.rightText}</Text>
                    
                </TouchableOpacity>
            </View>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        width: '100%', 
        height: 55, 
        backgroundColor: '#5F9C00', 
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },

})