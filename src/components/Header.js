import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
export default class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <LinearGradient colors={['#73AC00', '#6DA800', '#4A8D00']} >
                <View style={styles.headerContainer}>
                    <TouchableOpacity
                        style={{ position: 'absolute', left: 15, zIndex: 999 }}
                        onPress={() => {
                            this.props.leftAction()
                        }}>
                        <Image
                            style={{ resizeMode: 'contain', width: 20, height: 20,tintColor:"#fff" }}
                            source={this.props.leftIcon}
                        />
                    </TouchableOpacity>
                    <Text style={{ fontSize: 20, color: 'white',fontWeight:this.props.fontweight }}>{this.props.headerText}</Text>
                    <TouchableOpacity
                        style={{ position: 'absolute', right: 15, zIndex: 999 }}
                        onPress={() => {
                            this.props.rightAction()
                        }}>
                        <Image
                            style={{ resizeMode: 'contain', width: 20, height: 20,tintColor:"#fff" }}
                            source={this.props.rightIcon}
                        />
                    </TouchableOpacity>
                </View>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        width: '100%',
        height: 55,
        // backgroundColor: '#5F9C00',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: 0,
    },

})
