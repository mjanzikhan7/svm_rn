import React, { Component } from 'react';
import { Animated, StyleSheet, Text, View, Image } from 'react-native';

import { RectButton } from 'react-native-gesture-handler';

import Swipeable from 'react-native-gesture-handler/Swipeable';
const AnimatedIcon = Animated.createAnimatedComponent(Image);
export default class AppleStyleSwipeableRow extends Component {

  renderRightActions = (progress, dragX) => {
    const scale = dragX.interpolate({
        inputRange: [-80, 0],
        outputRange: [1, 0],
        extrapolate: 'clamp',
    });
    return (
        <View style={{ width: 75, flexDirection:'row', marginLeft: 15}}>
            <View style={{width:75}}>
            <RectButton style={styles.rightAction1} onPress={this.props.onDeletePress}>
                <AnimatedIcon
                    source={require('../assets/icons/deleteIcon.png')}
                    resizeMode='contain'
                    style={[styles.actionIcon, { transform: [{ scale }] }]}
                    />
            </RectButton>
          {/* <Text style={{color:'white',backgroundColor:'gray',fontSize:12,textAlign:'center'}}>Block User</Text> */}
            </View>
        </View>
    );
  };

  updateRef = ref => {
    this._swipeableRow = ref;
  };
  close = () => {
    this._swipeableRow.close();
  };
  render() {
    const { children } = this.props;
    return (
      <Swipeable
        ref={this.updateRef}
        friction={2}
        leftThreshold={20}
        rightThreshold={40}
        renderRightActions={this.renderRightActions}
        onSwipeableWillOpen={()=>{
          if(this.props.onSwipeableOpen){
            this.props.onSwipeableOpen(this._swipeableRow)
          }
        }}
        onSwipeableWillClose={()=>{
          if(this.props.onSwipeableClose){
            this.props.onSwipeableClose(this._swipeableRow)
          }
        }}
        >
        {children}
      </Swipeable>
    );
  }
}

const styles = StyleSheet.create({
  leftAction: {
    flex: 1,
    backgroundColor: '#497AFC',
    justifyContent: 'center',
  },
  actionText: {
    color: 'white',
    fontSize: 16,
    backgroundColor: 'transparent',
    padding: 10,
  },
  actionIcon: {
    width: 23,
    height: 23,
    marginHorizontal: 10,
  },
  rightAction: {
    alignItems: 'center',
    backgroundColor: '#096DC3',
    flex: 1,
    justifyContent: 'center',
  },
  rightAction1: {
    alignItems: 'center',
    backgroundColor: '#FF3723',
    flex: 1,
    justifyContent: 'center',

  },
});
