import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import Assets from '../assets';
export default class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View
            style={styles.headerContainer}>
                <Image style={{width:200,height:30
                }} resizeMode={'contain'}
                       source={Assets.images.Logo}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        width: '100%',
        height: 40,
         backgroundColor: Assets.colors.grayBg,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: 0,
    },

})
