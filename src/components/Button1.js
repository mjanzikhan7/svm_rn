import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import Assets from '../assets';
export default class Button1 extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <TouchableOpacity
                onPress={this.props.onPress}
                style={[styles.buttonStyle,this.props.buttonStyle]}>
                <Text style={[styles.buttonTextStyle,this.props.textStyle]}>{this.props.text1}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        flexDirection: "column",
        width:"70%",
        padding:3,
        borderRadius:30,
        margin: 5,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor:Assets.colors.vehicleTypeBg
        // marginTop: 0,
    },
    buttonTextStyle: {
        color:Assets.colors.white,
        // marginTop: 0,
        margin: 5,
        fontWeight:"bold"
    },

})
