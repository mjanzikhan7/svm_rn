import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import Assets from '../assets';
import {Picker} from 'native-base';
export default class DropDown extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    render() {
        return (
            <View
            style={[styles.headerContainer,this.props.pickerStyle]}>
                <Picker
                    textStyle={{ color: Assets.colors.vehicleTypeBg }}
                    itemStyle={{
                        backgroundColor: Assets.colors.white,
                        marginLeft: 0,
                        paddingLeft: 10,
                        color: Assets.colors.vehicleTypeBg
                    }}
                    itemTextStyle={{ color: Assets.colors.vehicleTypeBg }}

                    style={this.props.style}
                    selectedValue={this.props.selectedValue}
                    onValueChange={this.props.onValueChange}
                    mode={'dialog'}
                    placeholder={this.props.placeholder}
                >

                    {/* <Picker.Item label="Pakistan" value="Pakistan"/>
                                    <Picker.Item label="Palestine" value="Palestine"/>
                                    <Picker.Item label="Afghanistan" value="Afghanistan"/>
                                    <Picker.Item label="Iran" value="Iran"/>
                                    <Picker.Item label="Iraq" value="Iraq"/>*/}
                    {this.props.dropDownItems}
                </Picker>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        width: '47%',
        height: 25,
         backgroundColor: Assets.colors.white,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius:20,
    },

})
