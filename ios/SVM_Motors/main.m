#import <UIKit/UIKit.h>

#import "AppDelegate.h"
//#import "../../../svm_rn/ios/SVM_Motors/AppDelegate.h"


int main(int argc, char * argv[]) {
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
